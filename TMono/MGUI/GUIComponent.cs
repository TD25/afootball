﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;

namespace TMono.MGUI
{
    //TODO: IControl interface

    public abstract class GUIComponent : GraphicsComponent
    {
        private GUIContainer parent;

        /// <summary>
        /// Should be set if you want to remove gui elements by removing parent
        /// MGUI class will set this property
        /// </summary>
        static internal GameComponentCollection Components { set; get; }
        internal virtual GUIContainer Parent
        {
            get { return parent; }
            set { parent = value; }
        }

        public GUIComponent(GUIContainer parent)
        {
            this.parent = parent;
        }
        /// <summary>
        /// This class should subscribe to parents visibility changed event
        /// </summary>
        internal virtual void OnParentVisibilityChanged(object sender, EventArgs args)
        {
            Visible = ((DrawableGameComponent)sender).Visible;
        }
        internal virtual void OnParentRemoved()
        {
            Components.Remove(this);
        }
        internal virtual void OnParentEnabledChanged(object sender, EventArgs args)
        {
            //FIXME:? if compoenent was disabled before parent was disabled it is not preserved as disabled
            Enabled = ((DrawableGameComponent)sender).Enabled;
        }
        protected override void OnEnabledChanged(object sender, EventArgs args)
        {
            base.OnVisibleChanged(sender, args);
            bool val = ((GUIComponent)sender).Enabled;
            if (parent != null && val && !parent.Enabled) //if parent is disabled this has to be disabled too
            {
                Enabled = false;
                throw new MGUIException("Trying to set enabled on gui component when parent is disabled");
            }
        }
        protected override void OnVisibleChanged(object sender, EventArgs args)
        {
            base.OnVisibleChanged(sender, args);
            bool val = ((GUIComponent)sender).Visible;
            if ((parent != null) && (val && !parent.Visible))
            {
                Visible = false;
                throw new MGUIException("Trying to make gui component visible when parent is not");
            }
        }
        /// <summary>
        /// Sets Enabled and Visible properties to the same value
        /// </summary>
        public void SetEnabled(bool enabled)
        {
            Enabled = Visible = enabled;
        }
    }

    public abstract class PositionedGUIComponent : GUIComponent
    {
        #region Types
        public class RelativePosition
        {
            public Position posFrom;
            public Position posTo;
            public float verticalRatio;
            public float horizontalRatio;

            public RelativePosition(Position posFrom, Position posTo, float verticalRatio,
                float horizontalRatio)
            {
                this.posFrom = posFrom;
                this.posTo = posTo;
                this.verticalRatio = verticalRatio;
                this.horizontalRatio = horizontalRatio;
            }
            public RelativePosition(Position position)
            {
                posFrom = position;
                posTo = position;
                verticalRatio = horizontalRatio = 0.0f;
            }
        }
        #endregion
        #region Fields
        protected Vector2 position;
        private VerticalPosition vAlignment = VerticalPosition.TOP;
        private HorizontalPosition hAlignment = HorizontalPosition.LEFT;
        private Vector2 realPosition;
        private RelativePosition relativePosition; //used only when parent is resized
        private Vector2 scale = new Vector2(1, 1);
        private Vector2 realScale = new Vector2(1, 1);
        private Vector2 minScale = new Vector2(0, 0);
        private Vector2 maxScale = new Vector2(4, 4);
        #endregion
        #region Properties
        /// <summary>
        /// Minimum RealScale value
        /// </summary>
        public virtual Vector2 MinScale
        {
            get { return minScale; }
            set
            {
                minScale = value;
                SetRealScale(realScale);
            }
        }
        /// <summary>
        /// Maximum RealScale value
        /// </summary>
        public virtual Vector2 MaxScale
        {
            get { return maxScale; }
            set
            {
                maxScale = value;
                SetRealScale(realScale);
            }
        }
        /// <summary>
        /// Use this to find out where to draw
        /// </summary>
        public virtual Vector2 RealPosition
        {
            protected set { realPosition = value; }
            get { return realPosition; }
        }
        /// <summary>
        /// Should return scaled size
        /// </summary>
        public abstract Vector2 Size { get; }
        /// <summary>
        /// Position of top left corner
        /// </summary>
        public Vector2 NormaPos
        {
            get
            {
                Vector2 pos = Position;
                switch (hAlignment)
                {
                    case HorizontalPosition.LEFT:
                        pos.X = position.X;
                        break;
                    case HorizontalPosition.RIGHT:
                        pos.X = position.X - Size.X;
                        break;
                    case HorizontalPosition.CENTER:
                        pos.X = position.X - (Size.X / 2);
                        break;

                }
                switch (vAlignment)
                {
                    case VerticalPosition.TOP:
                        pos.Y = position.Y;
                        break;
                    case VerticalPosition.BOTTOM:
                        pos.Y = position.Y - Size.Y;
                        break;
                    case VerticalPosition.CENTER:
                        pos.Y = position.Y - (Size.Y / 2);
                        break;
                }
                return pos;
            }
        }
        //TODO: Have separate properties for internal and public setting of position,
        // because now position overrides relativePosition. That's okay if position is
        // being set according to relativePosition, but if not relativePosition should be set to null,
        // because otherwise if parent is resized position is set back according to relativePosition
        /// <summary>
        /// Position of corner specified by Vertical and Horizontal alignment
        /// </summary>
        public virtual Vector2 Position
        {
            get { return position; }
            set
            {
                position = value;            
                SetRealPos();
            }
        }
        /// <summary>
        /// Defines how to interpret position
        /// Is position relative to top, bottom or center
        /// </summary>
        public virtual VerticalPosition VerticalAlignment
        {
            get { return vAlignment; }
            set
            {
                vAlignment = value;
                SetRealPos();
            }
        }
        /// <summary>
        /// Defines how to interpret position
        /// Is position relative to left, right or center
        /// </summary>
        public virtual HorizontalPosition HorizontalAlignment
        {
            get { return hAlignment; }
            set
            {
                hAlignment = value;
                SetRealPos();
            }
        }
        internal override GUIContainer Parent
        {
            get { return base.Parent; }
            set
            {
                base.Parent = value;
                //Scale as well as position get reset according to parent
                Scale = Scale;
            }
        }
        public virtual Vector2 Scale
        {
            get { return scale; }            
            set
            {
                scale = value;
                if (Parent != null)
                    RealScale = Parent.RealScale * scale;
                else
                    RealScale = scale;
            }
        }
        /// <summary>
        /// Product of Scale * Parent.RealScale
        /// Derived classes should use it when drawing to actually scale.
        /// You may fail to set new RealScale if value is less than MinScale or
        /// larger than MaxScale
        /// </summary>
        public virtual Vector2 RealScale
        {
            protected set
            {
                Vector2 prev = realScale;
                SetRealScale(value);
                if (prev != realScale) 
                {
                    Position = Position; //reposition, because Size should return scaled size,
                                    //so realignment is necessary
                }
            } 
            get { return realScale; }
        }
        #endregion

        #region Constructors
        public PositionedGUIComponent(GUIContainer parent, Vector2 position) 
            : base(parent)
        {
            Position = position;
        }
        #endregion

        #region Methods
        protected bool CanScale(Vector2 val)
        {
            return (val.X <= MaxScale.X && val.Y <= MaxScale.Y && val.Y >= MinScale.Y &&
                    val.Y >= MinScale.Y);
        }
        internal virtual void OnParentMoved(object sender, EventArgs args)
        {
            //cause to recalculate real position
            Position = Position;
        }
        internal virtual void OnParentResized(object sender, EventArgs args)
        {
            //If position was set like RelativePosition then reposition back like it was, otherwise
            // do nothing.
            //TODO:see todo on Position property
            if (relativePosition != null)
            {
                PositionBetween(relativePosition.posFrom, relativePosition.posTo, relativePosition.verticalRatio,
                    relativePosition.horizontalRatio);
            }
            else
                SetRealPos();
        }
        internal virtual void OnParentScaled(object sender, EventArgs args)
        {
            RealScale = Parent.RealScale * Scale;
            // Need to call this even though RealScale may have not changed, else
            // bug appears for GUIContainers when resizing by dragging
            OnParentResized(sender, args); //need to do the same thing as after resizing
        }
        public void AlignToParent(VerticalPosition vPos, HorizontalPosition hPos)
        {
            if (Parent == null)
                throw new MGUIException("Trying to align to parent when parent is not set");
            Vector2 position = new Vector2(0, 0);
            position.Y = Parent.GetVerticalSide(vPos);
            position.X = Parent.GetHorizontalSide(hPos);
            Position = position;
            relativePosition = new RelativePosition(new Position(hPos, vPos));
        }
        //TODO: Allow positioning just verticaly or horizontaly
        public void PositionBetween(Position from, Position to, float verticalRatio, float horizontalRatio)
        {
            relativePosition = new RelativePosition(from, to, verticalRatio, horizontalRatio);
            Vector2 pos = Position;
            pos.X = InterpolatePositionHorizontaly(from.hPos, to.hPos, horizontalRatio);
            pos.Y = InterpolatePositionVerticaly(from.vPos, to.vPos, verticalRatio);
            Position = pos;
        }
        //public void PositionBetweenHorizontaly(HorizontalPosition from, HorizontalPosition to, float ratio)
        //{
        //    Vector2 pos = Position;
        //    pos.X = InterpolatePositionHorizontaly(from, to, ratio);
        //    Position = pos;
        //}
        //public void PositionBetweenVerticaly(VerticalPosition from, VerticalPosition to, float ratio)
        //{
        //    Vector2 pos = Position;
        //    pos.Y = InterpolatePositionVerticaly(from, to, ratio);
        //    Position = pos;
        //}
        private int InterpolatePositionVerticaly(VerticalPosition from, VerticalPosition to, float ratio)
        {
            if (Parent == null)
                throw new MGUIException("Can't position between parent sides if parent is not set");
            int pos = (int)Position.Y;
            int fromInt = Parent.GetVerticalSide(from);
            int toInt = Parent.GetVerticalSide(to);
            pos = Math.Abs(TMath.Interpolate(fromInt, toInt, ratio));
            return pos;
        }
        private int InterpolatePositionHorizontaly(HorizontalPosition from, HorizontalPosition to, float ratio)
        {
            if (Parent == null)
                throw new MGUIException("Can't position between parent sides if parent is not set");
            int pos = (int)Position.X;
            int fromInt = Parent.GetHorizontalSide(from);
            int toInt = Parent.GetHorizontalSide(to);
            pos = Math.Abs(TMath.Interpolate(fromInt, toInt, ratio));
            return pos;
        }

        protected void SetRealPos()
        {
            if (Parent != null)
                RealPosition = Parent.RealPosition + NormaPos;
        }
        public void SetAlignmet(VerticalPosition vPos, HorizontalPosition hPos)
        {
            VerticalAlignment = vPos;
            HorizontalAlignment = hPos;
        }

        protected virtual void SetRealScale(Vector2 scale)
        {
            if (scale.X > maxScale.X)
                scale.X = maxScale.X;
            else if (scale.X < minScale.X)
                scale.X = minScale.X;

            if (scale.Y > maxScale.Y)
                scale.Y = maxScale.Y;
            else if (scale.Y < minScale.Y)
                scale.Y = minScale.Y;

            realScale = scale;
        }
        #endregion
    }
}
