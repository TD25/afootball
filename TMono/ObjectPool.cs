﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMono
{
    public delegate void FreeEventHandler();
    /// <summary>
    /// Elements of ObjectPool must implement this interface
    /// And they have to have default constructor, and Free method has to invoke Freed event
    /// Call Free method to tell object pool that it is free (object has to created in object pool)
    /// </summary>
    public interface IPoolObject
    {
        event FreeEventHandler Freed;
        /// <summary>
        /// This method must invoke Freed event
        /// </summary>
        void Free();        
    }

    /// <summary>
    /// Implements object pool which can grow as you need it
    /// </summary>
    /// <typeparam name="T">Type of objects in the pool</typeparam>
    public class ObjectPool<T> where T : IPoolObject
    {
        #region Types
        internal delegate void FreedElement(int id);
        public delegate T CreateDefaultDel();
        public class ObjectAlreadyFreed : Exception
        {
            IPoolObject obj;

            public ObjectAlreadyFreed(IPoolObject obj)
            {
                this.obj = obj;
            }
            public ObjectAlreadyFreed(IPoolObject obj, string message)
                : base(message)
            {
                this.obj = obj;
            }
        }

        private class PoolElement
        {
            public T obj;
            public int id;
            FreedElement freedEvent;

            public PoolElement(int id, T obj, FreedElement freedDelegate)
            {
                this.id = id;
                this.obj = obj;
                freedEvent = freedDelegate;
                obj.Freed += OnFree;
            }

            public void OnFree()
            {
                freedEvent(id);
            }
        }
        #endregion

        #region Fields
        private List<PoolElement> elements;
        private int inUseCount = 0;
        private int unusedLimit = -1;
        private int expectedUsedCount = 10;
        private CreateDefaultDel createDefault;
        #endregion

        #region Properties
        /// <summary>
        /// Some unused objects are freed once unused count exceeds this value
        /// The number of objects left after dealocation is ExpectedUsedCount
        /// Can be -1, in that case it is not used
        /// </summary>
        public int UnusedLimit
        {
            get { return unusedLimit; }
            set { unusedLimit = value; }
        }
        /// <summary>
        /// Biggest amount of used objects you would expect
        /// There is no tragedy if actual object count exceeds this
        /// This is only needed when unusedCount exceeds UnusedLimit, 
        /// and when creating new pool (to set capacity of array)
        /// Set to 0 if you don't want to guess
        /// </summary>
        public int ExpectedMaxUsedCount
        {
            get { return expectedUsedCount; }
            set { expectedUsedCount = value; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes initialSize number of objects using its default constructor
        /// You can set initialSize to 0, and objects will be allocated as you need them
        /// See UnusedLimit and ExpectedUsedCount properties for other parameters
        /// </summary>
        /// <param name="initialSize">specifies initial size of the pool </param>
        /// <param name="expectedMaxUsedCount">specifies initial size of array that holds objects. 
        /// See ExpectedMaxUsedCount property for more.</param>
        public ObjectPool(int unusedLimit, int expectedMaxUsedCount, CreateDefaultDel createDefaultDel)
        {
            this.expectedUsedCount = expectedMaxUsedCount;
            this.unusedLimit = unusedLimit;
            this.createDefault = createDefaultDel;

            if (expectedUsedCount > 0)
                elements = new List<PoolElement>(expectedUsedCount);
            else
                elements = new List<PoolElement>();

        }

        #endregion

        #region Methods
        /// <summary>
        /// Initialize specified number of objects using createDefault delegate
        /// You can specify 0 and objects will be created as you need them
        /// </summary>
        /// <param name="initialSize"></param>
        public void InitObjects(int initialSize)
        {
            for (int i = 0; i < initialSize; i++)
                elements.Add(new PoolElement(i, createDefault(), OnFree));
        }

        /// <summary>
        /// Initialize pool with specified objects
        /// </summary>
        public void InitObjects(IList<T> obj)
        {
            for (int i = 0; i < obj.Count; i++)
                elements.Add(new PoolElement(i, obj[i], OnFree));
        }


        /// <summary>
        /// Calling code takes full responsibility for properly initializing this object!
        /// We give no guarantees about what the contents of this object is (it might not be default)
        /// </summary>
        public T Create()
        {
            T newObj;
            if (inUseCount < elements.Count)
                newObj = elements[inUseCount].obj;
            else
            {
                newObj = createDefault();
                elements.Add(new PoolElement(inUseCount, newObj, OnFree));
            }
            inUseCount++;
            return newObj;
        }

        private void OnFree(int id)
        {
            if (id >= inUseCount)
                throw new ObjectAlreadyFreed(elements[id].obj);
            Swap(id, inUseCount - 1);
            inUseCount--;

            //Check if there are too many unused objects
            if (unusedLimit > -1 && expectedUsedCount > 0 && unusedLimit < elements.Count - inUseCount)
                elements.RemoveRange(expectedUsedCount, elements.Count - inUseCount - expectedUsedCount);
        }

        private void Swap(int id1, int id2)
        {
            PoolElement temp1 = elements[id1];
            PoolElement temp2 = elements[id2];
            temp1.id = id2;
            temp2.id = id1;
            elements[id1] = temp2;
            elements[id2] = temp1;
        }
        #endregion
    }
}
