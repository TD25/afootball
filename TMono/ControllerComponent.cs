﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Jitter.Collision;
using System.Collections;
using System.Diagnostics;
using TMono.Physics;

namespace TMono
{
    /// <summary>
    /// This class should control GameObject object
    ///     using PhysicsComponent
    /// Derive from this class to create user controller with
    ///     PCControls, or to create AI which would control object.
    /// </summary>
    public class ControllerComponent : GameComponent
    {
        protected PhysicsComponent phys;

        public ControllerComponent(PhysicsComponent phys_)
            : base(null)
        {
            phys = phys_;
        }
        //public GraphicsComponentSkinned.playAnimationDel PlayAnimation
        //{
        //    set; get;
        //}

        override public void Update(GameTime time)
        {
        }
    }

    //public class UserController : ControllerComponent
    //{
    //    Controls controls;
    //    public UserController(PhysicsComponent phys, Controls controls_)
    //        : base(phys)
    //      {}
    //  public override void Update(GameTime time)
    //  {
    //           base.Update(time);
    //  }
    //}
}
