using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using TMono;
using Jitter.LinearMath;

namespace AFootball
{
    public class AI : ControllerComponent
    {
        public enum Status
        {
            ACTIVE,
            CONTROLLED,
            DISABLED
        }

        private FootballerPhysics oponent;
        private FootballerPhysics body;
        private AIProperties properties;
		private int reactionEllapsed = 0;
		private bool lost = false;      //true when behind oponent for too long
		private int behindElapsed = 0;
        private JVector intendedDirection = JVector.Zero;

        public delegate void ImpendingCollisionDel(FootballerPhysics footballer, float timeLeft);
        public ImpendingCollisionDel OnImpendingCollision;

        public Status CurrentStatus
        {
            set; get;
        }
        public JVector IntendedDirection
        {
            get { return intendedDirection; }
        }
        public Vector2 IntendedDirection2D
        {
            get { return new Vector2(intendedDirection.X, intendedDirection.Y); }
        }
		public bool HasLost
		{
			get { return lost; }
		}

        public AI(FootballerPhysics phys_, FootballerPhysics oponentPhysics, 
            int maxIterations_, int reactionPeriod, ImpendingCollisionDel onImpendingColl)
            : base(phys_)
        {
            OnImpendingCollision = onImpendingColl;
            body = phys_;
            properties = new AIProperties(maxIterations_, reactionPeriod);
            oponent = oponentPhysics;
            CurrentStatus = Status.ACTIVE;
        }
        public AI(FootballerPhysics phys_, FootballerPhysics oponentPhysics,
            AIProperties properties_, ImpendingCollisionDel onImpendingCollision) : base(phys_)
        {
            OnImpendingCollision = onImpendingCollision;
            body = phys_;
            properties = properties_;
            oponent = oponentPhysics;
            CurrentStatus = Status.ACTIVE;
        }

        public void Init(FootballerPhysics phys, FootballerPhysics oponentPhysics,
            AIProperties properties)
        {
            body = phys;
            this.properties = properties;
            oponent = oponentPhysics;
            Reset();
        }

        public void Reset()
        {
		    reactionEllapsed = 0;
		    lost = false;      
		    behindElapsed = 0;
            intendedDirection = JVector.Zero;
            CurrentStatus = Status.ACTIVE;
        }
        /// <summary>
        /// To reset back set CurrentStatus to active
        /// </summary>
        /// <param name="direction"></param>
        public void OverrideDirection(JVector direction)
        {
            CurrentStatus = Status.CONTROLLED;
            intendedDirection = direction;
        }
        public override void Update(GameTime time)
        {
			reactionEllapsed += time.ElapsedGameTime.Milliseconds;
            //don't do anything if in air or lost or body is not in control
            if (body.StaticColNormal.Length() <= float.Epsilon || lost || !body.HasControl) 
                return;
			if (reactionEllapsed >= properties.reactionPeriod)
			{
				reactionEllapsed = 0;
				//HACK: React should get time passed after last call to it...
                if (CurrentStatus == Status.ACTIVE && oponent.HasBall)
    				intendedDirection = React(properties.reactionPeriod);

                // We may no longer have control if we are charging
				if (intendedDirection.Length() > float.Epsilon && body.HasControl)
					body.Accelerate(Converter.ToXNAVector(intendedDirection));
			}
      //    base.Update(time);
        }
		//timePassed specifies time passed from last call to React
		private JVector React(int timePassed)
		{
            JVector distance = oponent.Body.Position - body.Body.Position;
            if (distance.Y >= 1.0f)
            {
                behindElapsed += timePassed;
                if (behindElapsed >= properties.timeUntilGivingUp)
                    lost = true;
            }
            else
                behindElapsed = 0;

            float time;
			JVector direction = GetDirection(distance, out time);

            // Thinking about charging
            if (body.Body.LinearVelocity.Length() > 1 && !lost && time <= 0.5f)
            {
                distance.Normalize();
                // It's harder to hit target while charging, when oponent is not going straight 
                //at us, so min time required (requiredTime) then needs to be smaller
                float velAngle = (oponent.Body.LinearVelocity.Length() > 0) ?
                    TMath.CalculateAngleAbs(-1 * distance, oponent.Body.LinearVelocity) : 0;
                float requiredTime = 0.5f * (float)((Math.PI / 2 - velAngle) / (Math.PI / 2));
                if (time <= requiredTime)
                {
                    //TODO: Make calling OnImpendingCallision independent of reaction times
                    OnImpendingCollision(body, time);
                    body.Charge(Converter.ToXNAVector(distance));
                }
            }

            return direction; 
		}
        /// <summary>
        /// Uses iterative aproximation to determine where
        ///     the target will be in future and aims there.
        /// http://www.gamedev.net/page/resources/_/technical/math-and-physics/leading-the-target-r4223
        /// </summary>
        /// <returns></returns>
        private JVector GetDirection(JVector distance, out float time)
        {
            time = float.MaxValue;
			//check if we lost then react differently
			if (lost)
			{
				if (body.Body.Position.X > 0.0f)
					return new JVector(0.2f, -0.5f, 0);
				else
					return new JVector(-0.2f, -0.5f, 0);
			}

            JVector vel = oponent.Body.LinearVelocity;
            JVector myVel = body.Body.LinearVelocity;
            float myAcc = body.Capabilities.quickness;
            float oponentAcc = oponent.Capabilities.quickness;
			//check if we really need iterative aproximation
            if (vel.Length() <= float.Epsilon)
            {
                distance.Normalize();
                return distance;
            }
			else if (vel.Length() > myVel.Length() && Math.Abs(distance.Y) > 1.0f &&
               TMath.CalculateAngleAbs(distance, vel) <= 1.7f) //if we are behind and slower
            {
                vel.Normalize();
                return vel;
            }
            //return FirstOrderCorrection(distance, vel, myVel);//just do first order correction, cause iterative won't help
            else
                return IterativeAproximation(distance, vel, oponentAcc, myVel, myAcc, out time);
        }

        private JVector IterativeAproximation(JVector distance, JVector vel, 
            float opAcc, JVector myVel, float myAcc, out float timeToCollision)
        {
            timeToCollision = float.MaxValue;
            float time = 0.0f;
            int iterations;
            float diff = 0.0f;  
            //float oldDiff = 0.0f;
            JVector opAccDir = TMath.Normalized(vel);
            JVector distanceNorm = TMath.Normalized(distance);
            //JVector myFutureVel = myVel;
			//sets predicted speed of itself to max
			//and oponents a little bigger than it's max speed
            myVel = distanceNorm * body.Capabilities.maxSpeed;
            vel = opAccDir * (oponent.Capabilities.maxSpeed + (oponent.Capabilities.quickness / 2));
            //if (myVel.Length() > 0 && TMath.CalculateAngleAbs(distanceNorm, myVel) < 1.57f)
            //    myVel = TMath.Projection(distanceNorm, myVel);
            //else
            //    myVel = distanceNorm * myAcc * (1.0f / 30.0f);
            for (iterations = 0; iterations < properties.maxIterations; iterations++)
            {
                float oldTime = time;
                //JVector newVel = vel + (oldTime * opAcc * opAccDir);
                //if (newVel.Length() <= oponent.Capabilities.maxSpeed)
                //    vel = newVel;
                //else
                //    vel = opAccDir * oponent.Capabilities.maxSpeed;
                //newVel = myVel + (oldTime * myAcc * distanceNorm);
                //if (newVel.Length() <= body.Capabilities.maxSpeed)
                //    myVel = newVel;
                //else
                //    myVel = distanceNorm * body.Capabilities.maxSpeed;
                //if (myVel.Length() == 0 || TMath.HasNaN(myVel))
                //    myVel = distanceNorm * myAcc * (1.0f / 30.0f);
                distance = ((oponent.Body.Position + (oldTime * vel)) - body.Body.Position);
				if (TMath.CalculateAngleAbs(distance, distanceNorm) > 2.8f)
                    //Math.Abs(vel.X) < 1.0f)//Math.Abs(oponent.Body.Position.X - body.Body.Position.X) < 1.0f)
                {
                    distance = -1 * distance;
                    break;
                }
                time = (distance.Length() / myVel.Length());
                diff = oldTime - time;
                if (oldTime > 0 && diff < properties.minTime) //|| Math.Abs(oldDiff - diff) < 0.00001f)
                {
                    //timeToCollision = time; //Setting timeToCollision only here (if collision is certain enough)
                    break;
                }
            }
            //if (iterations >= 20)  //for debugging only
            //    distance = distance;
            //distance += time * vel;
            timeToCollision = time;
            distance.Normalize();
            return distance;
        }
        private JVector FirstOrderCorrection(JVector distance, JVector vel, JVector myVel)
        {
            float time = distance.Length() / myVel.Length(); //TODO: myVel might be in a different direction
            JVector d = distance + vel * time;
            d.Normalize();
            return d;
        }
    }
    public class AIProperties
    {
        public int maxIterations;  //for iterative aproximation method, bigger = more accurate prediction
        public float minTime;     //smaller = more acurate
		public int reactionPeriod; //how much time in ms between reacting to oponent (changing direction)
		public int timeUntilGivingUp; 
		/// <summary>
		/// Initializes a new instance of the <see cref="T:AFootball.AIProperties"/> struct.
		/// Predicted speed not used
		/// </summary>
		/// <param name="maxIterations_"> Max iterations.</param>
		/// <param name="minTime_"> Minimum time.</param>
		/// <param name="reactionPeriod_"> Time between reacting, changin direction.</param>
        public AIProperties(int maxIterations_, int reactionPeriod_, int timeUntilGivingUp_ = 1000,
		                    float minTime_ = 0.1f)
        {
            maxIterations = maxIterations_;
            minTime = minTime_;
			reactionPeriod = reactionPeriod_;
			timeUntilGivingUp = timeUntilGivingUp_;
        }
        public AIProperties(AIProperties prop) : this(prop.maxIterations, prop.reactionPeriod,
            prop.timeUntilGivingUp, prop.minTime)
        {
        }
    }
}
