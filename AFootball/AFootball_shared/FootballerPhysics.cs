using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TMono;
using Microsoft.Xna.Framework;
using Jitter.Dynamics;
using Jitter.LinearMath;
using System.Threading;
using TMono.Physics;
using System.Diagnostics;

namespace AFootball
{
    public class FootballerPhysics : CharacterPhysics
    {
        FootballerPhysCaps caps;
        private bool hasControl = true; // Should be disabled when falling so that running animation is not set
        const float runThreshold = 13;  // At what speed footballer starts to run instead of walk
        const float maxSpeed = 20;      // Max speed for all footballers
        const float maxAnimSpeedFactor = 1.6f;
        const float minAnimSpeedFactor = 0.5f;
        //const float timeToCatch = 0.3f;
        //const float catchPosition = 1.8f;
        const float catchingDistance = 8.8f;
        const float catchingSpeed = 16.811f;
        static readonly Vector3 initialDirection = new Vector3(0, -1, 0);
        private bool catching = false;

        public bool HasBall
        { set; get; }
		public bool Catching
		{
			get { return catching; }
		}
        /// <summary>
        /// Has to be only one in a game. At least footballers can react to only one
        /// </summary>
        static public Ball Ball
        { set; get; }

        /// <summary>
        /// When enabled running animation is automaticaly run when moving
        /// </summary>
        public bool HasControl
        {
            get { return hasControl; }
            set { hasControl = value; }
        }
        public FootballerPhysCaps Capabilities
        {
            get { return caps; }
        }
        public bool IsAutoAccelerating
        {
            get; set;
        }
        public override RigidBody Body
        {
            get
            {
                return base.Body;
            }

            set
            {
                if (caps != null)
                    value.Mass = caps.mass;
                base.Body = value;
            }
        }
        public AnimatedGameObject ParentObject
        { set; get; }
        //FIXME: finite state machine?
        public bool ReadyToCatchBall
        { set; get; }
        public bool CanFall
        { set; get; } = false;
        public bool Charging
        { private set; get; }
        public bool Spinning
        { private set; get; }

        //HACK: Noticed tunneling through plane a few times.
        // Hard to find the cause, so we just use this to limit z coordinate
        public static float MinHeight
        {
            set; get;
        }

        //If invokes Fell, then does not invoke Collided
        public event EventHandler Fell;
        //TODO: Make handler take other footballer
        /// <summary>
        /// Invoked when collision with other footballer happens
        /// </summary>
        public event EventHandler Collided;

        /// <summary>
        /// </summary>
        /// <param name="hand">Hand bone with which ball should be held</param>
        public FootballerPhysics(RigidBody body_, Node node, Vector3 g,
            FootballerPhysCaps capabilities,
            bool isAcceleratingAuto = false, bool isFrictionNeededForMovement = true)
            : base(body_, node, g, isFrictionNeededForMovement)
        {
            ReadyToCatchBall = false;
            IsAutoAccelerating = isAcceleratingAuto;
            caps = capabilities;
            if (caps != null)
            {
                body.Mass = caps.mass;
                InactivityPeriod = caps.inactivityPeriod;
            }
        }
        /// <summary>
        /// </summary>
        /// <param name="hand">Hand bone with which ball should be held</param>
        public FootballerPhysics(Node node, Vector3 gravity_,
            FootballerPhysCaps capabilities,
            bool isAcceleratingAuto = false, bool isFrictionNeededForMovement = true)
            : base(node, gravity_, isFrictionNeededForMovement)
        {
            ReadyToCatchBall = false;
            IsAutoAccelerating = isAcceleratingAuto;
            caps = capabilities;
            if (caps != null)
            {
                InactivityPeriod = caps.inactivityPeriod;
                body.Mass = caps.mass;
            }
        }

        public void Init(Node node, FootballerPhysCaps capabilities)
        {
            this.node = node;
            caps = capabilities;
            if (caps != null)
            {
                InactivityPeriod = caps.inactivityPeriod;
                Body.Mass = caps.mass;
            }

            Reset();
        }
        public void Accelerate(Vector3 direction)
        {
            if (!hasControl)
                return;
            AcceleratePrivate(direction);
        }

        private void AcceleratePrivate(Vector3 direction)
        {
            if (IsInnactive)
                return;
            direction.Normalize();
            if (!Spinning)
                TurnToDirection(direction); //rotate world transform

            float angle = 0;
            Vector3 vel = new Vector3();
            float z = GetFixedVel(ref vel);
            Vector3 origVel = vel;
            if (staticColNormal.Length() != 0)   //make sure we are on the ground
            {
                if (vel.Length() > 0)
                    angle = TMath.CalculateAngleAbs(direction, vel);
                if (angle > 3.0f)
                    vel = Vector3.Zero;
                else if (angle > 0.5f)
                    vel = direction * vel.Length() * caps.stopFactor;
                else if (angle > float.Epsilon)
                    vel = TMath.Projection(direction, vel);

                origVel = vel;
                vel.Z = z;
                body.LinearVelocity = Converter.ToJVector(vel);
                if (origVel.Length() < caps.maxSpeed)
                    Acceleration = direction * caps.quickness;
            }
        }

        public override void Update(GameTime time)
        {
            base.Update(time);
            if (caps.quickness > 10 || caps.maxSpeed > 20)
                return;
            if (ReadyToCatchBall && HasControl && Ball.Physics.Enabled && !HasBall && !catching)
                TryCatchBall(Ball);
            // We are not actualy limiting speed anywhere, if I'm not mistaken, but 
            // it seems that speed is never over maxSpeed too much. At least not more than maxSpeed+1
            if (!IsAutoAccelerating || body.LinearVelocity.Length() > caps.maxSpeed)
                Acceleration = Vector3.Zero;
#if !ANIMATION_TESTING
            if (HasControl)
                SetAnimation(GetFixedVel().Length());
#endif
            //HACK: Preventing physics tuneling through plane errors
            if (body.Position.Z < MinHeight)
            {
                JVector pos = body.Position;
                pos.Z = MinHeight;
                body.Position = pos;
                node.StoreBodyTransform(body);
            }
        }

        public void AttachBall(Ball ball_)
        {
            Debug.Assert(ParentObject != null);
            catching = false;
            HasBall = true;
            Bone handBone = AnimatedModel.FindBone(
                ((GraphicsComponentSkinned)ParentObject.Graphics).Bones, "HandREnd");
            Ball.AttachToHand(ParentObject, handBone, ParentObject);
            AudioEngine.PlayEffect("catch");
        }

        public void LooseBall()
        {
            DetachBall();
            JVector direction = new JVector(0.5f, 0.5f, 0);
            //ball.Physics.Body.LinearVelocity = direction * 2;
            // Making sure ball is not inside footballer collision box
            Ball.SetPosition(Body.Position + new JVector(0, 1f, 0));
            // Could just set velocity of ball rigid body
            Ball.Throw(Converter.ToJVector(Ball.Position) + direction * 2, (float)Math.PI / 6);
        }

        public void DetachBall()
        {
            Debug.Assert(HasBall);
            catching = false;
            Ball.Dettach();
            HasBall = false;
        }

        public bool TryCatchBall(Ball ball)
        {
            //TODO: Check where the ball hits
            float distance = (ball.Physics.Body.Position - Body.Position).Length();
            float cDistance = catchingDistance * ball.Physics.Body.LinearVelocity.Length() / catchingSpeed;
            if (distance <= cDistance)
            {
                CatchBall(ball);
                return true;
            }
            else
                return false;
        }

		public void PlayCatchAnimation()
		{
            AnimPlayer.PlayClip("catch");
            AnimPlayer.Looping = false;
            catching = true;
		}

        public void CatchBall(Ball ball)
        {
			PlayCatchAnimation();

            Timer timer = null;
            // Time has to be synced with animation and its speed
            timer = new Timer(new TimerCallback((obj) =>
                {
                    timer.Dispose();
                    AttachBall(ball);
                }), null, 450, Timeout.Infinite);
        }

        public void Fall()
        {
            HasControl = false;
            Acceleration = Vector3.Zero;

            //double prevBlendTime = AnimPlayer.BlendTime;
            // Delay stopping
            Timer timer = null;
            // GC doesn't get called on timer until it is used (timer.Dispose) in lambda expression
            timer = new Timer(new TimerCallback((obj) =>
                {
                    body.LinearVelocity = JVector.Zero;
                    timer.Dispose();
                    //AnimPlayer.BlendTime = prevBlendTime;
                }), null, 800, Timeout.Infinite);

            // Delay getting up
            Timer timer2 = null;
            timer2 = new Timer(new TimerCallback((obj) =>
                {
                    timer2.Dispose();
                    HasControl = true;
                }), null, 2000, Timeout.Infinite);

            // Set up falling animation
            //AnimPlayer.BlendTime = 0.0f;
            AnimPlayer.PlayClip("fall");
            AnimPlayer.Looping = false;
            AnimPlayer.SpeedFactor = 1;

            if (HasBall)
                LooseBall();

            Fell?.Invoke(this, EventArgs.Empty);
        }

        public void Spin(bool left)
        {
            Spinning = true;            

            Timer timer = null;
            double blendTime = AnimPlayer.BlendTime;
            //double speedFactor = AnimPlayer.SpeedFactor;
            timer = new Timer(new TimerCallback((obj) =>
                {
                    TurnToDirection(Converter.ToXNAVector(Body.LinearVelocity));
                    timer.Dispose();
                    Spinning = false;
                    AnimPlayer.BlendTime = blendTime;
                    //AnimPlayer.SpeedFactor = speedFactor;
                }), null, 300, Timeout.Infinite);

            AnimPlayer.BlendTime = 0;
            //AnimPlayer.SpeedFactor = 1;
            AnimPlayer.PlayClip("spin_right", left);
        }

        public void Charge(Vector3 direction)
        {
            JVector dir = Converter.ToJVector(direction);
            if (TMath.CalculateAngleAbs(dir, body.LinearVelocity) < Math.PI / 2)
                body.LinearVelocity = TMath.Projection(Converter.ToJVector(direction), body.LinearVelocity);
            else
                body.LinearVelocity = JVector.Zero;
            //Change velocity directly, because we want to charge forward with enough force
            body.LinearVelocity += Converter.ToJVector(direction) * (caps.quickness / 2);
            TurnToDirection(direction);

            // footballer is not on his legs so he shouldn't bounce of so easily
            //FIXME: doesn't seem to make much difference
            float prevMass = Body.Mass;
            body.Mass *= 10000;

            HasControl = false;
            Charging = true;

            // GC doesn't get called on timer until it is used (timer.Dispose) in lambda expression
            Timer timer = null;
            timer = new Timer(new TimerCallback((obj) =>
                {
                    Acceleration = Vector3.Zero;
                    body.LinearVelocity = JVector.Zero;
                    timer.Dispose();
                    body.Mass = prevMass;
                }), null, 800, Timeout.Infinite);
            Timer timer2 = null;
            double prevBlendTime = AnimPlayer.BlendTime;
            timer2 = new Timer(new TimerCallback((obj) =>
                {
                    Reset();
                    timer2.Dispose();
                    AnimPlayer.BlendTime = prevBlendTime;
                }), null, 1500, Timeout.Infinite);

            AnimPlayer.BlendTime = 0;
            AnimPlayer.PlayClip("tackle");
            AnimPlayer.Looping = false;
            AnimPlayer.SpeedFactor = 2;
        }

        public override void Reset()
        {
            HasControl = true;
            Charging = false;
            if (AnimPlayer != null)
            {
                AnimPlayer.PlayClip("wait", false, true);
                AnimPlayer.Looping = true;
                AnimPlayer.SpeedFactor = 1;
            }
            if (HasBall)
                DetachBall();

            base.Reset();
        }

        private void TurnToDirection(Vector3 direction)
        {
            float angle = TMath.CalculateAngleAbs(initialDirection, direction);
            JMatrix rot = JMatrix.CreateFromYawPitchRoll(0.0f, 0.0f, (direction.X > 0.0f) ? angle : -angle);
            body.Orientation = rot;
        }

        private void SetAnimation(float speed)
        {
            if (!hasControl || Spinning)
                return;
            //making ratios equal: speed/maxSpeed and (minSpeedFactor + speedFactor)/maxSpeedFactor
            AnimPlayer.SpeedFactor = minAnimSpeedFactor +
                ((maxAnimSpeedFactor - minAnimSpeedFactor) * (speed / maxSpeed));
            if (speed > float.Epsilon)//runThreshold)
            {
                if (!HasBall && AnimPlayer.CurrentClipName != "run")
                    AnimPlayer.PlayClip("run");
                else if (HasBall && AnimPlayer.CurrentClipName != "run_with_ball")
                    AnimPlayer.PlayClip("run_with_ball");
                AnimPlayer.Looping = true;
            }
            //else if (speed > float.Epsilon && AnimPlayer.CurrentClipName != "walk")
            //{
            //    AnimPlayer.PlayClip("walk");
            //    AnimPlayer.Looping = true;            
            //}
            else if (!HasBall && AnimPlayer.CurrentClipName != "wait")
            {
                AnimPlayer.PlayClip("wait");
                AnimPlayer.Looping = true;
            }
        }

        private JVector GetFixedVel()
        {
            JVector vel = body.LinearVelocity;
            vel.Z = 0;
            return vel;
        }

        private float GetFixedVel(ref Vector3 vel)
        {
            vel = Converter.ToXNAVector(body.LinearVelocity);
            float r = vel.Z;
            vel.Z = 0;
            return r;
        }

        public override void Collide(PhysicsComponent obj, JVector point1, JVector velBeforeCollision,
            JVector normal, float penetration)
        {
            JVector vel = Body.LinearVelocity;
            base.Collide(obj, point1, velBeforeCollision, normal, penetration);

            if (!CanFall)
                return;
            FootballerPhysics phys = obj as FootballerPhysics;
			// if don't have control or
			// if oponent is not charging and not running means he is fallen or waiting
			// in other words not ready to stop us
			if (phys != null && (!HasControl || (!phys.Charging && 
			                                     phys.Body.LinearVelocity.Length() < 5)))//&& (phys.HasBall || !HasControl))
                return;

            JVector currVel = Body.LinearVelocity;
            if (vel.Length() > 10 && currVel.Length() == 0)
                Fall(); //invokes Fell event
            else if (vel.Length() > 0 && currVel.Length() > 0 &&
                  Math.Abs(TMath.CalculateAngleAbs(vel, currVel)) >= Math.PI / 2)
                Fall();
            else if (phys != null)
                Collided?.Invoke(this, EventArgs.Empty);
        }
    }

    public class FootballerPhysCaps
    {
        public float quickness;  //acceleration
        public float maxSpeed;
        public float stopFactor;  //smaller - faster it stops
        public float mass;
		public int inactivityPeriod;
        /// <summary>
        /// </summary>
        /// <param name="quickness_>- acceleration in m/s^2"</param>
        /// <param name="maxSpeed_"></param>
		/// <param name="stopFactor_"<\param>
 		/// <param name="mass_"<\param>
		/// <param name="inactivityPeriod_>-how long footballer stays inactive after collision"<\param>
        public FootballerPhysCaps(float quickness_, float maxSpeed_, float stopFactor_, 
            float mass_ = 100, int inactivityPeriod_ = 400)
        {
            quickness = quickness_;
            maxSpeed = maxSpeed_;
            stopFactor = stopFactor_;
            mass = mass_;
			inactivityPeriod = inactivityPeriod_;
        }
        public FootballerPhysCaps(FootballerPhysCaps phys) : this(phys.quickness, phys.maxSpeed,
            phys.stopFactor, phys.mass, phys.inactivityPeriod)
        { }
    }
}
