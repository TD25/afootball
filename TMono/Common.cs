using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Jitter.LinearMath;
using Jitter.Dynamics;
using Microsoft.Xna.Framework.Graphics;
using Jitter.Collision.Shapes;
using Jitter.Collision;


namespace TMono
{
    public interface ILoadable
    {
        void LoadContent();
    }
    public interface IEntity : IGameComponent
    {
    }

    public class Converter
    {
        public static Matrix ToXNAMatrix(JMatrix matrix)
        {
            return new Matrix(matrix.M11,
            matrix.M12,
            matrix.M13,
            0.0f,
            matrix.M21,
            matrix.M22,
            matrix.M23,
            0.0f,
            matrix.M31,
            matrix.M32,
            matrix.M33,
            0.0f, 0.0f, 0.0f, 0.0f, 1.0f);
        }
        public static JMatrix ToJMatrix(Matrix m)
        {
            return new JMatrix(m.M11, m.M12, m.M13,
                m.M21, m.M22, m.M23,
                m.M31, m.M32, m.M33);
        }
        public static Vector3 ToXNAVector(JVector v)
        {
            return new Vector3(v.X, v.Y, v.Z);
        }
        public static JVector ToJVector(Vector3 v)
        {
            return new JVector(v.X, v.Y, v.Z);
        }
        public static Point ToPoint(Vector2 v)
        {
            return new Point((int)Math.Round(v.X),
                (int)Math.Round(v.Y));
        }
    }

    public struct Circle
    {
        public float radius;
        public Vector2 pos;

        public Circle(float radius, Vector2 pos)
        {
            this.radius = radius;
            this.pos = pos;
        }

        public bool Contains(Vector2 pos)
        {
            return Math.Pow((pos.X - this.pos.X), 2) + Math.Pow((pos.Y - this.pos.Y), 2) <=
                Math.Pow(radius, 2);
        }
    }

    public static class TMath
    {
        public static JVector Slide(JVector normal, JVector v)
        {
            return v - normal * DotProduct(normal, v);
        }
        public static Vector3 Slide(Vector3 normal, Vector3 v)
        {
            return v - normal * DotProduct(normal, v);
        }
        public static Vector3 Projection(Vector3 v, Vector3 projected)
        {
            float cos = DotProduct(v, projected) / (v.Length() * projected.Length());
            if (float.IsNaN(cos))
                throw new ArithmeticException("Not finite number");
            Vector3 v2 = v;
            v2.Normalize();
            return (cos * (projected.Length())) * v2;
        }
        public static JVector Projection(JVector v, JVector projected)
        {
            float cos = DotProduct(v, projected) / (v.Length() * projected.Length());
            if (float.IsNaN(cos))
                throw new ArithmeticException("Not finite number");
            JVector v2 = v;
            v2.Normalize();
            return (cos * (projected.Length())) * v2;
        }
        public static float DotProduct(JVector v1, JVector v2)
        {
            if (HasNaN(v1) || HasNaN(v2))
                throw new ArithmeticException("Not finite number");
            return v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z;
        }
        public static float DotProduct(Vector3 v1, Vector3 v2)
        {
            if (HasNaN(v1) || HasNaN(v2))
                throw new ArithmeticException("Not finite number");
            return v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z;
        }
        public static float DotProduct(Vector2 v1, Vector2 v2)
        {
            if (HasNaN(v1) || HasNaN(v2))
                throw new ArithmeticException("Not finite number");
            return v1.X * v2.X + v1.Y * v2.Y;
        }
        public static float CalculateAngleAbs(Vector3 v1, Vector3 v2)
        {
            float r = DotProduct(v1, v2) / (v1.Length() * v2.Length());
            //r shouldn't be more than 1 but there might be noise after prev calculation
            r = (Math.Abs(r) > 1.0f) ? Math.Sign(r) : r;
            if (float.IsNaN(r))
                throw new ArithmeticException("Not finite number in CalculateAngleAbs()");
            return (float)Math.Acos(r);
        }
        public static float CalculateAngleAbs(JVector v1, JVector v2)
        {
            float r = DotProduct(v1, v2) / (v1.Length() * v2.Length());
            //r shouldn't be more than 1 but there might be noise after prev calculation
            r = (Math.Abs(r) > 1.0f) ? Math.Sign(r) : r;
            if (float.IsNaN(r)) //should never be thrown
                throw new ArithmeticException("Not finite number in CalculateAngleAbs()");
            return (float)Math.Acos(r);
        }
        public static float CalculateAngleAbs(Vector2 v1, Vector2 v2)
        {
            float r = DotProduct(v1, v2) / (v1.Length() * v2.Length());
            //r shouldn't be more than 1 but there might be noise after prev calculation
            r = (Math.Abs(r) > 1.0f) ? Math.Sign(r) : r;
            if (float.IsNaN(r)) //should never be thrown
                throw new ArithmeticException("Not finite number in CalculateAngleAbs()");
            return (float)Math.Acos(r);
        }
        public static float CalculateAngle(Vector3 v1, Vector3 v2)
        {
            float r = DotProduct(v1, v2) / (v1.Length() * v2.Length());
            //r shouldn't be more than 1 but there might be noise after prev calculation
            r = (Math.Abs(r) > 1.0f) ? Math.Sign(r) : r;
            if (float.IsNaN(r)) //should never be thrown
                throw new ArithmeticException("Not finite number in CalculateAngleAbs()");
            return Math.Sign(r) * (float)Math.Acos(r);
        }
        public static Vector2 Rotate(Vector2 v, float radians)
        {
            Vector2 v1 = new Vector2(v.X * (float)Math.Cos(radians) - v.Y * (float)Math.Sin(radians),
                v.X * (float)Math.Sin(radians) + v.Y * (float)Math.Cos(radians));
            return v1;
        }
        public static bool HasNaN(Vector3 v)
        {
            return float.IsNaN(v.X) || float.IsNaN(v.Y) || float.IsNaN(v.Z);
        }
        public static bool HasNaN(Vector2 v)
        {
            return float.IsNaN(v.X) || float.IsNaN(v.Y);
        }
        public static bool HasNaN(JVector v)
        {
            return float.IsNaN(v.X) || float.IsNaN(v.Y) || float.IsNaN(v.Z);
        }
        public static JVector Normalized(JVector v)
        {
            JVector n = v;
            n.Normalize();
            return n;
        }
        public static Matrix CreateTransform(Quaternion rotation, Vector3 scale, Vector3 translation)
        {
            Matrix tr = Matrix.CreateFromQuaternion(rotation);
            tr.Scale = scale;
            tr.Translation = translation;
            return tr;
        }
        public static int Interpolate(int from, int to, float ratio)
        {
            return from + (int)Math.Round((to - from) * ratio);
        }

        public static void SolveQuadratic(double a, double b, double c, out double x1, out double x2)
        {
            //Quadratic Formula: x = (-b +- sqrt(b^2 - 4ac)) / 2a

            //Calculate the inside of the square root
            double discriminant = (b * b) - 4 * a * c;

            if (discriminant < 0)
            {
                //There is no solution
                x1 = double.NaN;
                x2 = double.NaN;
            }
            else
            {
                //Compute the value of each x
                //if there is only one solution, both x's will be the same
                double sqrt = Math.Sqrt(discriminant);
                x1 = (-b + sqrt) / (2 * a);
                x2 = (-b - sqrt) / (2 * a);
            }
        }
    }
}


