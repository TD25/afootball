using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jitter;
using Jitter.Dynamics;
using Jitter.LinearMath;
using Jitter.Collision;
using Jitter.DataStructures;
using Microsoft.Xna.Framework;

namespace TMono.Physics
{
    //TODO: Implement angular velocity
    public class DynamicPhysics : PhysicsComponent
    {
        private JVector acceleration;
        private JVector gravity;
        private JVector staticColNormal = JVector.Zero;
        private float bounciness = 0.5f;
        
        public JVector Acceleration
        {
            private set { acceleration = value; }
            get { return acceleration; }
        }
        /// <summary>
        /// Defines friction coeficient on all static bodies
        /// </summary>
        public float Friction
        { set; get; }
        public JVector Gravity
        {
            get { return gravity; }
            set { gravity = value; }
        }
        public JVector Force
        {
            get { return body.Force; }
            set { body.Force = value; }
        }
        public float Mass
        {
            get { return Body.Mass; }
            set
            {
                // Take out previous gravity force
                body.Force -= gravity * body.Mass;
                Body.Mass = value;
                Force += Gravity * body.Mass;
            }
        }
        /// <summary>
        /// 1 - means bounces of off static bodies with the same velocity
        /// Currently is only used with static bodies
        /// </summary>
        public float Bounciness
        {
            get { return bounciness; }
            set
            {
                if (value > 1f)
                    throw new ArgumentException("You don't want to set bounciness to more than 1");
                bounciness = value;
            }
        }

        public DynamicPhysics(RigidBody body_, Node node, JVector gravity)
            : base(body_, node)
        {
            Gravity = gravity;
        }
        public DynamicPhysics(Node node, JVector gravity) : base(node)
        {
            Gravity = gravity;
        }
        public void SetVelocity(Vector3 vec)
        {
            body.LinearVelocity = Converter.ToJVector(vec);
        }
        public void Impulse(JVector impulse)
        {
            body.ApplyImpulse(impulse);
        }
        public override void Update(GameTime time)
        {
            acceleration = gravity + body.Force * (1f / body.Mass);
            float seconds = (float)time.ElapsedGameTime.TotalSeconds;
            body.LinearVelocity += acceleration * seconds;

            if (staticColNormal.Length() > float.Epsilon && Friction > 0 &&
                    body.LinearVelocity.Length() > 0.5f)
                ApplyFriction(seconds);

            body.Position += body.LinearVelocity * seconds;
            staticColNormal = JVector.Zero;
            base.Update(time);
        }
        public override void Collide(PhysicsComponent obj, JVector point, JVector velBeforeCollision, 
            JVector normal, float penetration)
        {
            if (!Enabled)
                return;
            body.Position += normal * penetration;
            if (obj.Body.IsStatic)
                CollideWithStatic(point, normal, penetration);
            else
                CollideWithActive(obj.Body.Mass, velBeforeCollision, normal);
        }
        private void CollideWithStatic(JVector point, JVector normal, float penetration)
        {
            //body.Position += normal * penetration;
            staticColNormal += normal;

            JVector normalProj = (body.LinearVelocity.Length() > 0) ?
                TMath.Projection(normal, body.LinearVelocity) : JVector.Zero;
            float u = normalProj.Length();

            float v1 = u * Bounciness;

            JVector v = normal * v1;
            body.LinearVelocity = body.LinearVelocity - normalProj + v;
        }

        //TODO: prevent penetration. But then bounciness is not that good
        private void CollideWithActive(float mass, JVector vel, JVector normal)
        {
            JVector normalProj1 = (body.LinearVelocity.Length() > 0) ?
                TMath.Projection(normal, body.LinearVelocity) : JVector.Zero;
            JVector normalProj2 = (vel.Length() > 0) ?
                TMath.Projection(normal, vel) : JVector.Zero;
            float u2 = normalProj2.Length();
            float u1 = normalProj1.Length();

            float v1 = (u1 * (body.Mass - mass) + 2 * mass * u2) /
               (body.Mass + mass);
            JVector v = normal * v1;
            body.LinearVelocity = body.LinearVelocity - normalProj1 + v;
        }

        private void ApplyFriction(float seconds)
        {
            JVector projection = (acceleration.Length() > 0) ? 
                TMath.Projection(-1 * staticColNormal, acceleration) : JVector.Zero;
            // Projection can bee negative. Which means that friction could be applied even if body is
            // moving away from static body.
            float frictionF = body.Mass * projection.Length() * Friction;
            float a = frictionF / body.Mass;
            JVector dir = -1 * body.LinearVelocity;
            dir.Normalize();
            body.LinearVelocity += dir * a * seconds;
        }
    }
}
