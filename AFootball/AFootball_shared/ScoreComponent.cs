﻿using System;
using System.Collections.Generic;
using System.Text;
using TMono;
using Microsoft.Xna.Framework;
using TMono.MGUI;

namespace AFootball
{
    class ScoreComponent : GameComponent
    {
        private LabelComponent textBox;
        private GameObject player;

        public int Score
        {
            get
            {
                return (int)Math.Floor((player.Position.Y / 6));
            }
        }

        public ScoreComponent(LabelComponent text, GameObject player)
            : base(null)
        {
            textBox = text;
            this.player = player;
        }

        public override void Update(GameTime time)
        {
            textBox.Text = Score.ToString();
        }
    }
}
