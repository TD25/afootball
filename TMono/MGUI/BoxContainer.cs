﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;

namespace TMono.MGUI
{
    //FIXME: Currently it is not reacting to scaling and resizing of elements inside
    public class BoxContainer : GUIContainer
    {
		#region Types
		public class Element
		{
			public PositionedGUIComponent comp;
			// Space before this component
			// Does not include default space after all components
			public int additionalSpace;
            /// <summary>
            /// Alignment relative to this container.
            /// Not used if Orientation is horizontal
            /// </summary>
            public VerticalPosition vParentAlignment;
            /// <summary>
            /// Default alignment relative to this container.
            /// Not used if Orientation is vertical
            /// </summary>
            public HorizontalPosition hParentAlignment;
            /// <summary>
            /// SetAlignment is called with these parameters when adding component
            /// </summary>
            public VerticalPosition verticalPosition;
            /// <summary>
            /// SetAlignment is called with these parameters when adding component
            /// </summary>
            public HorizontalPosition horizontalPosition;

			public Element(PositionedGUIComponent comp, int additionalSpace, VerticalPosition vParentAlignment,
                HorizontalPosition hParentAlignment, VerticalPosition verticalPosition, HorizontalPosition horizontalPosition)
			{
				this.comp = comp;
				this.additionalSpace = additionalSpace;
                this.vParentAlignment = vParentAlignment;
                this.hParentAlignment = hParentAlignment;
                this.verticalPosition = verticalPosition;
                this.horizontalPosition = horizontalPosition;
			}
		}
		#endregion
        #region Fields
        private List<Element> components = new List<Element>();
        private int nextPos = 0;
        #endregion

        #region Properties
        /// <summary>
        /// Default alignment relative to this container.
        /// Not used if Orientation is horizontal
        /// </summary>
        public VerticalPosition VParentAlignment
        { set; get; }

        /// <summary>
        /// Default alignment relative to this container.
        /// Not used if Orientation is vertical
        /// </summary>
        public HorizontalPosition HParentAlignment
        { set; get; }

        /// <summary>
        /// SetAlignment is called with these parameters when adding component
        /// </summary>
        public VerticalPosition DefaultVerticalAlignment
        { set; get; }

        /// <summary>
        /// SetAlignment is called with these parameters when adding component
        /// </summary>
        public HorizontalPosition DefaultHorizontalAlignment
        { set; get; }

        /// <summary>
        /// Space between components
        /// </summary>
        public int Spaces
        { set; get; } = 0;

        /// <summary>
        /// Do you want components arranged verticaly or horizontaly
        /// </summary>
        public Orientation Orientation
        { private set; get; }

		public override Vector2 RealScale
		{
			get { return base.RealScale; }
			protected set
			{
				// We need to reposition elements if they are scaled
				// So here we are trying to reposition everytime Scaled event
				// of this container is called. We trust that Scaled is only called
				// in this property setter of base class
				base.RealScale = value;
				RepositionComponents();

			}
		}
        #endregion

        #region Constructors
        public BoxContainer(Orientation orientation)
        {
            Orientation = orientation;
        }
        public BoxContainer(Orientation orientation, GUIContainer parent) : base(parent)
        {
            Orientation = orientation;
        }
        public BoxContainer(Orientation orientation, Rectangle workingArea, GUIContainer parent = null)
           : base(workingArea, parent)
        {
            Orientation = orientation;
        }
        #endregion

        #region Methods
        public override void Remove()
        {
            //TODO: Implement removing and inserting
            throw new NotImplementedException();
        }
        /// <summary>
        /// Add element with default additionall spacing and alignment
        /// </summary>
        /// <param name="component">Has to be PositionedGUIComponent</param>
        public override void AddComponent(GUIComponent component, int drawOrder)
        {
            PositionedGUIComponent comp = component as PositionedGUIComponent;
            Debug.Assert(comp != null, "BoxContainer can't contain components which are not " +
                "PositionedGUIComponents");
			Element el = new Element(comp, 0, VParentAlignment, HParentAlignment, DefaultVerticalAlignment,
                DefaultHorizontalAlignment);
            AddComponentPrivate(el, drawOrder);
        }

        /// <summary>
        /// Add element with specified additional space and 
        /// default alignment
        /// </summary>
        /// <param name="space">How much space to add before placing component. 
        /// Total space from previous ends up being Spaces + space</param>
        public virtual void AddComponentWithSpace(PositionedGUIComponent component, int space, int drawOrder = -1)
        {
			Element el = new Element(component, space, VParentAlignment, HParentAlignment,
                DefaultVerticalAlignment, DefaultHorizontalAlignment);
            AddComponentPrivate(el, drawOrder);
        }

        public virtual void AddComponent(Element element, int drawOrder = -1)
        {
            AddComponentPrivate(element, drawOrder);
        }

        private void AddComponentPrivate(Element element, int drawOrder)
        {
			Debug.Assert(element.comp != null);
            element.comp.SetAlignmet(element.verticalPosition, element.horizontalPosition);
            base.AddComponent(element.comp, drawOrder);

            components.Add(element);

			PositionComponentAt(element, nextPos + element.additionalSpace);
        }

        private void PositionComponentAt(Element element, int pos)
        {
            var comp = element.comp;
            Vector2 position;
            if (Orientation == Orientation.HORIZONTAL)
            {
                position.X = pos;
                position.Y = GetVerticalSide(element.vParentAlignment);
                nextPos = (int)Math.Ceiling(position.X) + (int)Math.Ceiling(comp.Size.X) + Spaces;
            }
            else
            {
                position.Y = pos;
                position.X = GetHorizontalSide(element.hParentAlignment);
                nextPos = (int)Math.Ceiling(position.Y) + (int)Math.Ceiling(comp.Size.Y) + Spaces;
            }
            comp.Position = position;
        }

		/// <summary>
		/// Should be called if Spaces is changed and you want to apply it
		/// to elements already added
		/// </summary>
		public void RepositionComponents()
		{
			nextPos = 0;
			foreach (Element el in components)
				PositionComponentAt(el, nextPos + el.additionalSpace);
		}

        #endregion
    }
}
