﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;

namespace TMono.TControls
{
    /// <summary>
    /// Add to components to make it work or manually call update every frame
    /// Make sure it is called every time to get state before using methods like IsKeyPressed...
    /// </summary>
    public class TouchControls : Controls
    {
        private Point lastPos = Point.Zero;
        public const int touched = 255;
        private TouchCollection state;

        public TouchControls(Dictionary<byte, Keys> actions) : base(actions)
        {
        }

        public override void Update(GameTime gameTime)
        {
            state = TouchPanel.GetState();
            base.Update(gameTime);
        }

        public override bool IsKeyPressed(byte action)
        {
            if (!Actions.ContainsKey(action))
                return false;

            if (Actions[action] == (Keys)touched)
            {
                foreach (TouchLocation location in state)
                {
                    if (location.State == TouchLocationState.Pressed ||
                        location.State == TouchLocationState.Moved)
                    {
                        lastPos = Converter.ToPoint(location.Position);
                        return true;
                    }
                }
                return false;
            }
            return false;
        }
        public override bool IsKeyUp(byte action)
        {
            if (!Actions.ContainsKey(action))
                return false;

            //FIXME: Should probably return true when it is move not only when there is not touches at all
            if (Actions[action] == (Keys)touched)
            {
                return state.Count == 0;
            }
            else
                return false;
        }
        /// <summary>
        /// Checks if was just released
        /// </summary>
        public bool IsKeyReleased(byte action)
        {
             if (!Actions.ContainsKey(action))
                return false;

             // If you change the way you check if released here
             // consider doing the same thing in UniversalControls
            if (Actions[action] == (Keys)touched)
            {
                foreach (TouchLocation location in state)
                {
                    if (location.State == TouchLocationState.Released)
                        return true;
                }
                return false;
            }
            return false;                    
        }
        //optimize?
        public override Point GetPosition()
        {
            if (state.Count > 0)
                lastPos = Converter.ToPoint(state[0].Position);
            return lastPos;
        }
        protected override void CallHandler(byte action, EventHandler<ControlsEventArgs> handler)
        {
            ControlsEventArgs args;
            if (Actions[action] == (Keys)touched)
                args = new PosControlsEventArgs(action, lastPos);
            else
                args = new ControlsEventArgs(action);
            handler(this, args);
        }
    }


}
