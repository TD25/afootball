using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.ViewManagement;
using MonoGame.Framework;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace AFootball
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class GamePage : SwapChainBackgroundPanel
    {
        internal readonly AFootball game;

        public GamePage(string launchArguments)
        {
            this.InitializeComponent();

            // Create the game.

            game = XamlGame<AFootball>.Create(launchArguments, Window.Current.CoreWindow, swapChainPanel);

            Window.Current.VisibilityChanged += OnWindowVisibilityChanged;
        }

        void OnWindowVisibilityChanged(object sender, Windows.UI.Core.VisibilityChangedEventArgs e)
        {
            if (!e.Visible && game.CurrentState == AFootball.State.InGame)
                game.Pause();
        }
    }
}
