using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TMono
{
    public class TGame : Game
    {
        protected GraphicsDeviceManager graphics;
        protected SpriteBatch spriteBatch;
        protected Scene rootScene;
        protected Color clearColor = Color.CornflowerBlue;
        protected SamplerState objSamplerState;

		public TimeSpan TotalElapsedTime
		{
			private set;
			get;
		}

        public TGame()
        {
            graphics = new GraphicsDeviceManager(this);
            GraphicsComponent.Content = Content;
            GraphicsComponent.GDevice = GraphicsDevice;
            GraphicsComponent.GManager = graphics;
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            GameObject.Components = Components;
            spriteBatch = new SpriteBatch(GraphicsDevice);
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            rootScene.LoadContent();

            //rootScene will be updated one of the first
            //you can set different update order in derived class
            rootScene.UpdateOrder = 0;  
            Components.Add(rootScene);
            rootScene.Initialize();
            rootScene.Node.UpdateWorldTransforms(false);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
			TotalElapsedTime = gameTime.TotalGameTime;
            base.Update(gameTime);
        }

		/// <summary>
		/// Jump forward specified amount of time
		/// We do this by just calling Update with TargetElapsedTime enough times
		/// After updates caused by fast forwarding TotalGameTime in gameTime passed to Update 
		/// is reset back to real game time!
		/// </summary>
		public void FastForward(TimeSpan time)
		{
			GameTime gameTime = new GameTime(TotalElapsedTime, TargetElapsedTime);
			TimeSpan target = TotalElapsedTime + time;

			while (target - gameTime.TotalGameTime > TargetElapsedTime)
			{
				gameTime.ElapsedGameTime = TargetElapsedTime;
				gameTime.TotalGameTime += gameTime.ElapsedGameTime;
				Update(gameTime);
			}
		}

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            // Update all transforms in node hiearcy so that objects are drawn in the right places
            // This is done is draw method not update, because transforms might change multiple times
            // until we need them
            if (rootScene.Enabled)
                rootScene.Node.UpdateWorldTransforms(false);

            GraphicsDevice.Clear(clearColor);

            //TODO: Maybe have a struct with all these settings neede to draw objects
            // These are needed for 3d objects to draw
            GraphicsDevice.BlendState = BlendState.Opaque;
            GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            GraphicsDevice.SamplerStates[0] = objSamplerState;

            //keep it here
            // MGUI should be last to draw because it overrides settings set above with batch.Begin()
            base.Draw(gameTime); //GraphicsComponents get drawn here

            //rootScene.Draw(gameTime);
        }
    }
}
