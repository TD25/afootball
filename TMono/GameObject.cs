using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Jitter.Collision.Shapes;
using Jitter.Collision;
using Jitter.LinearMath;
using TMono.Physics;
using System.Diagnostics;

namespace TMono
{
    //TODO: GameObject should derive world transform from parent to enable
    //  creating proper scene graphs
    public class GameObject : IEntity, IPoolObject //IGameComponent - for initialize
    {
        private Matrix localTransform = Matrix.Identity;
        private PhysicsComponent phys;
        private GraphicsComponent3D graphics;
        private ControllerComponent controller;
        protected Node node;
        static protected GameComponentCollection components;

        static internal GameComponentCollection Components
        {
            set { components = value; }
        }
        /// <summary>
        /// Default order properties to set for components
        /// Set this before creating GameObjects
        /// </summary>
        static public int PhysicsUpdateOrder { set; get; } = 200;
        static public int ControllerUpdateOrder { set; get; } = 100;

        public Node Node
        {
            get { return node; }
            set { node = value; }
        }

        /// <summary>
        /// Position in local space
        /// </summary>
        public Vector3 Position
		{
			get
			{
				return node.LocalTransform.Translation;
			}
		}
        public string Type
        {
            get; set;
        }
        public PhysicsComponent Physics
        {
            get { return phys; }
            set
            {
                if (phys != null)
                    components.Remove(phys);
                phys = value;
                phys.UpdateOrder = PhysicsUpdateOrder;
                components.Add(phys);
            }
        }
        public ControllerComponent Controller
        {
            get { return controller; }
            set
            {
                if (controller != null)
                    components.Remove(controller);
                //Object can be without controller
                if (value != null)
                {
                    controller = value;
                    controller.UpdateOrder = ControllerUpdateOrder;
                    components.Add(controller);
                }
            }
        }
        public GraphicsComponent3D Graphics
        {
            get { return graphics; }
            set
            {
                if (graphics != null)
                    components.Remove(graphics);
                graphics = value;
                components.Add(graphics);
            }
        }

        /// <summary>
        /// Override this if you add new component types to derived classes
        /// If you want to hide object set visible property of graphics component to false
        /// </summary>
        public virtual bool Enabled
        {
            set
            {
                phys.Enabled = value;
                if (controller != null)
                    controller.Enabled = value;
            }
        }

        /// <summary>
        /// Object can be without controller so contrl can be null
        /// </summary>
        public GameObject(PhysicsComponent phys_, GraphicsComponent3D graphics_, 
            ControllerComponent contrl, Node node, string type) 
        {
            this.node = node;
            Controller = contrl;
            Graphics = graphics_;
            Physics = phys_;
            phys.Node = graphics.Node = node;  //make sure its the same object in all of them
            Type = type;
            this.node.Name = type;
            Enabled = false;    //Call initialize to enable
            Graphics.Visible = false;
        }

        protected GameObject(Node node, string type)
        {
            this.node = node;
            Type = type;
            this.node.Name = type;
        }

        /// <summary>
        /// Call this to free element in object pool (if it exists in one)
        /// </summary>
        public event FreeEventHandler Freed;

        public void Free()
        {
            Enabled = false;
            graphics.Visible = false;
            Freed?.Invoke();
        }

        /// <summary>
        /// Removes components from component list
        /// Call this if you won't need these components anymore
        /// Otherwise if you will want to reause it call Free()
        /// </summary>
        public virtual void RemoveComponents()
        {
            Free();
            components.Remove(graphics);
            components.Remove(phys);
            components.Remove(controller);
        }

        //override public void Update(GameTime time)
        //{
        //    //keep this in this order
        //    controller.Update(time);
        //    phys.Update(time);
        //}

        //public override void Draw(GameTime time)
        //{
        //    graphics.Draw(time);
        //}

		/// <summary>
		/// Initialize this instance by adding it to component collection, so that 
        /// update and draw methods get called, and components become active
        /// Should always be called when object is added to the scene
		/// </summary>
        public virtual void Initialize()
        {
            if (phys.Body != null)
                phys.Body.Tag = this;

            Enabled = true;
            graphics.Visible = true;
        }

        public void InitComponents(PhysicsComponent phys, GraphicsComponent3D graphics, ControllerComponent controller,
            Node node, string type)
        {
            this.node = node;
            this.Controller = controller;
            this.Graphics = graphics;
            this.Physics = phys;
            this.Physics.Node = graphics.Node = node;  //make sure its the same object in all of them
            Type = type;
        }

        public virtual void ParenEnabledChanged(object sender, EventArgs args)
        {
            bool val = ((Scene)sender).Enabled;
            Enabled = val;
        }

        public void SetPosition(JVector pos)
        {
            Physics.Body.Position = pos;
            node.StoreBodyTransform(Physics.Body);
        }

        /// <summary>
        /// Use this to set transform for physics and graphics correctly
        /// </summary>
        public void SetTransform(Matrix worldTransform)
        {
            node.LocalTransform = worldTransform;
            node.StoreInBody(Physics.Body);
        }
    }

    /// <summary>
    /// Instances of classes with this interface should describe types,
    ///     and work like a factory to create objects
    /// Derived classes should provide static LoadContent to load graphics
    ///     resources of the object
    /// </summary>
    public interface IGameObjectType 
    {
        GameObject CreateObject(Vector3 pos);
        GameObject CreateObject(Matrix world);
    }
}
