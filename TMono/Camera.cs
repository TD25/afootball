﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TMono.Physics;

namespace TMono
{
    public class Camera : GameComponent
    {
        Node node;
        Vector3 initialPos;
        Matrix proj;
        Vector3 upVector, target;
        PhysicsComponent phys;  //for moving camera
        static Viewport viewport;
        //FIXME: should sync with Projection?
        public static Viewport Viewport
        {
            get { return viewport; }
            set { viewport = value; }
        }
        public PhysicsComponent Phys
        {
            get { return phys; }
            set { phys = value; }
        }
        public Node Node
        {
            get { return node; }
        }
        public Matrix View
        {
            get
            {
                Vector3 translation = node.WorldTransform.Translation;
                return Matrix.CreateLookAt(translation,
                    target + (translation - initialPos), upVector);
            }
        }
        public Matrix Projection
        {
            get { return proj; }
            set { proj = value; }
        }
        public Camera(Vector3 target_, Vector3 upVector_, Vector3 pos, Matrix proj_,
            Node parent)
            : base(null)
        {
           proj = proj_;
           initialPos = pos;
           upVector = upVector_;
           target = target_;
           node = new Node("camera", parent, Matrix.CreateTranslation(pos));
           //phys = new CharacterPhysics(new RigidBody(new SphereShape(1)),
           //     world, Vector3.Zero);
        }
        public Camera(Vector3 target_, Vector3 upVector_, Vector3 pos, Matrix proj_,
            PhysicsComponent phys_, Node parent)
            : base(null)
        {
            proj = proj_;
            initialPos = pos;
            upVector = upVector_;
            target = target_;
            node = new Node("camera", parent, Matrix.CreateTranslation(pos));
            phys = phys_; 
        }
        override public void Update(GameTime time)
        {
            phys.Update(time);
        }
    }
}
