using System;
using System.Collections.Generic;
using TMono;
using Microsoft.Xna.Framework;
using Jitter.LinearMath;

namespace AFootball
{

	/// <summary>
	/// Coordinates action of all AI agents in game,
	///  	creates them using FootballerFactory
	/// </summary>
	class AITeam : GameComponent
	{
		FootballerFactory factory;
		List<GameObject> footballers;
		MainScene.RemoveFootballerDel removeFootballer;
		protected Vector3 oponentPos = Vector3.Zero;         //player position
        DifficultyStatus diffStatus;
        Queue<Stage> stages = new Queue<Stage>();
        Stage currentStage;
        int totalMsPassed = 0;
        FootballerType firstType;
        DifficultyStatus initialDiff;
        FootballerFactory initialFactory;

		/// <summary>
		/// Total in game time passed
		/// </summary>
		public TimeSpan TotalGameTime
		{
			get
			{
				return TimeSpan.FromMilliseconds(totalMsPassed);
			}
		}
        public float AgentRadius
        {
            set; get;
        }
		/// <summary>
		/// Gets or sets the oponent position.
		/// Update this before every Update call
		/// </summary>
		/// <value>The oponent position.</value>
		public Vector3 OponentPos
		{
			get { return oponentPos; }
			set 
			{ 
				oponentPos = value;
				factory.OponentPos = value;
			}
		}
		public int MaxDistanceBehind
		{
			set; get;
		}
        private Stage CurrentStage
        {
            set
            {
                currentStage = value;
                if (factory is FootballerFactory1)
                    ((FootballerFactory1)factory).CurrentStage = value;
            }
            get { return currentStage; }
        }

		public AITeam(List<GameObject> footballers, MainScene.RemoveFootballerDel removeDel,
		              FootballerFactory factory_,
                      DifficultyStatus status, int maxDistanceBehind_ = 100)
            : base(null)
		{
			this.footballers = footballers;
			removeFootballer = removeDel;
			MaxDistanceBehind = maxDistanceBehind_;

            initialDiff = status;
            initialFactory = factory_;
            firstType = initialFactory.GetType((0));

            AgentRadius = 2.0f;

            Reset();
		}

        public void Reset()
        {
            //keep this order:
            factory = initialFactory.Clone();
            diffStatus = new DifficultyStatus(initialDiff);
            factory.DifficultyStatus = diffStatus;
            InitStages(factory.GetType(0));
            diffStatus.SetCurrentDifficulty(factory.GetType(0));
            totalMsPassed = 0;
        }

        //be carefull not to make too big times between initial stages so that improvements don't get used up
        private void InitStages(FootballerType fType)
        {
            Stage.MaxCaps = new FootballerPhysCaps(10, 20, 0, 100);
            Stage.MinReactionInterval = 100;
            MultipleOponentStage.MaxNumberOfOponents = 4;

            List<ImprovementType> types4 = new List<ImprovementType> { ImprovementType.QUICKNESS,
                ImprovementType.SPEED, ImprovementType.REACTION, ImprovementType.NUMBERS };
            List<SpawnType> spawnTypes = new List<SpawnType> { SpawnType.ONE, SpawnType.NUMBERS_HORIZONTAL, 
				                                                        SpawnType.NUMBERS_VERTICAL };
            CurrentStage = new MultipleOponentStage(0, types4, fType, 300,
			                                        2500, spawnTypes, 1);
			stages.Enqueue(new MultipleOponentStage(15000, types4, null, 1000, 2500, spawnTypes, 2));
			CurrentStage.Init(diffStatus);
        }

		public override void Update(GameTime time)
		{
            if (diffStatus.Update(time))
                factory.Spawn(time);
			LoopThroughAgents(time);

            totalMsPassed += time.ElapsedGameTime.Milliseconds; //spawnInterval bigger than 1s
            if (stages.Count == 0)
                return;
            Stage newStage = stages.Peek();
            if (newStage.StartTime < totalMsPassed)
            {
				MultipleOponentStage prevStage = (MultipleOponentStage)CurrentStage;
                CurrentStage = stages.Dequeue();
				if (CurrentStage.FootballerTypes.Count == 0)
					currentStage.Init(diffStatus, prevStage);
				else
            	    currentStage.Init(diffStatus);
            }
		}

		//should be called every frame
		//override this to implement coordinated action
		//if overriding don't forget to call CheckAgent!!!
		virtual protected void LoopThroughAgents(GameTime time)
		{
            //agents.Clear();
            //GameObject best = null;
            //float bestDistance = float.MaxValue;
			for (int i = 0; i < footballers.Count; i++)
			{
				if (CheckAgent(footballers[i]))
					i--;
                //trying not to collide with teammated (does not work well)
                //else
                //{
                //    Vector2 pos = new Vector2(footballers[i].Position.X,
                //        footballers[i].Position.Y);
                //    float distance = (footballers[i].Position - oponentPos).Length();
                //    if (pos.Y > oponentPos.Y && distance < bestDistance)
                //    {
                //        bestDistance = distance;
                //        best = footballers[i];
                //    }
                //    else
                //    {
                //        foreach (var circle in agents)
                //        {
                //            if (circle.Contains(pos))
                //                AdjustMovement(footballers[i], circle, pos);
                //            else
                //                ((AI)footballers[i].Controller).CurrentStatus = AI.Status.ACTIVE;
                //        }
                //    }

                //    agents.Add(new Circle(AgentRadius, pos));
                //}
			}
		}
        //private void AdjustMovement(GameObject footballer, Circle objToAvoid, Vector2 pos)
        //{
        //    AI ai = (AI)footballer.Controller;
        //    Vector2 dir = ai.IntendedDirection2D;
        //    Vector2 dirToObj = objToAvoid.pos - pos;
        //    float angle = TMath.CalculateAngleAbs(dirToObj, dir);
        //    if (angle < (Math.PI / 2))
        //    {
        //        angle = (float)Math.PI / 2 - angle;
        //        if (dir.X > 0)
        //            angle = -angle;
        //        Vector2 newDir = TMath.Rotate(dir, angle);
        //        ai.OverrideDirection(new JVector(newDir.X, newDir.Y, 0));
        //    }
        //}
        
		//can free if too far behind
		//if removes returns true, else false
		private bool CheckAgent(GameObject agent)
		{
			Vector3 distance = agent.Position - oponentPos;
			if (distance.Length() >= MaxDistanceBehind && distance.Y < 0)
			{
				removeFootballer(agent);
				return true;
			}
			else
				return false;
		}
	}

    //TODO: add types of tactics?
    public enum ImprovementType
    {
        SPEED, QUICKNESS, MASS, REACTION, NUMBERS
    }

    class DifficultyStatus 
    {
        int timeSinceSpawn;
        int currentDiff;
        int spawnInterval;
        int gainInterval;

        int timeSinceGain = 0;

        //how much time between adding 1 to currentDiff
        public int GainInterval
        {
            set { gainInterval = value; }
            get { return gainInterval; }
        }
        public int SpawnInterval
        {
            set { spawnInterval = value; }
            get { return spawnInterval; }
        }
        public int CurrentDifficulty
        {
            get { return currentDiff; }
        }

        public DifficultyStatus(int spawnInterval, int gainInterval, 
            int initialSpawnDelay)
        {
            this.spawnInterval = spawnInterval;
            this.gainInterval = gainInterval;

            timeSinceSpawn = this.spawnInterval - initialSpawnDelay;
            timeSinceGain = 0;
        }

        public DifficultyStatus(DifficultyStatus status)
        {
            spawnInterval = status.spawnInterval;
            gainInterval = status.gainInterval;
            timeSinceSpawn = status.timeSinceSpawn;
            timeSinceGain = 0;
        }
        //should be called once when initialising
        //TODO: include mass in equation
        public void SetCurrentDifficulty(FootballerType type)
        {
            FootballerPhysCaps phys = type.PhysicsCaps;
            currentDiff = (int)(((double)Stage.MinReactionInterval / type.PropertiesAI.reactionPeriod)*100
                + phys.maxSpeed + phys.quickness);
        }
        public static int CalculateDiff(FootballerType type)
        {
            FootballerPhysCaps phys = type.PhysicsCaps;
            int diff = (int)(((double)Stage.MinReactionInterval / type.PropertiesAI.reactionPeriod)*100
                + phys.maxSpeed + phys.quickness);
            return diff;
        }
        public static int GetSpawnIntervalWeight(int interval)
        {
            return (int)((4000.0 / interval) * 10);
        }
		public static int GetNumbersWeight(int footballerCount)
		{
			return 5 * footballerCount;
		}
        public static int GetSpeedImprovement(int ammount)
        {
            return ammount;
        }
        public static int GetMassImprovement(int ammount)
        {
            return ammount;
        }
        public static int GetQuicknessImprovement(int ammount)
        {
            return ammount;
        }
        public static int GetReactionImprovement(int ammount)
        {
            return -10 * ammount;
        }
        public static int GetNumbersImprovement(int ammount)
        {
            return ((ammount / 10) >= 1) ? 1 : 0;
        }
        //true means you have to spawn
        public bool Update(GameTime time)
        {
            timeSinceSpawn += time.ElapsedGameTime.Milliseconds;
            timeSinceGain += time.ElapsedGameTime.Milliseconds;
            if (timeSinceGain > gainInterval)
            {
                timeSinceGain = 0;
                currentDiff++;
            }

            if (timeSinceSpawn > spawnInterval)
            {
                timeSinceSpawn = 0;
                return true;
            }
            else
                return false;
        }
    }
}

