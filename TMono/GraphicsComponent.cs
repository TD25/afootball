﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace TMono
{
    public abstract class GraphicsComponent : DrawableGameComponent
    {
        public static ContentManager Content
        { set; get; }
        public static GraphicsDevice GDevice
        { set; get; }
        public static GraphicsDeviceManager GManager
        { set; get; }

        public GraphicsComponent() : base(null) { }
    }
}
