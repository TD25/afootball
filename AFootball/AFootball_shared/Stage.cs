﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace AFootball
{
	class Stage
	{
		int startTime; //in ms
		int gainInterval, spawnInterval;
		protected List<ImprovementType> improvements;
		protected List<FootballerType> types = new List<FootballerType>();
		static protected Random rnd = new Random();

		public List<FootballerType> FootballerTypes
		{
			get { return types; }
		}
		public static FootballerPhysCaps MaxCaps
		{
			set; get;
		}
		public static int MinReactionInterval
		{
			set; get;
		}

		public int GainInterval
		{
			get { return gainInterval; }
		}
		public int SpawnInterval
		{
			get { return spawnInterval; }
		}
		public int StartTime
		{
			get { return startTime; }
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="T:AFootball.Stage"/> class.
		/// You can give baseType null but then be sure to call Init with list of footballer types
		/// </summary>
		public Stage(int startTime, List<ImprovementType> improvements, FootballerType baseType,
			int gainInterval, int spawnInterval)
		{
			this.startTime = startTime;
			this.improvements = improvements;
			this.gainInterval = gainInterval;
			this.spawnInterval = spawnInterval;
			if (baseType != null)
				types.Add(baseType);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="T:AFootball.Stage"/> class.
		/// You can give baseType null but then be sure to call Init with list of footballer types
		/// </summary>        
		public Stage(int startTime, List<ImprovementType> improvements, FootballerType baseType,
			DifficultyStatus status) : this(startTime, improvements, baseType, status.GainInterval,
				status.SpawnInterval)
		{
		}
		//call before starting stage
		public void Init(DifficultyStatus status)
		{
			status.GainInterval = gainInterval;
			status.SpawnInterval = spawnInterval;
			Debug.Assert(types.Count > 0, "Stage have to be initialized with list of footballer types");
		}
		/// <summary>
		/// Init using previous param name="stage" to initialize
		/// footballer types
		/// </summary>
		public virtual void Init(DifficultyStatus status, Stage stage)
        {
			this.types = stage.types;
			Init(status);
        }

        protected void Improve(FootballerType fb, int ammount, ImprovementType type,
            List<ImprovementType> list)
        {
            switch (type)
            {
                //FIXME:
                //case ImprovementType.MASS:
                //    if (types[0].PhysicsCaps.mass > MaxCaps.mass)
                //          improvements.Remove(ImprovementType.MASS);
                //    else
                //      types[0].PhysicsCaps.mass += 
                //          Math.Min(DifficultyStatus.GetMassImprovement(ammount), MaxCaps.mass);
                //    break;
                case ImprovementType.QUICKNESS:
                    if (fb.PhysicsCaps.quickness >= MaxCaps.quickness)
                        list.Remove(ImprovementType.QUICKNESS);
                    else
                    {
                        fb.PhysicsCaps.quickness += DifficultyStatus.GetQuicknessImprovement(ammount);
                        fb.PhysicsCaps.quickness = Math.Min(MaxCaps.quickness,
                            fb.PhysicsCaps.quickness);
                    }
                    break;
                case ImprovementType.SPEED:
                    if (fb.PhysicsCaps.maxSpeed >= MaxCaps.maxSpeed)
                        list.Remove(ImprovementType.SPEED);
                    else
                    {
                        fb.PhysicsCaps.maxSpeed += DifficultyStatus.GetSpeedImprovement(ammount);
                        fb.PhysicsCaps.maxSpeed = Math.Min(MaxCaps.maxSpeed,
                            fb.PhysicsCaps.maxSpeed);
                    }
                    break;
                case ImprovementType.REACTION:
                    if (fb.PropertiesAI.reactionPeriod <= MinReactionInterval)
                        list.Remove(ImprovementType.REACTION);
                    else
                    {
                        fb.PropertiesAI.reactionPeriod +=
                            DifficultyStatus.GetReactionImprovement(ammount);
                        fb.PropertiesAI.reactionPeriod = Math.Max(MinReactionInterval,
                            fb.PropertiesAI.reactionPeriod);
                    }
                    break;
            }
        }
        public virtual ImprovementType Improve(int ammount)
        {
            ImprovementType type = ChooseImprovement(improvements);
            Improve(types[0], ammount, type, improvements);
            return type;
        }
        public virtual List<FootballerType> ChooseTypeToSpawn(out SpawnType type)
        {
			type = SpawnType.ONE;
            return new List<FootballerType> { types[0] };
        }
        protected ImprovementType ChooseImprovement(List<ImprovementType> list)
        {
            int max = list.Count; 
            return list[rnd.Next(max)];
        }
    }

    public enum SpawnType
    {
        ONE,
        NUMBERS_VERTICAL,
		NUMBERS_HORIZONTAL
    }

    class MultipleOponentStage : Stage
    {
        List<SpawnType> spawnTypes;
        //improvement list in base class is general this one is individual for each f type
		// number of individual improvements is one less than types.Count, because we don't
		// improve the base type (index 0).
        List<List<ImprovementType>> individualImprovements = new List<List<ImprovementType>>();
        int oponentCount;

        public static int MaxNumberOfOponents
        {
            set; get;
        }

		public MultipleOponentStage(int startTime, List<ImprovementType> improvements,
			FootballerType baseType, int gainInterval, int spawnInterval,
			List<SpawnType> spawnTypes, int initialOponentCount = 2)
			 : base(startTime, improvements, baseType, gainInterval, spawnInterval)
		{
			oponentCount = initialOponentCount;
			this.spawnTypes = spawnTypes;
			var list = new List<ImprovementType>(improvements);
			if (list.Contains(ImprovementType.NUMBERS))
				list.Remove(ImprovementType.NUMBERS);
			//we save first oponent stored in base constructor
			if (baseType != null)
			{
				for (int i = 0; i < MaxNumberOfOponents; i++)
				{
					types.Add(new FootballerType(baseType));
					individualImprovements.Add(new List<ImprovementType>(list));
				}
			}
        }
        public MultipleOponentStage(int startTime, List<ImprovementType> improvements, FootballerType baseType,
            DifficultyStatus status, List<SpawnType> spawnTypes, int initialOponentCount = 2) 
            : this(startTime, improvements, baseType, status.GainInterval, status.SpawnInterval,
                  spawnTypes, initialOponentCount)
        {
        }

		public override void Init(DifficultyStatus status, Stage stage)
		{
			base.Init(status, stage);
			MultipleOponentStage mStage = stage as MultipleOponentStage;
			Debug.Assert(mStage != null, "If you are using Init(DifficultyStatus, Stage)" +
						 "method to initialize stage, you have to pass stage of the exact same type");

			var list = new List<ImprovementType>(improvements);
			if (list.Contains(ImprovementType.NUMBERS))
				list.Remove(ImprovementType.NUMBERS);			
			// Some abilities might have already reached max value, but we still add
			// whole list of improvements to each footballer type.
			// We check if max values are reached in improve methods anyway
			for (int i = 0; i < MaxNumberOfOponents; i++)
				individualImprovements.Add(new List<ImprovementType>(list));
		}

        public override ImprovementType Improve(int ammount)
        {
            //decide if individual or for all
            ImprovementType impr = ChooseImprovement(improvements);
            if (impr == ImprovementType.NUMBERS) //means general
            {
                if (oponentCount >= MaxNumberOfOponents)
                    improvements.Remove(impr);
                else
                {
                    oponentCount += DifficultyStatus.GetNumbersImprovement(ammount);
                    oponentCount = Math.Min(MaxNumberOfOponents, oponentCount);
                }
            }
            else
            {
                //decide which footballer to update
                int ind = rnd.Next(1, oponentCount + 1);
                if (individualImprovements[ind - 1].Count > 0)
                {
                    ImprovementType im = ChooseImprovement(individualImprovements[ind - 1]);
                    Improve(types[ind], ammount, im, individualImprovements[ind - 1]);
                }
            }
            //return first impr
            return impr;
        }

        public override List<FootballerType> ChooseTypeToSpawn(out SpawnType type)
        {
            int max = spawnTypes.Count;
            int ind = rnd.Next(max);
            SpawnType spawn = spawnTypes[ind];

            List<FootballerType> selection = new List<FootballerType>();
            switch (spawn)
            {
                case SpawnType.ONE:
                    //max = oponentCount+1;
                    //ind = rnd.Next(1, max);
                    //selection.Add(types[ind]);
                    selection.Add(types[1]); //choosing the oldest
                    break;
                case SpawnType.NUMBERS_VERTICAL:
				case SpawnType.NUMBERS_HORIZONTAL:	
                    for (int i = 1; i <= oponentCount; i++)
                        selection.Add(types[i]);
                    break;
            }
			type = spawn;
            return selection;
        }
    }
}
