using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TMono
{
    /// <summary>
    /// Bones in this model are represented by this class, which
    /// allows a bone to have more detail associatd with it.
    /// </summary>
	public class Bone : Node
    {
        #region Fields
        /// <summary>
        /// The bind transform is the transform for this bone
        /// as loaded from the original model. It's the base pose.
        /// I do remove any scaling, though?
        /// </summary>
        private Matrix bindTransform = Matrix.Identity;
        #endregion 

        #region Properties
        /// <summary>
        /// The bone bind transform
        /// </summary>
        public Matrix BindTransform { get {return bindTransform;} }

        public int Index
        { private set; get; }

        /// <summary>
        /// Inverse of absolute bind transform for skinnning
        /// </summary>
        public Matrix SkinTransform { get; private set; }

        #endregion

        #region Operations

        /// <summary>
        /// Constructor for a bone object
        /// </summary>
        /// <param name="name">The name of the bone</param>
        /// <param name="bindTransform">The initial bind transform for the bone</param>
        /// <param name="parent">A parent for this bone</param>
        public Bone(string name, Matrix bindTransform, Bone parent, int index)
            : base(name, parent, bindTransform)
        {
            Index = index;
            this.bindTransform = bindTransform;

            if (parent != null)
                parent.AddChild(this);

            // Set the skinning bind transform
            // That is the inverse of the absolute transform in the bind pose
            // local transform is already set as bindTransform in base constructor
            ComputeWorldTransform();
            SkinTransform = Matrix.Invert(WorldTransform);
        }
        #endregion

    }
}
