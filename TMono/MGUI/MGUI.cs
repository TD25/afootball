﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;

//TODO: Create GUIContainer which can take input

namespace TMono.MGUI
{
    public class IncorrectDrawOrder : MGUIException
    {
        public IncorrectDrawOrder() :
            base("Draw order of gui component cannot be bigger than drawOrderEnd - 1 " +
                "or lower than drawOrderStart+1. You can set those in MGUI constructor")
        { }
    }
    public class MGUIException : Exception
    {
        public MGUIException(string message)
            : base(message) { }
    }

    public enum VerticalPosition
    {
        TOP, BOTTOM, CENTER
    }
    public enum HorizontalPosition
    {
        LEFT, RIGHT, CENTER
    }
    public enum Orientation
    {
        HORIZONTAL, VERTICAL        
    }
    public struct Position
    {
        public HorizontalPosition hPos;
        public VerticalPosition vPos;
        public Position(HorizontalPosition horizontalPosition, VerticalPosition verticalPosition)
        {
            hPos = horizontalPosition;
            vPos = verticalPosition;
        }
    }

    /// <summary>
    /// Makes sense to have only one instance of this class
    /// Gui element container which begins and ends drawing with spritebatch
    /// and manages draw order of elements
    /// </summary>
    public class MGUI : GUIContainer
    {
        #region Fields
        private int drawOrderStart;
        private int drawOrderEnd;
        //TODO: Allow modyfying
        internal RasterizerState rasterizerState = new RasterizerState() { ScissorTestEnable = true };
        
        //TODO: graphics settings
        #endregion

        #region Properties
        #endregion

        #region Constructors
        /// <summary>
        /// drawOrderStart and drawOrderEnd define when gui drawing begins and ends
        /// This must be set once before calling Initialize
        /// </summary>
        /// <param name="components">GameComponent collection of Game. Gui components will be added to it</param>
        /// <param name="drawArea">Area in which GUI components will be drawn, usually whole window area</param>
        /// <param name="drawOrderStart">All elements in this gui would have at least this draw order</param>
        /// <param name="drawOrderEnd">Biggest draw order possible for gui element</param>
        public MGUI(SpriteBatch batch, GameComponentCollection components, Rectangle drawArea, int drawOrderStart = 10000,
            int drawOrderEnd = 20000) : base(drawArea)
        {
            Batch = DrawEnder.batch = batch;
            this.drawOrderStart = drawOrderStart;
            //FIXME: Do something about drawOrderEnd. Currently not used
            this.drawOrderEnd = drawOrderEnd;
            Components = components;
            TextureComponent.batch = LabelComponent.batch = batch;
            scalable = false;
            GUIContainer.RasterizerState = rasterizerState;
            SeparateBatch = true;
        }
        #endregion

        #region Methods
        public override void Initialize()
        {
            DrawOrder = drawOrderStart;
            Components.Add(this);
            SetDrawOrder = SetDrawOrder_;
            base.Initialize();
        }

        private void SetDrawOrder_(GUIComponent component, int drawOrder)
        {
            if (drawOrder == -1)
                drawOrder = drawOrderStart+1;
            else if (drawOrder > drawOrderEnd-1 || drawOrder < drawOrderStart+1)
                throw new IncorrectDrawOrder();
            component.DrawOrder = drawOrder;
        }

        /// <summary>
        /// Call Initialize again to add back
        /// </summary>
        public override void Remove()
        {
            base.Remove();
        }

        /// <summary>
        /// Scales gui elements so that they look more like they would look like in 
        ///  specified virtual resolution, preserving aspect ratio of those elements.
        /// WorkingArea has to be set for this to work.
        /// </summary>
        public void ScaleForResolution(int virtualWidth, int virtualHeight)
        {
            //TODO: Allow scaling without preserving aspect ratio of elements
            // Usefull for scalable containers. You can do this by giving preserveAspectRatio
            // property to GUIComponent class, and giving it responsibility of scaling properly

            int realWidth = Math.Max(WorkingArea.Size.X, WorkingArea.Size.Y);
            int realHeight = Math.Min(WorkingArea.Size.X, WorkingArea.Size.Y);
            int vWidth = Math.Max(virtualWidth, virtualHeight);
            int vHeight = Math.Min(virtualWidth, virtualHeight);

            float widthFactor = (float)realWidth / (float)vWidth;
            float heightFactor = (float)realHeight / (float)vHeight;

            float scale;
            // We have to scale x and y by same factor in order to preserve aspect ratio of textures
            // So just pick factor which scale the most
            if (Math.Abs(1 - widthFactor) > Math.Abs(1 - heightFactor))
                scale =  widthFactor;
            else
                scale = heightFactor;

            if (Math.Abs(1 - scale) > float.Epsilon)
                RealScale = Scale * new Vector2(scale, scale);
        }
        #endregion
    }
}
