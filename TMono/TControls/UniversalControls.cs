﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;

namespace TMono.TControls
{
    /// <summary>
    /// Controls for universal apps like WINDOWS_UAP,
    /// where we could get touch as well keyboard input
    ///
    /// GetPosition returns mouse position
    /// </summary>
    public class UniversalControls : PCControls
    {
        private TouchCollection touchState;

        public UniversalControls(Dictionary<byte, Keys> actions) : base(actions)
        {
            Debug.Assert(TouchControls.touched == PCControls.leftMouseButton,
                "UniversalControls class depends on TouchControls.touched and PCControls.leftMouseButton being the same");
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            touchState = TouchPanel.GetState();
        }

        public override bool IsKeyPressed(byte action)
        {
            // if left mouse button or touched then returns true
            // TouchControls.touched is the same as PCControls.leftMouseButton
            // trusting that action exists in dictionary (responsibility of Controls class)
            if (Actions[action] == (Keys)leftMouseButton)
            {
                bool pressed = base.IsKeyPressed(action);
                if (pressed)
                    return true;
                // Here we do the same thing as in TouchControls.IsKeyPressed method
                foreach (TouchLocation location in touchState)
                {
                    if (location.State == TouchLocationState.Pressed ||
                        location.State == TouchLocationState.Moved)
                    {
                        lastMousePos = Converter.ToPoint(location.Position);
                        return true;
                    }
                }
                return false;
            }
            else
                return base.IsKeyPressed(action);
        }
        public override bool IsKeyUp(byte action)
        {
            if (Actions[action] == (Keys)leftMouseButton)
            {
                // Both leftmouse button and touched has to be released in order to return true
                bool released = base.IsKeyUp(action);
                // Checking if touch is released the same way as in TouchControls
                if (touchState.Count == 0 && released)
                    return true;
                else
                    return false;
            }
            else
                return base.IsKeyUp(action);
        }
    }
}
