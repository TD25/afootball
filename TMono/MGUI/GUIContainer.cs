﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;

namespace TMono.MGUI
{
    public class GUIContainer : PositionedGUIComponent
    {
        #region Fields
        //FIXME:? currently if child component is not inside it is still visible (if inside window area)
        private Rectangle area;
        private bool inheritParentSize = false;
        //Use EventHandler instead of this?
        internal delegate void RemovedDel();
        //wantedOrder = -1 means set default order
        protected delegate void SetDrawOrderDel(GUIComponent component, int wantedOrder = -1);
        private Rectangle localArea;
        protected bool scalable = true;
        private int lastDrawOrder;   // Biggest draw order in container
        private bool separetBatch = false;
        private DrawEnder ender;
        #endregion

        #region Properties
        internal static RasterizerState RasterizerState
        { set; get; }
        /// <summary>
        /// If true, then batch.begin(), batch.end() before starting and ending drawing
        /// components inside
        /// The less of these call the better, because of effieciency
        /// But if you want to clip components which do not fit, that's the only way
        /// Just set this to true and components will be clipped according to WorkingArea
        /// </summary>
        public bool SeparateBatch
        {
            get { return separetBatch; }
            set
            {
                separetBatch = value;
                if (separetBatch)
                {
                    SetEnderDrawOrder();
                    ender.Enabled = ender.Visible = true;
                    if (!Components.Contains(ender))
                        Components.Add(ender);
                }
                else
                    ender.Enabled = ender.Visible = false;

            }
        }
        internal static SpriteBatch Batch
        { set; get; }
        public Texture2D BackgroundTexture
        { set; get; }
        /// <summary>
        /// Make sure this container fits container we are in
        /// </summary>
        public bool MakeFit
        { set; get; } = false;
        public bool InheritParentSize
        {
            get { return inheritParentSize; }
            set
            {
                inheritParentSize = value;
                if (inheritParentSize && Parent != null)
                    area.Size = Parent.area.Size;
            }
        }
        public Rectangle LocalArea
        {
            get { return localArea; }
            //TODO: Implement setter
        }
        /// <summary>
        /// Real (global) position and size
        /// </summary>
        public Rectangle WorkingArea
        {
            get { return area; }
            // Use SetSize to set size
            // Don't want to set here because area is now used as in global coordinate system
            // Not tested:
            //set 
            //{
            //    Vector2 prevSize = Size;
            //    Vector2 prevPos = Position;
            //    area = value;
            //    //Not using this.Position, because we interpret value as global
            //    base.Position = new Vector2(area.Location.X, area.Location.Y);
            //    if (prevSize != Size)
            //        Resized?.Invoke(this, EventArgs.Empty);
            //    if (base.Position != prevPos)
            //        Moved?.Invoke(this, EventArgs.Empty);
            //}
        }
        public override Vector2 Size
        {
            get
            {
                return new Vector2(area.Size.X, area.Size.Y); 
            }
        }
        public override Vector2 Position
        {
            get { return new Vector2(LocalArea.Location.X, LocalArea.Location.Y); }
            set
            {
                Point prev = area.Location;
                localArea.Location = new Point((int)value.X, (int)value.Y);
                base.Position = value;
                if (area.Location != prev)
                    Moved?.Invoke(this, EventArgs.Empty);
            }
        }
        public override Vector2 Scale
        {
            get { return base.Scale; }
            set
            {
                //TODO: set new size?
                base.Scale = value;
            }
        }
        public override Vector2 RealPosition
        {
            protected set
            {
                base.RealPosition = value;
                // Assuming value was set to position
                area.Location = new Point((int)value.X, (int)value.Y);
            }
            get { return base.RealPosition; }
        }
        public override Vector2 RealScale
        {
            get { return base.RealScale; }
            protected set
            {
                //Not sure if we need to call this here (probably for children only)
                SetRealScale(value);
                if (scalable)
                {
                    SetSize(new Point((int)Math.Round(localArea.Size.X * RealScale.X),
                          (int)Math.Round(localArea.Size.Y * RealScale.Y)), false);
                    Position = Position;
                }
                Scaled?.Invoke(this, EventArgs.Empty);
            }
        }
        public bool Scalable
        {
            get { return scalable; }
            // Do not reset what's already scaled on purpose,
            // we might want to scale once and then stop scaling
            set { scalable = value; }
        }
        #endregion

        #region Events&Types
        internal class DrawEnder : DrawableGameComponent
        {
            internal static SpriteBatch batch;

            // For saving previous cliping rectangle after drawing all containers
            public Rectangle Rect2Reset 
            { set; get; }
            public RasterizerState Rasterizer2Reset
            { set; get; }

            public DrawEnder() : base(null)
            {
            }

            public override void Draw(GameTime gameTime)
            {
                batch.End();                
                batch.GraphicsDevice.ScissorRectangle = Rect2Reset;
                batch.GraphicsDevice.RasterizerState = Rasterizer2Reset;
            }
        }
        internal class ComponentAddedEventArgs : EventArgs
        {
            public GUIComponent component;

            public ComponentAddedEventArgs(GUIComponent component)
            {
                this.component = component;
            }
        }
        protected SetDrawOrderDel SetDrawOrder { set; get; }

        //FIXME: Unsuscribe children when they are removed,
        // check all event in MGUI
        internal event RemovedDel Removed;
        internal event EventHandler Moved;
        internal event EventHandler Resized;
        internal event EventHandler Scaled;
        internal event EventHandler ComponentAdded;
        #endregion

        #region Constructors
        /// <summary>
        /// If using this or default constructor you have to specify global coordinates for components added
        /// (instead of relative), unless you don't need to specify position for the kind of component you wish to add
        /// </summary>
        public GUIContainer(GUIContainer parent) : base(parent, new Vector2(0, 0))
        {
            area = Rectangle.Empty;
            ender = new DrawEnder();
        }
        public GUIContainer() : base(null, new Vector2(0, 0))
        {
            ender = new DrawEnder();
        }
        public GUIContainer(Rectangle workingArea, GUIContainer parent = null)
           : base(parent, new Vector2(workingArea.Location.X, workingArea.Location.Y))
        {
            localArea = workingArea;
            area = workingArea;
            area.Location = new Point((int)RealPosition.X, (int)RealPosition.Y);
            ender = new DrawEnder();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Component inherits visibility of this container
        /// </summary>
        public virtual void AddComponent(GUIComponent component)
        {
            AddComponent(component, -1);
        }

        public override void Draw(GameTime gameTime)
        {
            if (SeparateBatch)
                BeginDraw();                
            if (BackgroundTexture != null)
                Batch.Draw(BackgroundTexture, area, Color.White);                
        }

        private void BeginDraw()
        {
            //Set up the spritebatch to draw using scissoring (for what doesn't fit into containers)
            //TODO: Make properties where these are set
            ender.Rasterizer2Reset = Batch.GraphicsDevice.RasterizerState;
            Batch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend,
                      null, null, RasterizerState);
            ender.Rect2Reset = Batch.GraphicsDevice.ScissorRectangle;
            Batch.GraphicsDevice.ScissorRectangle = area;
        }

        /// <summary>
        /// Draw order = -1 means use default 
        /// </summary>
        /// <param name="hPos">Horizontal position relative to parent</param>
        /// <param name="vPos">Vertical position relative to parent</param>
        public void AddComponent(PositionedGUIComponent component, HorizontalPosition hPos, VerticalPosition vPos, 
            int drawOrder = -1)
        {
            AddComponent(component, drawOrder);
            component.AlignToParent(vPos, hPos);
        }

        /// <summary>
        /// Actual drawOrder might change if containers SeparateBatch property is true
        /// And set SeparateBatch before adding component
        /// </summary>
        public virtual void AddComponent(GUIComponent component, int drawOrder)
        {
            if (SetDrawOrder == null)
                throw new MGUIException("GUIContainer must be added to MGUI instance before adding components to it " +
                    "and MGUI instance has to be initialized");
            //FIXME: If this component was added after all components was added to it then 
            //child component draw orders need to be updated
            if (drawOrder == -1)
            {
                // We usually want containers to be above normal components
                drawOrder = DrawOrder + ((component is GUIContainer) ? 2 : 1);
            }
            Debug.Assert(drawOrder > DrawOrder, "Draw order of container children must be bigger than containers");
            SetDrawOrder(component, drawOrder);
            AddComponentPrivate(component);
        }

        public virtual void RemoveComponent(GUIComponent component)
        {
            var container = component as GUIContainer;
            if (container != null)
                container.Remove();
            Components.Remove(component);
            component.Parent = null;
        }

        private void AddComponentPrivate(GUIComponent component)
        {
            GUIContainer cont = component as GUIContainer;
            if (cont != null)
            {
                cont.SetDrawOrder = SetDrawOrder;
                cont.ComponentAdded += OnComponentAdded;
                // Make sure ender is called before other containers begin
                //TODO: Should react to child draw order changes in this case
                if (cont.SeparateBatch)
                    cont.DrawOrder += 100;
            }
            VisibleChanged += component.OnParentVisibilityChanged;
            Removed += component.OnParentRemoved;
            EnabledChanged += component.OnParentEnabledChanged;
            PositionedGUIComponent posComp = component as PositionedGUIComponent;
            if (posComp != null)
            {
                Moved += posComp.OnParentMoved;
                Resized += posComp.OnParentResized;
                Scaled += posComp.OnParentScaled;
            }
            component.Visible = Visible;
            Components.Add(component);
            //this resets position so it becomes relative to this container
            // and scale
            component.Parent = this;

            GUIContainer c = component as GUIContainer;
            if (c != null && c.inheritParentSize)
            {
                c.area.Size = area.Size;
                c.localArea.Size = localArea.Size;
            }

            if (component.DrawOrder > lastDrawOrder && (cont == null || !cont.SeparateBatch))
            {
                lastDrawOrder = component.DrawOrder;
                SetEnderDrawOrder();
            }
            ComponentAdded?.Invoke(this, new ComponentAddedEventArgs(component));
        }

        /// <summary>
        /// Should be called when component is added to child containers
        /// </summary>
        private void OnComponentAdded(object sender, EventArgs args)
        {
            // Need to make sure our ender is after all the child containers
            GUIComponent comp = ((ComponentAddedEventArgs)args).component;
            if (!((GUIContainer)sender).SeparateBatch && comp.DrawOrder > lastDrawOrder)
            {
                GUIContainer cont = comp as GUIContainer;
                if (cont == null || !cont.SeparateBatch)
                {
                    lastDrawOrder = comp.DrawOrder;
                    SetEnderDrawOrder();
                    ComponentAdded?.Invoke(this, args);
                }
            }
        }

        public virtual void Remove()
        {
            SetDrawOrder = null;
            Components.Remove(this);
            if (Components.Contains(ender))
                Components.Remove(ender);
            Removed?.Invoke();
        }

        protected override void OnVisibleChanged(object sender, EventArgs args)
        {
            ender.Visible = ((DrawableGameComponent)sender).Visible;
        }

        internal override void OnParentRemoved()
        {
            Remove();
        }

        internal override void OnParentResized(object sender, EventArgs args)
        {
            base.OnParentResized(sender, args);
            if (inheritParentSize && Parent != null)
                SetSize(Parent.area.Size);
        }
        internal override void OnParentScaled(object sender, EventArgs args)
        {
            base.OnParentScaled(sender, args);
        }

        public int GetVerticalSide(VerticalPosition side)
        {
            int c = area.Location.Y;
            switch (side)
            {
                case VerticalPosition.TOP:
                    c = 0;
                    break;
                case VerticalPosition.BOTTOM:
                    c = area.Size.Y;
                    break;
                case VerticalPosition.CENTER:
                    c = area.Size.Y / 2;
                    break;
            }
            return c;
        }

        public int GetHorizontalSide(HorizontalPosition side)
        {
            int c = area.Location.X;
            switch (side)
            {
                case HorizontalPosition.LEFT:
                    c = 0;
                    break;
                case HorizontalPosition.RIGHT:
                    c = area.Size.X;
                    break;
                case HorizontalPosition.CENTER:
                    c = area.Size.X / 2;
                    break;
            }
            return c;
        }

        public void SetSize(Point size)
        {
            SetSize(size, true);
        }

        private void SetSize(Point size, bool fireEvent)
        {
            if (area.Size != size)
            {
                if (MakeFit && Parent != null)
                {
                    int limitX = Parent.area.Location.X + Parent.area.Size.X;
                    if (area.Location.X + size.X > limitX)
                        size.X = limitX - area.Location.X;
                    int limitY = Parent.area.Location.Y + Parent.area.Size.Y;
                    if (area.Location.X + size.Y > limitY)
                        size.Y = limitY - area.Location.Y;
                }
                area.Size = size;
                if (fireEvent)
                    Resized?.Invoke(this, EventArgs.Empty);
            }
        }
        
        private void SetEnderDrawOrder()
        {
            ender.DrawOrder = lastDrawOrder + 5;
        }
        #endregion
    }
}
