﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.Diagnostics;
using TMono;
using TMono.TControls;

namespace TMono.MGUI
{
    public class CheckBox : Button
    {
        private TextureComponent uncheckedText;
        private TextureComponent checkedText;
        private LabelComponent uncheckedLabel;
        private LabelComponent checkedLabel;
        private bool check = false;

        public bool Checked
        {
            get { return check; }
            set
            {
                if (value != check)
                {
                    check = value;
                    if (check)
                    {
                        Texture = checkedText;
                        Label = checkedLabel;
                    }
                    else
                    {
                        Texture = uncheckedText;
                        Label = uncheckedLabel;
                    }
                    CheckedChanged?.Invoke(this, EventArgs.Empty);
                }
            }
        }


        public event EventHandler CheckedChanged;

        /// <summary>
        /// By default label is placed to the right of checkbox texture
        /// You can set labelChecked and unchecked to the same value
        /// </summary>
        public CheckBox(Controls controls, TextureComponent textureUnchecked,
            TextureComponent textureChecked, LabelComponent labelUnchecked, LabelComponent labelChecked,
            Vector2 position, GUIContainer parent = null)
            : base(controls, textureUnchecked, labelUnchecked, position, parent)
        {
            LabelPlacement = new Position(HorizontalPosition.RIGHT, VerticalPosition.CENTER);
            uncheckedText = textureUnchecked;
            checkedText = textureChecked;
            uncheckedLabel = labelUnchecked;
            checkedLabel = labelChecked;
            Clicked += OnClicked;
            // If not set to false, when on pressed one texture gets scaled down, on release,
            // different scaled back 
            AnimationEnabled = false; 
        }
        public CheckBox(Controls controls, TextureComponent textureUnchecked,
            TextureComponent textureChecked, LabelComponent label)
            : this(controls, textureUnchecked, textureChecked, label, label,
                  new Vector2(0, 0), null)
        {
        }
        public CheckBox(Controls controls, TextureComponent textureUnchecked,
            TextureComponent textureChecked, LabelComponent labelUnchecked, LabelComponent labelChecked)
            : this(controls, textureUnchecked, textureChecked, labelUnchecked, labelChecked,
                  new Vector2(0, 0), null)
        {
        }

        private void OnClicked(object sender, EventArgs e)
        {
            Checked = !Checked;
        }

        

    }
}
