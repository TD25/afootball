﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Jitter;
using Jitter.Collision;
using Jitter.Dynamics;
using Jitter.LinearMath;

namespace TMono
{
    // TODO: Make 2D scene class. This class doesn't really work because it has
    // methods like AddObject which use collision system
    /// <summary>
    /// 3D scene
    /// </summary>
    public abstract class Scene : DrawableGameComponent, ILoadable, IEntity
    {
        public delegate void SetActiveDel(bool isActive);

        private Scene parent;
        protected SpriteBatch batch;
        private bool active = true;
        //private Camera camara;

        public Node Node
        {
            set; get;
        }
        /// <summary>
        /// This system should be used for only one scene
        /// </summary>
        public CollisionSystem CollSystem
        {
            set; get;
        }
        public Scene Parent
        {
            set { parent = value; }
            get { return parent; }
        }

        public Scene(CollisionSystem collSystem_, TGame game, SpriteBatch batch)
            : base(game)
        {
            CollSystem = collSystem_;
            this.batch = batch;
            Node = new Node("scene", null, Matrix.Identity);
        }
        public Scene(Scene parent_, TGame game, SpriteBatch batch)
            : base(game)
        {
            Node = new TMono.Node("scene", null, Matrix.Identity);
            if (parent != null)
            {
                parent = parent_;
                CollSystem = parent_.CollSystem;
            }
            this.batch = batch;
        }
        public Scene(TGame game, Node node, CollisionSystem colisionSystem)
            : base(game)
        {
            this.Node = node;
            CollSystem = colisionSystem;
        }
        public override void Update(GameTime time)
        {
            CollSystem.Detect(true);
        }
        /// <summary>
        /// This method should create objects in the scene
        /// </summary>
        public override void Initialize()
        {
        }
        new public virtual void LoadContent()
        {
        }
        public override void Draw(GameTime time)
        {
        }
        
        //TODO: Child game objects should inherit parents transform
        /// <summary>
        /// Adds GameObject to the scene, initializes and adds it to collision system
        /// If you want to add scene you only need to set this scene to parent other scene
        /// </summary>
        public void AddObject(GameObject entity)
        {
            entity.Initialize();
            Node.AddChild(entity.Node);
            EnabledChanged += entity.ParenEnabledChanged;
            CollSystem.AddEntity(entity.Physics.Body);
        }

        /// <summary>
        /// Completely removes entity
        /// Call this if you don't plan on using it anymore, else call DisableEntity
        /// </summary>
		public void RemoveObject(GameObject entity)
		{
            entity.RemoveComponents();
            RemoveObjectPr(entity);
		}

        /// <summary>
        /// Disables object, so that its update/draw methods don't get called
        /// </summary>
        public void DisableObject(GameObject entity)
        {
            entity.Free();
            RemoveObjectPr(entity); 
        }

        private void RemoveObjectPr(GameObject entity)
        {
            Node.RemoveChild(entity.Node);
            EnabledChanged -= entity.ParenEnabledChanged;
            CollSystem.RemoveEntity(entity.Physics.Body);
        }
    }
}
