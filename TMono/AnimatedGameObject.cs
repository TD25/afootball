using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AnimationPipeline;   //FIXME: make more specific
using Microsoft.Xna.Framework;
using System.Diagnostics;
using TMono.Physics;

namespace TMono
{
    public class AnimatedGameObject : GameObject
    {
        private AnimationPlayer player;

        /// <summary>
        /// Default animation update order
        /// </summary>
        static public int AnimationUpdateOrder { set; get; } = 300;
        public override bool Enabled
        {
            set
            {
                if (player != null)
                    player.Enabled = value;
                base.Enabled = value;
            }
        }
        protected AnimationPlayer AnimationPlayer
        {
            get { return player; }
            set
            {
                if (player != null)
                    components.Remove(player);
                player = value;
                player.UpdateOrder = AnimationUpdateOrder;
                components.Add(player);
            }
        }

        public AnimatedGameObject(PhysicsComponent phys_, GraphicsComponentSkinned graphics_, 
            ControllerComponent contrl, Node node, string type) :
             base(phys_, graphics_, contrl, node, type)
        {
            InitAnimationPlayer();
        }

        /// <summary>
        /// Creates player and sets it to physics and graphics components
        /// Call this if you change these components
        /// </summary>
        public void InitAnimationPlayer()
        {
            GraphicsComponentSkinned skinnedGr = Graphics as GraphicsComponentSkinned;
            if (skinnedGr != null)
            {
                // Each object should their own separate set of bones
                List<Bone> bones = skinnedGr.Model.ObtainBones();

                // Set node to be a parent of root bone
                // This is needed so we can attach objects to parts of body easily
                Bone root = bones[0];
                Debug.Assert(root.Parent == null && bones[1].Parent != null && bones[2] != null,
                    "AnimatedGameObject expects first bone of a model to be a root bone" +
                    " (a bone without parent) and all the following bones to be children of root");
                node.AddChild(root);

                AnimationPlayer = new AnimationPlayer(skinnedGr.Model, bones);
                skinnedGr.Bones = bones;
            }

            CharacterPhysics chPhys = Physics as CharacterPhysics;
            if (chPhys != null && player != null && skinnedGr != null)
            {
                chPhys.AnimPlayer = player;
            }
        }

        /// <summary>
        /// Call this after all components are set and content is loaded
        /// </summary>
        public override void Initialize()
        {
            player.Initialize();
            base.Initialize();
        }
    }
}
