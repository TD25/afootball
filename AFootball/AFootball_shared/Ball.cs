using System;
using System.Collections.Generic;
using System.Text;
using TMono;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Jitter.Collision.Shapes;
using Jitter.Dynamics;
using Jitter.LinearMath;
using TMono.Physics;
using System.Diagnostics;

namespace AFootball
{
    public class Ball : GameObject
    {
        private static Model model;
        // Expecting it to be a scene
        private Node parent;
        private bool isAttached = false;

        public bool IsAttached
        {
            get { return isAttached; }
        }

        public Ball(Node node, Camera camera) : base(node, "ball")
        {
            Graphics = new GraphicsComponent3D(node, camera, model);
            Controller = null;
            parent = node.Parent;
        }
        public Ball(Vector3 pos, Camera camera, Node parent) : 
            this(new Node("ball", parent, Matrix.CreateTranslation(pos)), camera)
        {
            this.parent = parent;
        }

        static public void LoadContent()
        {
            model = GraphicsComponent.Content.Load<Model>("Ball/ball");
        }
        public override void Initialize()
        {
            ((GraphicsComponent3D)Graphics).Model = model;
            float radius = model.Meshes[0].BoundingSphere.Radius;
            SphereShape shape = new SphereShape(radius);
            RigidBody body = new RigidBody(shape);
            body.Mass = 1f;
            body.IsStatic = false;
            DynamicPhysics phys = new DynamicPhysics(body, node, new JVector(0, 0, -11f));
            phys.Friction = 1f;
            phys.Bounciness = 0.45f;
            Physics = phys;
            base.Initialize();
        }
        public void Throw(JVector target, float angle)
        {
            Debug.Assert(!isAttached, "Can't throw ball which is attached");
            //http://gamedev.stackexchange.com/questions/17467/calculating-velocity-needed-to-hit-target-in-parabolic-arc

            JVector distance = target - Physics.Body.Position;
            float zDist = distance.Z;
            distance.Z = 0;
            float distanceLen = distance.Length();
            float g = ((DynamicPhysics)Physics).Gravity.Length();
            //FIXME: Not accurate when zDist absolute value is bigger or maybe it just very sensitive to changes
            float speed = (float)(Math.Sqrt(g) * Math.Sqrt(distanceLen) *
                Math.Sqrt(Math.Tan(angle) * Math.Tan(angle) + 1)) /
                (float)Math.Sqrt(2 * Math.Tan(angle) - (2 * g * zDist) / distanceLen);

            distance.Normalize();

            Matrix rotation = Matrix.CreateFromAxisAngle(new Vector3(1, 0, 0), -1 * angle);
            Matrix translation = Matrix.CreateTranslation(Converter.ToXNAVector(distance));
            Vector3 direction = (translation * rotation).Translation;

            Physics.Body.LinearVelocity = Converter.ToJVector(direction) * speed;
        }

        /// <summary>
        /// Attaches ball to hand with proper local transform and disables physics of ball
        /// </summary>
        /// <param name="owner"> owner of hand bone</param>
        public void AttachToHand(AnimatedGameObject footballer, Bone hand, GameObject owner)
        {
            Debug.Assert(!isAttached, "Ball is already attached to something");
            Physics.Enabled = false;
            parent.RemoveChild(Node);
            hand.AddChild(Node);
            Node.LocalTransform = Matrix.CreateFromAxisAngle(Vector3.UnitY, (float)Math.PI / 2);

            isAttached = true;
        }
        public void Dettach()
        {
            Debug.Assert(isAttached, "Ball is not attached");
            Physics.Enabled = true;
            Node.Parent.RemoveChild(Node);
            parent.AddChild(Node);
            isAttached = false;
        }
        public override void ParenEnabledChanged(object sender, EventArgs args)
        {
            // Only enable physics if not attached
            if (!isAttached)
                base.ParenEnabledChanged(sender, args);
        }

    }
}
