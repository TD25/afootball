using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TMono;
using Microsoft.Xna.Framework;
using Jitter.Dynamics;
using Jitter.Collision.Shapes;
using Jitter.LinearMath;
using TMono.Physics;

namespace AFootball
{
    class CameraController : CharacterPhysics
    {
        PhysicsComponent obj; //followed object
        //float slowDownFactor; 

        //public Vector3 Offset
        //{
        //    set { offset = value; }
        //    get { return offset; }
        //}
        public Vector2 PositiveBoundaries
        { set; get; }

        public Vector2 NegativeBoundaries
        { set; get; }

        public float LimitsX
        { set; get; }


        public CameraController(Node node, PhysicsComponent obj, 
            float limitsX) : base(node, Vector3.Zero, false)
        {
            this.obj = obj;
            Body = new RigidBody(new SphereShape(1));
            LimitsX = limitsX;
        }

        public override void Update(GameTime time)
        {
            Vector2 cPos = new Vector2(Body.Position.X, Body.Position.Y);
            Vector2 oPos = new Vector2(obj.Body.Position.X, obj.Body.Position.Y);
            Vector2 distance = oPos - cPos; //also distance from center of the screen to the object
            JVector oVel = obj.Body.LinearVelocity;
            JVector newVel = Body.LinearVelocity;

            // Deal with X axis
            // If on boundary and moving in the same direction as the boundary is from the origin
            // then keep same velocity as the object, else make it 0
            if ((distance.X >= PositiveBoundaries.X || distance.X <= NegativeBoundaries.X) && 
                 Math.Sign(distance.X) == Math.Sign(oVel.X) && (Math.Abs(cPos.X) < LimitsX || Math.Sign(cPos.X) != Math.Sign(oVel.X)))
                newVel.X = oVel.X;
            else
                newVel.X = 0;


            //Y axis
            // Have at least object velocity and if not on boundary or not on the right boundary, 
            // then add to it to bocome on it.
            // Which boundary (up or down) depends on sign of velocity
            if ((distance.Y < PositiveBoundaries.Y && distance.Y > NegativeBoundaries.Y) || 
               Math.Sign(distance.Y - PositiveBoundaries.Y) == Math.Sign(oVel.Y) || 
               Math.Sign(distance.Y - NegativeBoundaries.Y) == Math.Sign(oVel.Y))
            {
                if (Math.Sign(newVel.Y) != Math.Sign(oVel.Y))
                    newVel.Y = oVel.Y;
                newVel.Y += Math.Sign(oVel.Y) * 25 * (float)time.ElapsedGameTime.TotalSeconds;
            }
            else
                newVel.Y = oVel.Y;

            Body.LinearVelocity = newVel; //Z stays the same

            base.Update(time);
        }

        /// <summary>
        /// Makes sure camera position is not outside limitsX
        /// </summary>
        public void FixPosition()
        {
            JVector pos = body.Position;
            if (Math.Abs(pos.X) > LimitsX)
                pos.X = Math.Sign(pos.X) * LimitsX;
            JVector dist = obj.Body.Position - pos;
            if (dist.X > PositiveBoundaries.X)
                pos.X += dist.X - PositiveBoundaries.X;
            else if (dist.X < NegativeBoundaries.X)
                pos.X += dist.X - NegativeBoundaries.X;
            body.Position = pos;
        }
        
        public void Reset(JVector position)
        {
            Body.Position = position;
            Acceleration = Vector3.Zero;
            Body.LinearVelocity = JVector.Zero;
        }
    }
}
