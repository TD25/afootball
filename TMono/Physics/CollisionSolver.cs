using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jitter.Collision;
using Jitter.Collision.Shapes;
using Jitter.Dynamics;
using Jitter.LinearMath;
using System.Diagnostics;

namespace TMono.Physics
{
    public class CollisionSolver
    {
        public CollisionSolver(CollisionSystem colSystem)
        {
            colSystem.CollisionDetected += CollisionDetected;
        }

        // Same pairs does not get called more than once!
        // Make sure it stays this way
        private void CollisionDetected(RigidBody body1, RigidBody body2, 
            JVector point1, JVector point2, JVector normal, float penetration)
        {
            //all bodies should have GameObjects
            GameObject ob1 = (GameObject)body1.Tag;
            GameObject ob2 = (GameObject)body2.Tag;

            Debug.Assert(ob1.Physics != null && ob2.Physics != null);

            if (!ob1.Physics.Enabled || !ob2.Physics.Enabled)
                return; 

            SolveCollision(ob1, ob2, point1, point2, normal, penetration);
        }

        protected virtual void SolveCollision(GameObject body1, GameObject body2,
            JVector point1, JVector point2, JVector normal, float penetration)
        {
            JVector vel1 = body1.Physics.Body.LinearVelocity;
            JVector vel2 = body2.Physics.Body.LinearVelocity;
            body1.Physics.Collide(body2.Physics, point1, vel2, normal, penetration);
            body2.Physics.Collide(body1.Physics, point2, vel1, -1 * normal, penetration);
        }
    }
}
