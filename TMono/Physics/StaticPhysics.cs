using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jitter;
using Jitter.Dynamics;
using Jitter.LinearMath;
using Jitter.Collision;
using Microsoft.Xna.Framework;

namespace TMono.Physics
{
    public class StaticPhysics : PhysicsComponent
    {
        public override RigidBody Body
        {
            get { return base.Body; }
            set
            {
                base.Body = value;
                body.LinearVelocity = JVector.Zero;
            }
        }

        public StaticPhysics(RigidBody body, Node node)
            : base(body, node)
        {
            // Cannot protect against changing this property of body from outside
            body.IsStatic = true;
        }
        public StaticPhysics(Node node)
            : base(node)
        { }

        public override void Collide(PhysicsComponent obj, JVector point, JVector velBeforeCollision, 
            JVector normal, float penetration)
        {
        }
    }
}
