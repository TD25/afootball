using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.ViewManagement;
using System.Diagnostics;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace AFootball
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class GamePage : Page
    {
		internal static AFootball game;

		public GamePage()
        {
            this.InitializeComponent();

            ApplicationView.PreferredLaunchViewSize = new Size(900, 800);
            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.PreferredLaunchViewSize;

            game = LaunchGame();

            Window.Current.VisibilityChanged += OnWindowVisibilityChanged;
        }

        private AFootball LaunchGame()
        {
            var launchArguments = string.Empty;
            var game = MonoGame.Framework.XamlGame<AFootball>.Create(launchArguments, Window.Current.CoreWindow, swapChainPanel);
            return game;
        }

        void OnWindowVisibilityChanged(object sender, Windows.UI.Core.VisibilityChangedEventArgs e)
        {
            if (e.Visible && game.CurrentState == AFootball.State.NotInitialized)
                game.InitState();
            else if (!e.Visible && game.CurrentState == AFootball.State.InGame)
                game.Pause();
        }
    }
}
