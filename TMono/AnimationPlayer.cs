using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AnimationPipeline;
using System.ComponentModel;
using Microsoft.Xna.Framework;

namespace TMono
{
    /// <summary>
    /// Animation clip player. It maps an animation clip onto a model
    /// </summary>
    public class AnimationPlayer : GameComponent
    {
        #region Nested
        private class Animation
        {
            public AnimationClip clip;
            public int boneCnt = 0;
            public BoneInfo[] boneInfos;
            private double position;

            public double Position
            {
                get { return position; }
            }
            public bool Backwards
            { set; get; }
            public bool Finished
            {
                get
                {
                    return (!Backwards && position == clip.Duration) || (Backwards && position == 0);
                }
            }

            public Animation(AnimationClip clip, BoneInfo[] boneInfos, int boneCnt)
            {
                this.boneInfos = boneInfos;
                this.clip = clip;
                this.boneCnt = boneCnt;
            }

            /// <summary>
            /// Call this before starting to play clip from start
            /// To set right initial position value
            /// Only sets Position variable, use SetPosition to set bones
            /// </summary>
            public void Reset()
            {
                position = (Backwards) ? clip.Duration : 0;
            }

            public void ResetToBindPose()
            {
                foreach (var boneInfo in boneInfos)
                    boneInfo.ModelBone.LocalTransform = boneInfo.ModelBone.BindTransform;
            }

            /// <summary>
            /// Sets current bone positions in this animation to an actual model bones
            /// </summary>
            public void SetPositionToBone()
            {
                foreach (BoneInfo bone in boneInfos)
                    bone.ModelBone.LocalTransform = bone.Transform;
            }

            public void AddToPosition(double delta, bool setToModelBone)
            {
                delta = (Backwards) ? -delta : delta;
                SetPosition(Position + delta, setToModelBone);
            }

            /// <summary>
            /// Sets bones to specific time value in animation
            /// You can override Backwards option this way so be careful
            /// </summary>
            public void SetPosition(double value, bool setToModelBone)
            {
                if (value > clip.Duration)
                    value = clip.Duration;
                else if (value < 0)
                    value = 0;

                position = value;

                foreach (BoneInfo bone in boneInfos)
                {
                    bone.SetPosition(position);

                    if (setToModelBone)
                        bone.ModelBone.LocalTransform = bone.Transform;
                }
            }

            /// <summary>
            /// Properly increments position value without setting any bones
            /// </summary>
            public void IncrementPosition(double time)
            {
                double delta = (Backwards) ? -time : time;
                position += delta;
                if (position > clip.Duration)
                    position = clip.Duration;
                else if (position < 0)
                    position = 0;
            }

            // If you want to blend with bind transform create Animation with one keyframe: bind transform
            /// <summary>
            /// We use this method to blend animations to avoid looping through all the bones more than 1 time
            /// When currentClip is null it blends nextClip with current bone transform
            /// </summary>
            static public void BlendAnimations(Animation currentClip, Animation nextClip,
                double time, float blendFactor)
            {
                nextClip.IncrementPosition(time);

                // Blend with  curent bone transforms
                if (currentClip == null)
                {
                    for (int i = 0; i < nextClip.boneCnt; i++)
                    {
                        BoneInfo bone = nextClip.boneInfos[i];
                        bone.SetPosition(nextClip.position);

                        //NOTE: Because we are using current bone transform, we start blending with what we have set
                        // previously. Because of this transition appears faster.
                        Matrix bindTr = bone.ModelBone.LocalTransform;
                        Quaternion rot = Quaternion.Slerp(bindTr.Rotation, bone.Rotation, blendFactor);
                        Vector3 translation = Vector3.Lerp(bindTr.Translation, bone.Translation, blendFactor);
                        Vector3 scale = Vector3.Lerp(bindTr.Scale, bone.Scale, blendFactor);
                        Matrix transform = TMath.CreateTransform(rot, scale, translation);
                        bone.ModelBone.LocalTransform = transform;
                    }
                }
                else
                {
                    currentClip.IncrementPosition(time);
                    if (currentClip.boneCnt != nextClip.boneCnt)
                        throw new NotSupportedException("Trying to blend animations with different sets of bones");
                    for (int i = 0; i < currentClip.boneCnt; i++)
                    {
                        BoneInfo bone1 = currentClip.boneInfos[i];
                        BoneInfo bone2 = nextClip.boneInfos[i];
                        if (bone1.ModelBone != bone2.ModelBone)
                            throw new NotSupportedException("Trying to blend animations with different sets of bones");

                        bone1.SetPosition(currentClip.position);
                        bone2.SetPosition(nextClip.position);

                        Quaternion rot = Quaternion.Slerp(bone1.Rotation, bone2.Rotation, blendFactor);
                        Vector3 translation = Vector3.Lerp(bone1.Translation, bone2.Translation, blendFactor);
                        Vector3 scale = Vector3.Lerp(bone1.Scale, bone2.Scale, blendFactor);
                        Matrix transform = TMath.CreateTransform(rot, scale, translation);
                        bone1.ModelBone.LocalTransform = transform;
                    }
                }
            }
        }
        #endregion
        
        #region Fields
        /// <summary>
        /// The clip we are playing now
        /// </summary>
        private Animation currentAnim = null;

        private double blendTime = 0.4f;
        private double currentBlendTime = 0;
        private double speedFactor = 1.0;

        /// <summary>
        /// Animation to be played and blended with currentAnim
        /// </summary>
        private Animation nextAnim = null;

        private Dictionary<string, Animation> animations;

        /// <summary>
        /// The looping option
        /// </summary>
        private bool looping = false;
        private bool isRunning = false;

        #endregion

        #region Properties
        /// <summary>
        /// How long animations should be blended
        /// </summary>
        public double BlendTime
        {
            set { blendTime = value; }
            get { return blendTime; }
        }

        public double SpeedFactor
        {
            set { speedFactor = value; }
            get { return speedFactor; }
        }
        /// <summary>
        /// Currently running animation clip
        /// </summary>
        public AnimationClip CurrentClip { get { return currentAnim.clip; } }

        public string CurrentClipName
        {
            get
            {
                if (currentAnim != null)
                    return currentAnim.clip.Name;
                else if (nextAnim != null)
                    return nextAnim.clip.Name;
                else
                    return null;
            }
        }

        /// <summary>
        /// The current clip duration
        /// </summary>
        public double Duration { get { return currentAnim.clip.Duration; } }

        /// <summary>
        /// The looping option. Set to true if you want the animation to loop
        /// back at the end
        /// </summary>
        public bool Looping { get { return looping; } set { looping = value; } }

        /// <summary>
        /// Might be true but not running because Update method does not get called
        /// </summary>
        public bool IsRunning
        {
            get { return isRunning; }
        }

        //public BoneInfo[] BoneInfos { get { return currentAnim.boneInfos; } }

        #endregion

        #region Construction

        /// <summary>
        /// Constructor for the animation player. It makes the 
        /// association between a clip and a model and sets up for playing
        /// </summary>
        /// <param name="model"></param>
        /// <param name="bones"> Bones for specific object we are animating ></param>
        public AnimationPlayer(AnimatedModel model, List<Bone> bones) 
            : base(null)
        {
            List<AnimationClip> clips = model.Clips;
            animations = new Dictionary<string, Animation>();

            foreach (AnimationClip clip in clips)
            {
                // Create the bone information classes
                int boneCnt = clip.Bones.Count;
                BoneInfo[] boneInfos = new BoneInfo[boneCnt];

                for (int b=0;  b<boneInfos.Length;  b++)
                {
                    // Create it
                    boneInfos[b] = new BoneInfo(clip.Bones[b], bones);
                    // Assign it to a model bone
                    boneInfos[b].SetModel(bones);
                }

                animations.Add(clip.Name, new Animation(clip, boneInfos, boneCnt));
            }
        }

        #endregion

        #region Update and Transport Controls

        public override void Initialize()
        {
            //Rewind();
            //Stop();
        }
        /// <summary>
        /// Reset back to time zero.
        /// </summary>
        public void Rewind()
        {
            if (currentAnim != null)
                currentAnim.SetPosition(0, true);
        }

        public void Pause()
        {
            isRunning = false;
        }

        public void Stop(bool resetToBindPose = false)
        {
            if (currentAnim != null && resetToBindPose)
                currentAnim.ResetToBindPose();
            currentAnim = nextAnim = null;
            isRunning = false;
            currentBlendTime = 0;
        }

        /// <summary>
        /// Update the clip position
        /// </summary>
        /// <param name="delta"></param>
        public override void Update(GameTime gameTime)
        {
            if (!isRunning)
                return;
            double posDelta = gameTime.ElapsedGameTime.TotalSeconds * speedFactor;
            if (nextAnim == null) //if not blending
            {
                currentAnim.IncrementPosition(posDelta);
                if (currentAnim.Finished)
                {
                    if (!looping)
                    {
                        isRunning = false;
                        //currentAnim = null;
                    }
                    else
                        currentAnim.Reset();
                }
                currentAnim.SetPosition(currentAnim.Position, true);
            }
            else //blend currentAnim and nextAnim
            {
                currentBlendTime += posDelta;

                if (currentBlendTime >= blendTime)
                {
                    currentBlendTime = 0;
                    currentAnim = nextAnim;
                    nextAnim = null;
                    currentAnim.AddToPosition(posDelta, true);
                }
                else
                {
                    float blendFactor = (float)(currentBlendTime / blendTime);
                    // Blends animation and set it to actual bones.
                    // if currentAnim is null then blends with current bone transforms
                    Animation.BlendAnimations(currentAnim, nextAnim, posDelta, blendFactor);
                }
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="setToInitialPosition">Set to true if you want to set model to starting pose immediately.
        ///  This is usefull if animation isn't running but you want to see model in particular pose</param>
        public void PlayClip(string name, bool backwards = false, bool setToInitialPosition = false)
        {
            if ((currentAnim == null || name != currentAnim.clip.Name) 
                && (nextAnim == null || name != nextAnim.clip.Name))
            {
                nextAnim = animations[name]; //throws KeyNotFoundException
                nextAnim.Backwards = backwards;
                nextAnim.Reset();
                nextAnim.SetPosition(nextAnim.Position, setToInitialPosition);
            }
            isRunning = true;   // keep it here, cause currentAnim might be paused
        }

        #endregion

        #region BoneInfo class


        /// <summary>
        /// Information about a bone we are animating. This class connects a bone
        /// in the clip to a bone in the model.
        /// </summary>
        internal class BoneInfo
        {
            #region Fields

            /// <summary>
            /// The current keyframe. Our position is a time such that the 
            /// we are greater than or equal to this keyframe's time and less
            /// than the next keyframes time.
            /// </summary>
            private int currentKeyframe = 0;

            /// <summary>
            /// Bone in a model that this keyframe bone is assigned to
            /// </summary>
            private Bone assignedBone = null;

            /// <summary>
            /// We are not valid until the rotation and translation are set.
            /// If there are no keyframes, we will never be valid
            /// </summary>
            public bool valid = false;

            /// <summary>
            /// Current animation rotation
            /// </summary>
            private Quaternion rotation;

            /// <summary>
            /// Current animation translation
            /// </summary>
            private Vector3 translation;

            /// <summary>
            /// Current animation scale
            /// </summary>
            private Vector3 scale;

            /// <summary>
            /// We are at a location between Keyframe1 and Keyframe2 such 
            /// that Keyframe1's time is less than or equal to the current position
            /// </summary>
            public AnimationClip.Keyframe Keyframe1;

            /// <summary>
            /// Second keyframe value
            /// </summary>
            public AnimationClip.Keyframe Keyframe2;

            #endregion

            #region Properties

            public Vector3 Scale
            {
                get { return scale; }
            }

            public Vector3 Translation
            {
                get { return translation; }
            }

            public Quaternion Rotation
            {
                get { return rotation; }
            }

            public Matrix Transform
            {
                get { return TMath.CreateTransform(rotation, scale, translation); }
            }

            /// <summary>
            /// The bone in the actual animation clip
            /// </summary>
            public AnimationClip.Bone ClipBone { get; set; }

            /// <summary>
            /// The bone this animation bone is assigned to in the model
            /// </summary>
            public Bone ModelBone { get { return assignedBone; } }

            #endregion

            #region Constructor

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="bone"></param>
            /// <param name="boneList"> List of objects bones to find the one this info represents </param>
            public BoneInfo(AnimationClip.Bone bone, List<Bone> boneList)
            {
                this.ClipBone = bone;
                SetKeyframes();
                SetModel(boneList);
                //SetPosition(1);
            }

            #endregion

            #region Position and Keyframes

            /// <summary>
            /// Set the bone based on the supplied position value
            /// </summary>
            /// <param name="position"></param>
            public void SetPosition(double position)
            {
                List<AnimationClip.Keyframe> keyframes = ClipBone.Keyframes;
                if (keyframes.Count == 0)
                    return;

                // If our current position is less that the first keyframe
                // we move the currentKeyframe backward until we get to the right keyframe
                while (position < Keyframe1.Time && currentKeyframe > 0)
                {
                    // We need to move backwards in time
                    currentKeyframe--;
                    SetKeyframes();
                }

                // If our current position is greater than the second keyframe
                // we move the position forward until we get to the right keyframe
                while (position >= Keyframe2.Time && currentKeyframe < ClipBone.Keyframes.Count - 2)
                {
                    // We need to move forwards in time
                    currentKeyframe++;
                    SetKeyframes();
                }

                if (Keyframe1 == Keyframe2)
                {
                    // Keyframes are equal
                    rotation = Keyframe1.Rotation;
                    translation = Keyframe1.Translation;
                    scale = Keyframe1.Scale;
                }
                else
                {
                    // Interpolate between keyframes
                    float t = (float)((position - Keyframe1.Time) / (Keyframe2.Time - Keyframe1.Time));
                    rotation = Quaternion.Slerp(Keyframe1.Rotation, Keyframe2.Rotation, t);
                    translation = Vector3.Lerp(Keyframe1.Translation, Keyframe2.Translation, t);
                    scale = Vector3.Lerp(Keyframe1.Scale, Keyframe2.Scale, t);
                    rotation = Keyframe1.Rotation;
                    translation = Keyframe1.Translation;
                    scale = Keyframe1.Scale;
                }

                valid = true;

                if (assignedBone == null)
                    throw new NullReferenceException("assignedBone = null");

                // Send to the model
                // Make it a matrix first
                //Matrix m = Matrix.CreateFromQuaternion(rotation);
                //m.Scale = scale;
                //m.Translation = translation;
                //m.Scale = Vector3.One;
                //Matrix m = Matrix.CreateScale(scale) * Matrix.CreateFromQuaternion(rotation) *
                //    Matrix.CreateTranslation(translation);
                //assignedBone.SetCompleteTransform(m);
            }

            /// <summary>
            /// Set the keyframes to a valid value relative to 
            /// the current keyframe
            /// </summary>
            private void SetKeyframes()
            {
                if (ClipBone.Keyframes.Count > 0)
                {
                    Keyframe1 = ClipBone.Keyframes[currentKeyframe];
                    if (currentKeyframe == ClipBone.Keyframes.Count - 1)
                        Keyframe2 = Keyframe1;
                    else
                        Keyframe2 = ClipBone.Keyframes[currentKeyframe + 1];
                }
                else
                {
                    // If there are no keyframes, set both to null
                    Keyframe1 = null;
                    Keyframe2 = null;
                }
            }

            /// <summary>
            /// Assign this bone to the correct bone in the model
            /// </summary>
            /// <param name="model"></param>
            public void SetModel(List<Bone> modelBones)
            {
                assignedBone = AnimatedModel.FindBone(modelBones, ClipBone.Name);
            }

            #endregion
        }

        #endregion

    }
}
