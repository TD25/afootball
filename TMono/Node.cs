﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Jitter.LinearMath;
using Jitter.Dynamics;
using System.Diagnostics;

namespace TMono
{
    public class Node
    {
        #region Fields
        private Node parent = null;
        // Curently you should add children manualy, with Children property
        private LinkedList<Node> children = new LinkedList<Node>();
        private Matrix transform = Matrix.Identity;
        private Matrix worldTransform = Matrix.Identity;
        private bool dirty = true;
        private readonly object childrenLock = new object();
        #endregion 

        #region Properties
		public string Name = "";
        public Matrix LocalTransform
        {
            get { return transform; }
            set
            {
                transform = value;
                dirty = true;
            }
        }
		public Quaternion Rotation
        {
            get
            {
                return Quaternion.CreateFromRotationMatrix(transform);
            }
        }

        /// <summary>
        /// Any translations
        /// </summary>
		public Vector3 Translation
        {
            get { return transform.Translation; }
        }

        /// <summary>
        /// Any scaling
        /// </summary>
        public Vector3 Scale { get { return transform.Scale; }}

        /// <summary>
        /// The parent bone or null for the root bone
        /// </summary>
        public Node Parent
        { 
            get { return parent; }
            private set
            {
                parent = value;
                dirty = true;
            }
        }

        /// <summary>
        /// The children of this node
        /// Don't add or remove children directly but use AddChild, RemoveChild
        /// </summary>
        public LinkedList<Node> Children { get { return children; } }

        /// <summary>
        /// World transform might need to be updated if local transform
        /// or parent transform changed. Do ComputeWorldTransform to do that
        /// </summary>
        public Matrix WorldTransform
        {
            get { return worldTransform; }
            private set
            {
                if (worldTransform != value)
                    worldTransform = value;
            }
        }
        /// <summary>
        /// If true, then WorldTransform is out of sync with LocalTransform,
        /// which means new local transorm has been set, but world transform hasn't been
        /// recalculated. However this doesn't show if world transform needs to be recalculated,
        /// because parent transform changed
        /// </summary>
        public bool Dirty
        {
            get { return dirty; }
        }

        #endregion

        #region Constructors

        public Node(string name, Node parent, Matrix localTransform)
        {
            this.Name = name;
            Parent = parent;
            this.transform = localTransform;
        }
        public Node(Node parent)
           : this(string.Empty, parent, Matrix.Identity)
        { }
        public Node(Matrix localTransform)
            : this(string.Empty, null, localTransform)
        { }
        public Node(string name)
            : this(name, null, Matrix.Identity)
        { }
        #endregion

        #region Operations

        public void AddChild(Node child)
        {
            lock (childrenLock)
            {
                Debug.Assert(!Children.Contains(child), "This node already has this child: " + child);
                Children.AddLast(child);
            }
            child.Parent = this;
        }
        public void RemoveChild(Node child)
        {
            lock (childrenLock)
            {
                Debug.Assert(Children.Contains(child), "Can't remove a child which is not in children list");
                Children.Remove(child);
            }
            child.Parent = null;
        }
        /// <summary>
        /// Compute the absolute transformation for this bone.
        /// </summary>
        public void ComputeWorldTransform()
        {
            if (Parent != null)
                WorldTransform = transform * Parent.WorldTransform;
            else
                WorldTransform = transform;
            dirty = false;
        }

	    // more than 100 in a few seconds of in game
        //long skipCount = 0;
        /// <summary>
        /// Update world transform of this node and all child nodes
        /// </summary>
        /// <param name="parentDirty">Whether parent dirty flag is set (parent's world transform was changed).
        /// Should be set to false for root node</param>
        public void UpdateWorldTransforms(bool parentDirty)
        {
            // If either parent dirty flag was set or dirty flag of this node was set
            // then compute world transform
            parentDirty |= dirty;
            if (parentDirty)
                ComputeWorldTransform(); //resets dirty flag of this node
            //else
            //    skipCount++;

            lock (childrenLock)
            {
                foreach (Node child in Children)
                    child.UpdateWorldTransforms(parentDirty);
            }
        }

        /// <summary>
        /// Stores world transform from specified body into local transform of this node
        /// </summary>
        public void StoreBodyTransform(RigidBody body)
        {
            transform = Converter.ToXNAMatrix(body.Orientation);
            transform.Translation = Converter.ToXNAVector(body.Position);
            dirty = true;
        }

        /// <summary>
        /// Stores local transform of this node into specified body
        /// </summary>
        public void StoreInBody(RigidBody b)
        {
            b.Position = Converter.ToJVector(transform.Translation);
            b.Orientation = Converter.ToJMatrix(
                Matrix.CreateFromQuaternion(transform.Rotation));
        }
        #endregion
    }
}
