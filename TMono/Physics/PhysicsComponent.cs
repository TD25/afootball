using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jitter;
using Jitter.Dynamics;
using Jitter.LinearMath;
using Jitter.Collision;
using Jitter.DataStructures;
using Microsoft.Xna.Framework;

namespace TMono.Physics
{
    //one of the component of objects
    public abstract class PhysicsComponent : GameComponent
    {
        protected RigidBody body;
        protected Node node;   //this is shared between components of the object

        //FIXME: We should hide Body
        virtual public RigidBody Body
        {
            get { return body; }
            set
            {
                body = value;
                node.StoreInBody(body);
            }
        }
        public Node Node
        {
            get { return node; }
            set { node = value; }
        }

        public PhysicsComponent(RigidBody body_, Node node)
            : base(null)
        {
            this.node = node;
            Body = body_;
        }
       public PhysicsComponent(Node node)
            : base(null)
        {
            this.node = node;
        }
        public override void Update(GameTime time)
        {
            if (Enabled)
                node.StoreBodyTransform(body);
        }
        /// <summary>
        /// React to collision with other physics object
        /// </summary>
        /// <param name="obj">Physics component of object with which we collide</param>
        /// <param name="point">Collision point</param>
        /// <param name="velBeforeCollision">Velocity of obj before collision</param>
        public abstract void Collide(PhysicsComponent obj, JVector point, JVector velBeforeCollision,
            JVector normal, float penetration);

        protected override void OnEnabledChanged(object sender, EventArgs args)
        {
            base.OnEnabledChanged(sender, args);
        }
    }
}
