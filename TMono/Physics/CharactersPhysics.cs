using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jitter;
using Jitter.Dynamics;
using Jitter.LinearMath;
using Jitter.Collision;
using Jitter.DataStructures;
using Microsoft.Xna.Framework;
using System.Diagnostics;

namespace TMono.Physics
{
    /// <summary>
    ///class for character physics, when not using physics simulation (JitterWorld)
    /// </summary>
    public class CharacterPhysics : PhysicsComponent
    {
        private Vector3 directedAcc = Vector3.Zero;
        private Vector3 accel = Vector3.Zero;
        private Vector3 gravity;
        protected JVector staticColNormal = JVector.Zero;
        private int inactivityPeriod = 0;  //how long to be innactive after collision in mss
        private int inactiveElapsed = 0;
        private Vector3 afterCollAcc = Vector3.Zero;
        private AnimationPlayer animPlayer = null;

        /// <summary>
        /// Is static body needed for movement,
        ///  friction can be zero.
        /// </summary>
        public bool IsFrictionNeededForMovement
        {
            set; get;
        }
        public float Friction
        {
            set; get;
        }
        /// <summary>
        /// How fast does body stop after colliding
        /// In other words length of acceleration vector, in
        ///     opposite direction of collision
        /// </summary>
        //public float ResistanceAfterCollision
        //{
        //    set; get;
        //}
        public Vector3 Acceleration
        {
            get { return directedAcc; }
            set { directedAcc = value; }
        }
        public JVector StaticColNormal
        {
            get { return staticColNormal; }
        }

        /// <summary>
        /// Gets or sets the inactivity period.
        /// How long to be inactive after collision with active (not static) object
        /// Default - 0
        /// </summary>
        /// <value>The inactivity period.</value>
        protected int InactivityPeriod
        {
            set
            {
                inactivityPeriod = value;
                inactiveElapsed = value;
            }
            get
            {
                return inactivityPeriod;
            }

        }

        public bool IsInnactive
        {
            get
            {
                return (inactiveElapsed < inactivityPeriod);
            }
        }

        public AnimationPlayer AnimPlayer
        {
            set { animPlayer = value; } get { return animPlayer; }
        }

        public CharacterPhysics(RigidBody body_, Node node, Vector3 g,
            bool isFrictionNeededForMovement = false) : base(body_, node)
        {
            gravity = g;
            IsFrictionNeededForMovement = isFrictionNeededForMovement;
            Friction = 0;
        }
        public CharacterPhysics(Node node, Vector3 gravity_,
            bool isFrictionNeededForMovement = false) : base(node)
        {
            gravity = gravity_;
            IsFrictionNeededForMovement = isFrictionNeededForMovement;
            Friction = 0;
        }
        /// <summary>
        /// When innactive cannot accelerate
        /// </summary>
        /// <returns>The inactive.</returns>
        /// <param name="isInactive">Is inactive.</param>
        public void SetInactive(bool isInactive = true)
        {
            if (isInactive)
            {
                inactiveElapsed = 0;
                Acceleration = Vector3.Zero;
            }
            else
                inactiveElapsed = inactivityPeriod;
        }
        /// <summary>
        /// integrates acceleration and velocity
        /// </summary>
        /// <param name="time"></param>
        public override void Update(GameTime time)
        {
            if (inactiveElapsed < inactivityPeriod)
                inactiveElapsed += time.ElapsedGameTime.Milliseconds;
            float len = staticColNormal.Length();
            accel = gravity;
            if ((!IsFrictionNeededForMovement || len > float.Epsilon) && inactiveElapsed >= inactivityPeriod)
                accel += directedAcc; //allow changing acceleration only when on ground and not innactive
            else if (inactiveElapsed < inactivityPeriod)
                accel += afterCollAcc;
            float seconds = (float)time.ElapsedGameTime.TotalSeconds;
            body.LinearVelocity += Converter.ToJVector(accel) * seconds;
            if (len > float.Epsilon)
            {
                body.LinearVelocity = TMath.Slide(staticColNormal, body.LinearVelocity);
                if (Friction > 0 && body.LinearVelocity.Length() > float.Epsilon)
                    ApplyFriction(seconds);
            }
            body.Position += body.LinearVelocity * seconds;
            staticColNormal = JVector.Zero;
            base.Update(time);
        }

        /// <summary>
        ///one of objects must be CharacterPhysics 
        /// Order of PhysicsComponent parameters is important
        /// </summary>
        //public static void SolveCollision(PhysicsComponent obj1, PhysicsComponent obj2,
        //    JVector point1, JVector point2, JVector normal, float penetration)
        //{
        //    Debug.Assert(obj1 is CharacterPhysics || obj2 is CharacterPhysics);
        //    var body1 = obj1.Body;
        //    var body2 = obj2.Body;
        //    if (body1.IsStatic && !body2.IsStatic)
        //        ((CharacterPhysics)obj2).CollideWithStatic(-1*normal, penetration);
        //    else if (!body1.IsStatic && body2.IsStatic)
        //        ((CharacterPhysics)obj1).CollideWithStatic(normal, penetration);
        //    else if (!body1.IsStatic && !body2.IsStatic)
        //    {
        //        JVector v1 = body1.LinearVelocity;
        //        JVector v2 = body2.LinearVelocity;
        //        ActiveCollision((CharacterPhysics)obj1, (CharacterPhysics)obj2, normal);
        //    }
        //}

        public override void Collide(PhysicsComponent obj, JVector point1, JVector velBeforeCollision, 
            JVector normal, float penetration)
        {
            if (obj.Body.IsStatic)
                CollideWithStatic(normal, penetration);
            else
                CollideWithActive(obj.Body.Mass, velBeforeCollision, normal);
        }

        private void CollideWithStatic(JVector normal, float penetration)
        {
            body.Position += normal * penetration;
            staticColNormal += normal;
        }

        //TODO: prevent penetration. But then bounciness is not that good
        /// <summary>
        ///for collisions with another Character physics objects or maybe other types of not 
        ///  static objects
        /// </summary>
        /// <param name="mass">other objects mass</param>
        /// <param name="vel">other objects velocity</param>
        /// <param name="normal">collision normal</param>
        private void CollideWithActive(float mass, JVector vel, JVector normal)
        {
            //That small mass shouldn't affect the whole body
            if (mass <= Body.Mass / 10)
                return;

            JVector normalProj1 = (body.LinearVelocity.Length() > 0) ?
                TMath.Projection(normal, body.LinearVelocity) : JVector.Zero;
            JVector normalProj2 = (vel.Length() > 0) ?
                TMath.Projection(normal, vel) : JVector.Zero;
            float u2 = normalProj2.Length();
            float u1 = normalProj1.Length();

            float v1 = (u1 * (body.Mass - mass) + 2 * mass * u2) /
               (body.Mass + mass);
            JVector v = normal * v1;
            body.LinearVelocity = body.LinearVelocity - normalProj1 + v;
			SetInactive();
            // Slowing it down 
			if (inactivityPeriod > 0)
				afterCollAcc = Converter.ToXNAVector(-1 * v * (1000 / inactivityPeriod));
        }

        /// <summary>
        /// Solves collision between active objects
        /// Order of Character physics parameters is important, because of normal vector
        /// </summary>
   //     private static void ActiveCollision(CharacterPhysics obj1, CharacterPhysics obj2, JVector normal)
   //     {
   //         JVector normalProj1 = (obj1.body.LinearVelocity.Length() > 0) ?
   //             TMath.Projection(normal, obj1.body.LinearVelocity) : JVector.Zero;
   //         JVector normalProj2 = (obj2.body.LinearVelocity.Length() > 0) ?
   //             TMath.Projection(normal, obj2.body.LinearVelocity) : JVector.Zero;
   //         float u2 = normalProj2.Length();
   //         float u1 = normalProj1.Length();

   //         //obj1
   //         float v1 = (u1 * (obj1.body.Mass - obj2.Body.Mass) + 2 * obj2.Body.Mass * u2) /
   //            (obj1.body.Mass + obj2.Body.Mass);
   //         JVector v = normal * v1;
   //         obj1.body.LinearVelocity = obj1.body.LinearVelocity - normalProj1 + v;
			//obj1.SetInactive();
   //         // Slowing it down 
			//if (obj1.inactivityPeriod > 0)
			//	obj1.afterCollAcc = Converter.ToXNAVector(-1 * v * (1000 / obj1.inactivityPeriod));

   //         //obj2
   //         v1 = (u2 * (obj2.body.Mass - obj1.Body.Mass) + 2 * obj1.Body.Mass * u1) /
   //            (obj2.body.Mass + obj1.Body.Mass);
   //         v = -1 *normal * v1;
   //         obj2.body.LinearVelocity = obj2.body.LinearVelocity - normalProj2 + v;
			//obj2.SetInactive();
   //         // Slowing it down
			//if (obj2.inactivityPeriod > 0)
			//	obj2.afterCollAcc = Converter.ToXNAVector(-1 * v * (1000 / obj2.inactivityPeriod));
   //     }

        private void ApplyFriction(float seconds)
        {
            float frictionF = body.Mass*gravity.Length() * Friction;
            float a = frictionF / body.Mass;
            JVector dir = -1 * body.LinearVelocity;
            dir.Normalize();
            body.LinearVelocity += dir * a * seconds;
        }

        public virtual void Reset()
        {
            body.LinearVelocity = JVector.Zero;
            Acceleration = Vector3.Zero;
            afterCollAcc = Vector3.Zero;
            staticColNormal = JVector.Zero;
        }
        //TODO: make private
        public void PlayClip(string name)
        {
            animPlayer.PlayClip(name);                         
        }

    }
}
