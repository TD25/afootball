﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TMono.MGUI
{
    public class TextureComponent : PositionedGUIComponent
    {
        static internal SpriteBatch batch; //should be set by MGUI
        private Texture2D texture;
        public Color color;
        //by default we don't specify size when drawing
        private Rectangle rect = new Rectangle(Point.Zero, new Point(-1, -1)); //global
        private Rectangle localRect;    //local
        private Rectangle scaledRect = new Rectangle();
        private bool useDefaultSize = true;

        public float Rotation
        { set; get; } = 0.0f;
        public SpriteEffects Effects
        { set; get; } = SpriteEffects.None;
        /// <summary>
        /// Origin point around which to rotate
        /// </summary>
        public Vector2 Origin
        { set; get; } = new Vector2(0, 0);

        public bool UseDefaultSize
        {
            get { return useDefaultSize; }
            set
            {
                useDefaultSize = value;
                if (useDefaultSize)
                    rect.Size = texture.Bounds.Size;
            }
        }
        public override Vector2 Scale
        {
            get { return base.Scale; }
            set
            {
                base.Scale = value;
                CalcScaledRect();
                Position = Position; //Reposition with new size
            }
        }
        public override Vector2 Size
        {
            get { return new Vector2(scaledRect.Size.X, scaledRect.Size.Y); }
        }
        public Point TextureSize
        {
            set
            {
                useDefaultSize = false;
                rect.Size = value;
                CalcScaledRect();
            }
        }
        public override Vector2 Position
        {
            get
            {
                return base.Position;
            }

            set
            {
                base.Position = value;
                localRect.Location = new Point((int)value.X, (int)value.Y);
                rect.Location = new Point((int)RealPosition.X, (int)RealPosition.Y);
                scaledRect.Location = rect.Location;
            }
        }
        public override Vector2 RealScale
        {
            get { return base.RealScale; }
            protected set
            {
                scaledRect.Size = new Point((int)Math.Round(rect.Size.X * value.X), 
                  (int)Math.Round(rect.Size.Y * value.Y));
                base.RealScale = value;
            }
        }
        public Rectangle Rectangle
        {
            //setting this means you don't use default texture size
            set
            {
                localRect = value;
                rect.Size = localRect.Size;
                Position = new Vector2(localRect.Location.X, localRect.Location.Y);
                useDefaultSize = false;
                //location of scaledRect is set in Position
                scaledRect.Size = new Point((int)Math.Round(rect.Size.X * RealScale.X), 
                    (int)Math.Round(rect.Size.Y * RealScale.Y));
            }
            get { return rect; }
        }
        public Texture2D Texture
        {
            get { return texture; }
            set
            {
                texture = value;
                if (useDefaultSize)
                    rect.Size = texture.Bounds.Size;
            }
        }

        public TextureComponent(Texture2D tex, Color col, Vector2 position, Vector2 size)
             : base(null, position)
        {
            color = col;
            texture = tex;
            if (size.X != -1 && size.Y != -1)
            {
                rect.Size = new Point((int)size.X, (int)size.Y);
                useDefaultSize = false;
            }
            else
            {
                rect.Size = texture.Bounds.Size;
                useDefaultSize = true;
            }
            scaledRect = rect;
        }

        public TextureComponent(Texture2D tex, Color col, Rectangle rectangle)
            : base(null, new Vector2(rectangle.X, rectangle.Y))
        {
            Rectangle = rectangle;
            Texture = tex;
            color = col;
        }

        public TextureComponent(Texture2D tex, Color col, Vector2 position)
             : base(null, position)
        {
            color = col;
            useDefaultSize = true;
            Texture = tex;
            scaledRect = rect;
        }

        public TextureComponent(Texture2D tex, Color color, Point size)
            : base(null, new Vector2(0, 0))
        {
            Rectangle = new Rectangle(Point.Zero, size);
            texture = tex;
            this.color = color;
            scaledRect = rect;
        }

        public override void Draw(GameTime gameTime)
        {
            batch.Draw(texture, scaledRect, null, color, Rotation, Origin, Effects, 0.0f);
            base.Draw(gameTime);
        }

        //public void Scale(Vector2 scale)
        //{
        //    Point size = localRect.Size;
        //    size.X = (int)Math.Round((float)size.X * scale.X);
        //    size.Y = (int)Math.Round((float)size.Y * scale.Y);
        //    localRect.Size = size;
        //    Rectangle = localRect;
        //}

        //FIXME:
        public void SetSize(Point size)
        {
            localRect.Size = size;
            Rectangle = localRect;
        }
        private void CalcScaledRect()
        {
            scaledRect.Location = rect.Location;
            scaledRect.Size = new Point((int)Math.Round(rect.Size.X * RealScale.X), 
                (int)Math.Round(rect.Size.Y * RealScale.Y));
        }

        public static TextureComponent CreateTextureComponent(GraphicsDevice device, Point size, Color color)
        {
            return new TextureComponent(CreateTexture(device, color), color, size);
        }

        public static Texture2D CreateTexture(GraphicsDevice device, Color color)
        {
            Texture2D texture = new Texture2D(device, 1, 1);
            texture.SetData(new Color[] { color });
            return texture;
        }

        protected override void SetRealScale(Vector2 scale)
        {
            base.SetRealScale(scale);
            CalcScaledRect();
        }
    }
}
