﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using TMono;
using TMono.TControls;
using System.Diagnostics;

namespace TMono.MGUI
{
    /// <summary>
    /// Update order should be set manualy
    /// </summary>
    public class Button : GUIControl
    {
        private TextureComponent texture;
        private LabelComponent label;
        private bool sendPressedEvent = false;
        private bool sendClickedEvent = false;
        private const byte tappedAction = 0; //works for mouse and touch controls
        private Vector2 labelOriginalScale;
        private Vector2 originalScale;
        private bool isAnimatedPressed = false;

        /// <summary>
        /// Just scaled down when pressed
        /// </summary>
        public bool AnimationEnabled
        { set; get; } = true;
        protected TextureComponent Texture
        {
            get { return texture; }
            set
            {
                texture = value;
                if (texture != null)
                {
                    //Checking because a lot gets done by setting these properties
                    // repositioning and such
                    if (texture.Parent != Parent)
                        texture.Parent = Parent;
                    if (texture.MinScale != MinScale)
                        texture.MinScale = MinScale;
                    if (texture.MaxScale != MaxScale)
                        texture.MaxScale = MaxScale;
                    if (texture.Scale != Scale)
                        texture.Scale = Scale;
                }
                // Reposition because size may have changed
                Position = Position;
            }
        }
        /// <summary>
        /// Set it after assigning texture
        /// </summary>
        public LabelComponent Label
        {
            get { return label; }
            set
            {
                if (value == null)
                    return;
                label = value;
                label.Parent = Parent;
                label.HorizontalAlignment = HorizontalPosition.CENTER;
                label.VerticalAlignment = VerticalPosition.CENTER;
                FixLabelPos();
            }
        }
        public Vector2 ScaleWhenPressed
        { set; get; }
        public override Vector2 Size
        {
            get
            {
                if (texture == null)
                    return label.Size;
                else
                    return texture.Size;
            }
        }
        public override Vector2 Position
        {
            get
            {
                return base.Position;
            }

            set
            {
                base.Position = value;
                if (texture != null)
                {
                    texture.Position = value;
                    FixLabelPos();
                }
                else if (label != null)
                    label.Position = value;
            }
        }

        public override VerticalPosition VerticalAlignment
        {
            get { return base.VerticalAlignment; }
            set
            {
                base.VerticalAlignment = value;
                if (texture != null)
                    texture.VerticalAlignment = value;
                else
                    label.VerticalAlignment = value;
            }
        }

        public override HorizontalPosition HorizontalAlignment
        {
            get { return base.HorizontalAlignment; }
            set
            {
                base.HorizontalAlignment = value;
                if (texture != null)
                    texture.HorizontalAlignment = value;
                else
                    label.HorizontalAlignment = value;
            }
        }
        public bool IsPressed
        {
            private set; get;
        }
        internal override GUIContainer Parent
        {
            get { return base.Parent; }

            set
            {
                base.Parent = value;
                // We want texture to position itself relative to parent without our help
                // and scale relative to parent
                if (texture != null)
                    texture.Parent = Parent;
                if (label != null)
                    label.Parent = Parent;
            }
        }
        public override Vector2 Scale
        {
            get { return base.Scale; }
            set
            {
                base.Scale = value;
                if (texture != null)
                    texture.Scale = Scale;
                //if (label != null)
                //    label.Scale = value;
            }
        }
        //You might want to set min and max scale for different for labels
        // so it makes more sense to set them separately
        public override Vector2 MaxScale
        {
            get { return base.MaxScale; }
            set
            {
                base.MaxScale = value;
                if (texture != null)
                    texture.MaxScale = MaxScale;
                else if (label != null)
                    label.MaxScale = MaxScale;
            }
        }
        //You might want to set min and max scale for different for labels
        // so it makes more sense to set them separately
        public override Vector2 MinScale
        {
            get { return base.MinScale; }
            set
            {
                base.MinScale = value;
                if (texture != null)
                    texture.MinScale = MinScale;
                if (label != null)
                    label.MinScale = MinScale;
            }
        }
        /// <summary>
        /// Label placement relative to button texture.
        /// Not used if button does not have a texture.
        /// Center by default
        /// Assuming you want label to overlap only if in total center
        /// </summary>
        public Position LabelPlacement
        { set; get; } = new Position(HorizontalPosition.CENTER, VerticalPosition.CENTER);

        /// <summary>
        /// Using size of passed texture componenet to determine size of button
        /// </summary>
        public Button(Controls controls, TextureComponent texture, LabelComponent label,
            Vector2 position, GUIContainer parent = null)
            : base(controls, position, parent)
        {
            IsPressed = false;
            Texture = texture;
            
            Label = label;
            Debug.Assert(label != null || texture != null, "Button has to have label or texture set");

            if (texture != null)
                Scale = texture.Scale; //size and scale of texture and button has to be equal

            //DefaultAnimationEnabled = true;
            ScaleWhenPressed = new Vector2(0.7f, 0.7f);

            //listen for tapped events
            //leftMouseButton and touched and tappedAction have the same value
            controls.AddAction(tappedAction, (Keys)PCControls.leftMouseButton);
            controls.RegisterPressedHandler(tappedAction, OnMouseLButtonPressed);
            controls.RegisterReleasedHandler(tappedAction, OnMouseLButtonReleased);
        }

        /// <summary>
        /// Using size of passed texture componenet to determine size of button
        /// </summary>
        public Button(Controls controls, TextureComponent texture, Vector2 position, GUIContainer parent = null)
            : this(controls, texture, null, position, parent)
        { }

        /// <summary>
        /// Using size and position of texture parameter
        /// </summary>
        public Button(Controls controls, TextureComponent texture, GUIContainer parent = null, LabelComponent label = null)
            : this(controls, texture, label, texture.Position, parent)
        { }
        public Button(Controls controls, LabelComponent label)
            : this(controls, null, label, label.Position, null)
        { }

        public event EventHandler Pressed;
        public event EventHandler Clicked;

        private void FixLabelPos()
        {
            // We directly set labels position, not going through all the parents
            if (texture != null && label != null)
            {
                float x = 0, y = 0;
                Vector2 normalPos = texture.NormaPos;
                switch (LabelPlacement.hPos)
                {
                    case HorizontalPosition.CENTER:
                        x = normalPos.X + texture.Size.X / 2;
                        label.HorizontalAlignment = HorizontalPosition.CENTER;
                        break;
                    case HorizontalPosition.LEFT:
                        x = normalPos.X;
                        label.HorizontalAlignment = HorizontalPosition.RIGHT;
                        break;
                    case HorizontalPosition.RIGHT:
                        x = normalPos.X + texture.Size.X;
                        label.HorizontalAlignment = HorizontalPosition.LEFT;
                        break;
                }
                switch (LabelPlacement.vPos)
                {
                    case VerticalPosition.CENTER:
                        y = normalPos.Y + texture.Size.Y / 2;
                        label.VerticalAlignment = VerticalPosition.CENTER;
                        break;
                    case VerticalPosition.BOTTOM:
                        y = normalPos.Y + texture.Size.Y;
                        label.VerticalAlignment = VerticalPosition.TOP;
                        break;
                    case VerticalPosition.TOP:
                        y = normalPos.Y;
                        label.VerticalAlignment = VerticalPosition.BOTTOM;
                        break;
                }
                label.Position = new Vector2(x, y);
            }
        }
        public override void Draw(GameTime gameTime)
        {
            if (texture != null)
                texture.Draw(gameTime);
            if (label != null)
                label.Draw(gameTime);
        }
        /// <summary>
        /// Currently there is no way to remove hotkey once it is added
        /// But you can add more than one
        /// Use Pressed and clicked events works as key is pressed and released
        /// </summary>
        public void AddHotkey(Keys key)
        {
            byte hotKeyAction = Controls.AddActionDefault(key);
            Controls.RegisterReleasedHandler(hotKeyAction, OnReleased);
            Controls.RegisterPressedHandler(hotKeyAction, OnPressed);
        }
        private void OnMouseLButtonPressed(object sender, ControlsEventArgs args)
        {
            var arg = (PosControlsEventArgs)args;
            // If contained in button texture or label
            if (Enabled && (Contains(RealPosition, Size, arg.Position) ||
                (label != null && Contains(label.RealPosition, label.Size, arg.Position))))
            {
                OnPressed(sender, args);
            }
        }

        private void OnMouseLButtonReleased(object sender, ControlsEventArgs args)
        {
            var arg = (PosControlsEventArgs)args;
            if (IsPressed)
                OnReleased(sender, args);
        }

        private void OnPressed(object sender, ControlsEventArgs args)
        {
            if (Enabled)
            {
                IsPressed = true;
                sendPressedEvent = true;
                AnimatePressed();
            }
        }

        private void OnReleased(object sender, ControlsEventArgs args)
        {
            if (Enabled && IsPressed)
            {
                IsPressed = false;
                sendClickedEvent = true;
                AnimateReleased();
            }
        }

        protected bool Contains(Vector2 location, Vector2 size, Point point)
        {
            return (point.X >= location.X && point.X <= location.X + size.X &&
                point.Y >= location.Y && point.Y <= location.Y + size.Y);
        }

        /// <summary>
        /// Does not get called if Disabled, so events doesn't get called
        /// </summary>
        public override void Update(GameTime gameTime)
        {
            if (sendPressedEvent)
            {
                sendPressedEvent = false;
                Pressed?.Invoke(this, EventArgs.Empty);
            }
            if (sendClickedEvent)
            {
                sendClickedEvent = false;
                Clicked?.Invoke(this, EventArgs.Empty);
            }
        }
        protected virtual void AnimatePressed()
        {
            if (!AnimationEnabled)
                return;
            if (texture != null)
            {
                originalScale = texture.Scale;
                texture.Scale *= ScaleWhenPressed;
            }
            if (label != null)
            {
                labelOriginalScale = label.Scale;
                label.Scale *= ScaleWhenPressed;
            }
            isAnimatedPressed = true;
        }
        protected virtual void AnimateReleased()
        {
            if (!AnimationEnabled)
                return;

            if (texture != null)
                texture.Scale = originalScale;
            if (label != null)
                label.Scale = labelOriginalScale;
            originalScale = labelOriginalScale = Vector2.Zero;
            isAnimatedPressed = false;
        }
        internal override void OnParentScaled(object sender, EventArgs args)
        {
            if (texture != null)
                texture.OnParentScaled(sender, args);
            if (label != null)
                label.OnParentScaled(sender, args);
            base.OnParentScaled(sender, args);
        }

        protected override void OnEnabledChanged(object sender, EventArgs args)
        {
            base.OnEnabledChanged(sender, args);
            if (IsPressed)
                AnimateReleased();
            IsPressed = false;
            sendClickedEvent = sendPressedEvent = false;
        }
    }
}
