﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TMono.MGUI
{
    public class LabelComponent : PositionedGUIComponent
    {
        static internal SpriteBatch batch;
        private string str;
        private SpriteFont font;
        public Color color;

        public float Rotation
        { set; get; } = 0.0f;
        public SpriteEffects Effects
        { set; get; } = SpriteEffects.None;
        public override Vector2 Scale
        {
            get { return base.Scale; }
            set
            {
                base.Scale = value;
                //Position = Position; //reposition with new size
            }
        }
        public Vector2 Origin
        { set; get; } = new Vector2(0, 0);
        public override Vector2 Size
        {
            get
            {
                if (font == null || str == null)
                    throw new MGUIException("Trying to get size when font or string is null");
                return font.MeasureString(str) * RealScale;
            }
        }
        public SpriteFont Font
        {
            get { return font; }
            set
            {
                font = value;
                SetRealPos();
            }
        }
        public string Text
        {
            get { return str; }
            set
            {
                str = value;
                SetRealPos();
            }
        }

        public LabelComponent(SpriteFont font_, Color col, Vector2 pos_, string str_)
             : base(null, pos_)
        {
            str = str_;
            font = font_;
            color = col;
        }

        public LabelComponent(SpriteFont font, Color col, string str)
            : this(font, col, new Vector2(0, 0), str)
        { }

        public LabelComponent(SpriteFont font, Color color)
            : this(font, color, new Vector2(0, 0), string.Empty)
        { }

        public override void Draw(GameTime gameTime)
        {
            batch.DrawString(font, str, RealPosition, color, Rotation, Origin, RealScale, Effects, 0.0f);
            base.Draw(gameTime);
        }
    }
}
