﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMono.TControls;
using Microsoft.Xna.Framework;

namespace TMono.MGUI
{
    public class LinkComponent : Button
    {
        public delegate void LaunchUrlDel(string url);

        private string url;

        /// <summary>
        /// You have to set this before using this component
        /// </summary>
        public static LaunchUrlDel LaunchUrl
        { set; get; }

        public LinkComponent(Controls controls, TextureComponent texture, LabelComponent label,
            Vector2 position, string url, GUIContainer parent = null) 
            : base(controls, texture, label, position, parent)
        {
            this.url = url;
            Clicked += OnClicked;
        }

        public LinkComponent(Controls controls, LabelComponent label, string url)
            : this(controls, null, label, label.Position, url, null)
        { }
        public LinkComponent(Controls controls, TextureComponent texture, string url)
            : this(controls, texture, null, texture.Position, url, null)
        { }

        private void OnClicked(object sender, EventArgs e)
        {
            LaunchUrl(url);
        }
        
    }
}
