using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using AnimationPipeline;

namespace TMono
{
    /// <summary>
    /// An encloser for an XNA model that we will use that includes support for
    /// bones, animation, and some manipulations.
    /// </summary>
    public class AnimatedModel 
    {
        #region Fields

        /// <summary>
        /// The actual underlying XNA model
        /// </summary>
        private Model model = null;

        /// <summary>
        /// Extra data associated with the XNA model
        /// </summary>
        private ModelExtra modelExtra = null;

        /// <summary>
        /// The model bones
        /// </summary>
        private List<Bone> bones = new List<Bone>();

        /// <summary>
        /// The model asset name
        /// </summary>
        private string assetName = "";

        #endregion

        #region Properties

        /// <summary>
        /// The actual underlying XNA model
        /// </summary>
        public Model Model
        {
            get { return model; }
        }

        public ModelExtra ModelExtra
        {
            get { return modelExtra; }
        }

        /// <summary>
        /// The underlying bones for the model
        /// </summary>
        public List<Bone> Bones { get { return bones; } }

        /// <summary>
        /// The model animation clips
        /// </summary>
        public List<AnimationClip> Clips { get { return modelExtra.Clips; } }

        #endregion

        #region Construction and Loading

        /// <summary>
        /// Constructor. Creates the model from an XNA model
        /// </summary>
        /// <param name="assetName">The name of the asset for this model</param>
        public AnimatedModel(string assetName)
        {
            this.assetName = assetName;
        }

        /// <summary>
        /// Load the model asset from content
        /// </summary>
        /// <param name="content"></param>
        public void LoadContent(ContentManager content)
        {
            this.model = content.Load<Model>(assetName);
            modelExtra = model.Tag as ModelExtra;
            System.Diagnostics.Debug.Assert(modelExtra != null);

            bones = ObtainBones();
        }

        #endregion

        #region Bones Management

        /// <summary>
        /// Make a new copy of bones
        /// LoadContent must be called before
        /// </summary>
        //public List<Bone> CloneBones()
        //{
        //    List<Bone> newBones = new List<Bone>();
        //    int index = 0;
        //    foreach (Bone bone in bones)
        //    {
        //        // Create the bone object and add to the heirarchy
        //        Bone newBone = new Bone(bone.Name, bone.Transform, 
        //            bone.Parent != null ? newBones[bone.Parent.Index] : null, index);
        //        newBones.Add(newBone);
        //        index++;
        //    }
        //    return newBones;
        //}

        /// <summary>
        /// Get the bones from the model and create a bone class object for
        /// each bone. We use our bone class to do the real animated bone work.
        /// </summary>
        public List<Bone> ObtainBones()
        {
            List<Bone> bones = new List<Bone>();
            int index = 0;
            foreach (ModelBone bone in model.Bones)
            {
                // Create the bone object and add to the heirarchy
                Bone newBone = new Bone(bone.Name, bone.Transform, 
                    bone.Parent != null ? bones[bone.Parent.Index] : null, index);
                // Add to the bones for this model
                bones.Add(newBone);
                index++;
            }
            return bones;
        }

        /// <summary>
        /// Find a bone in this model by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Bone FindBone(string name)
        {
            foreach(Bone bone in Bones)
            {
                if (bone.Name == name)
                    return bone;
            }

            return null;
        }

        static public Bone FindBone(List<Bone> bones, string name)
        {
            foreach(Bone bone in bones)
            {
                if (bone.Name == name)
                    return bone;
            }
            return null;
        }

        //public List<Bone> CloneBones()
        //{
        //    List<Bone> newList = new List<Bone>();
        //    foreach (Bone bone in bones)
        //    {
        //        Bone parent = (bone.Parent != null) ? newList[bone.Parent.Index]
        //        newList.Add(new Bone(bone, parent));
        //    }
        //    return newList;
        //}

        #endregion
    }
}
