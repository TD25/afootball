﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace TMono
{
    // Should be in TMono library, but you can't put platform specific implementations
    // into portable library
    static class OS 
    {
 #if (ANDROID)
        public static Microsoft.Xna.Framework.AndroidGameActivity Activity
        { set; get; }
#endif

        public static void LaunchUrl(string url)
        {
#if (WINDOWS_DESKTOP || LINUX_DESKTOP)
            Process process = Process.Start(url);
#elif (ANDROID)
            var uri = Android.Net.Uri.Parse(url);
            var intent = new Android.Content.Intent(Android.Content.Intent.ActionView, uri);
            Activity.StartActivity(intent);
#elif (WINDOWS_UAP || WINDOWS_PHONE_APP || WINDOWS_APP)
            Uri uri = new Uri(url);
            Windows.System.Launcher.LaunchUriAsync(uri);
#else
            throw new NotImplementedException();
#endif
        }
    }
}
