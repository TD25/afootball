﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace TMono.TControls
{
    public class ControlsEventArgs : EventArgs
    {
        //TODO: Add state (released, pressed)?
        public byte ActionName { get; set; }
        public ControlsEventArgs(byte action)
        { ActionName = action; }
    }

    public class PosControlsEventArgs : ControlsEventArgs
    {
        public Point Position { set; get; }
        public PosControlsEventArgs(byte action, Point position) : base(action)
        {
            Position = position;
        }
    }

    public abstract class Controls : GameComponent
    {
        //using this then action is not specified in AddAction
        private const byte defaultAction = 100;
        /// <summary>
        /// key (byte) value represents action,
        ///     create enumeration for that
        /// When deriving:
        /// If controller is not keyboard, make Keys enumeration mean something else,
        ///      create enumeration for that controller.
        /// </summary>
        private Dictionary<byte, Keys> actions;

        //Currently pressed keys
        //only those that are in action dictionary are recorded
        private HashSet<byte> pressedKeys = new HashSet<byte>();

        //Gets called every frame while pressed
        private Dictionary<byte, List<EventHandler<ControlsEventArgs>>> downHandlers =
            new Dictionary<byte, List<EventHandler<ControlsEventArgs>>>();
        //Gets called once, when pressed
        private Dictionary<byte, List<EventHandler<ControlsEventArgs>>> pressedHandlers =
            new Dictionary<byte, List<EventHandler<ControlsEventArgs>>>();
        //Gets called once, when released
        private Dictionary<byte, List<EventHandler<ControlsEventArgs>>> releasedHandlers =
            new Dictionary<byte, List<EventHandler<ControlsEventArgs>>>();

        public Controls(Dictionary<byte, Keys> actions_) : base(null)
        {
            Actions = actions_;
        }

        protected Dictionary<byte, Keys> Actions
        {
            get { return actions; }
            private set { actions = value; }
        }

        public abstract bool IsKeyPressed(byte action);
        public abstract bool IsKeyUp(byte action);

        /// <summary>
        /// Calls event handlers
        /// Derived classes must oveverride this and get state before this is executed
        /// Firing event every fram while key is pressed (FIXME?)
        /// </summary>
        public override void Update(GameTime gameTime)
        {
            var actionList = actions.ToList();
            foreach (var pair in actionList)
            {
                if (IsKeyPressed(pair.Key))
                {
                    //record that it is currently pressed
                    if (!pressedKeys.Contains(pair.Key))
                    {
                        pressedKeys.Add(pair.Key);
                        //call pressedHandlers once
                        if (pressedHandlers.ContainsKey(pair.Key))
                            CallHandlers(pressedHandlers[pair.Key], pair.Key);
                    }
                    //calling every frame while pressed
                    if (downHandlers.ContainsKey(pair.Key))
                        CallHandlers(downHandlers[pair.Key], pair.Key);
                }
            }

            //check for released keys
            var keys = pressedKeys.ToList();
            foreach (byte key in keys)
            {
                if (IsKeyUp(key))
                {
                    pressedKeys.Remove(key);
                    if (releasedHandlers.ContainsKey(key))
                        CallHandlers(releasedHandlers[key], key);
                }
            }
        }

        void CallHandlers(List<EventHandler<ControlsEventArgs>> handlerList, byte key)
        {
            foreach (var handler in handlerList)
                CallHandler(key, handler);
        }

        public void RegisterDownHandler(byte action, EventHandler<ControlsEventArgs> handler)
        {
            RegisterHandler(downHandlers, action, handler);
        }
        public void RegisterReleasedHandler(byte action, EventHandler<ControlsEventArgs> handler)
        {
            RegisterHandler(releasedHandlers, action, handler);
        }
        public void RegisterPressedHandler(byte action, EventHandler<ControlsEventArgs> handler)
        {
            RegisterHandler(pressedHandlers, action, handler);
        }
        private void RegisterHandler(Dictionary<byte, List<EventHandler<ControlsEventArgs>>> handlers,
            byte action, EventHandler<ControlsEventArgs> handler)
        {
            if (handler == null)
                throw new NullReferenceException();
            if (!actions.ContainsKey(action))
                throw new ArgumentException("Can't register handler, because no action " + 
                    action.ToString() + "registered");
            List<EventHandler<ControlsEventArgs>> list;
            if (!handlers.ContainsKey(action))
            {
                list = new List<EventHandler<ControlsEventArgs>>();
                handlers[action] = list;
            }
            else
                list = handlers[action];
            handlers[action].Add(handler);
        }

        /// <summary>
        /// Call handler with proper ControlsEventArgs set
        /// </summary>
        protected abstract void CallHandler(byte action, EventHandler<ControlsEventArgs> handler);
        public void AddActions(Dictionary<byte, Keys> dict)
        {
            var list = dict.ToList();
            foreach (var pair in list)
                AddAction(pair.Key, pair.Value);
        }
        /// <summary>
        /// Use this if you don't want to specify any action, just handle key
        /// </summary>
        public byte AddActionDefault(Keys key)
        {
            byte action = defaultAction;
            while (actions.ContainsKey(action))
                action++;
            AddAction(action, key);
            return action;
        }
        /// <summary>
        /// This can override existing value!
        /// </summary>
        public void AddAction(byte action, Keys key)
        {
            actions[action] = key;
        }
        /// <summary>
        /// position of pointable device,
        /// if controller doesnt have something like that throw not implemented exc.
        /// </summary>
        /// <returns></returns>
        public abstract Point GetPosition();
        public List<byte> GetActiveActions()
        {
            List<byte> active = new List<byte>();
            foreach (KeyValuePair<byte, Keys> key in actions)
            {
                if (IsKeyPressed(key.Key))
                    active.Add(key.Key);
            }
            return active;
        }
    }
}
