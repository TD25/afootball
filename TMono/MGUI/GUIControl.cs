﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using TMono.TControls;

namespace TMono.MGUI
{
    public abstract class GUIControl : PositionedGUIComponent
    {
        private Controls controls;

        protected Controls Controls
        {
            private set
            {
                controls = value;
            }
            get { return controls; }
        }

        public GUIControl(Controls controls) 
            : this(controls, new Vector2(0, 0), null)
        { }
        public GUIControl(Controls controls, GUIContainer parent)
            : this(controls, new Vector2(0, 0), parent)
        { }
        public GUIControl(Controls controls, Vector2 position, GUIContainer parent = null)
            : base(parent, position)
        {
            Controls = controls;
        }
    }
}
