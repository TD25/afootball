using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TMono;
using Jitter.Collision;
using Jitter.Collision.Shapes;
using Jitter.Dynamics;
using Jitter.LinearMath;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using System.Threading;
using System.Diagnostics;
using TMono.TControls;
using TMono.Physics;
using Microsoft.Xna.Framework.Audio;


namespace AFootball
{
    class MainScene : Scene
    {
        protected AFootball parent;
        FootballerType playerType;
        Camera camera;
        GameObject player;
		//all footballers in scene excluding user
		protected List<GameObject> footballers = new List<GameObject>();
		protected AITeam aiTeam;
		public delegate void AddFootballerDel(GameObject footballer);
		public delegate void RemoveFootballerDel(GameObject footballer);
        const int halfWidth = 100 / 2;
        private Controls controls;  //for creating player
        private FootballerPhysCaps firstAICaps = new FootballerPhysCaps(5, 15, 0.95f);
        private FootballerPhysCaps defaultAICaps;
        private int cameraHeight = 30;
        private CollisionSolver solver;
        private Ball ball;
        private Matrix initialTransform;
        private const float negativeBounds = -50; // How much backwards can the player go until game over
		private SoundEffectInstance collisionEffect = null;
		private SoundEffectInstance fallEffect = null;
        const float maxAspectRatio = (float)9 / (float)8;
        const float minAspectRatio = (float)9 / (float)16;

		internal AITeam AITeam
		{
			get { return aiTeam; }
		}
        internal GameObject Player
        {
            get { return player; }
        }
		internal Ball Ball
		{
			get { return ball; }
		}

        public MainScene(CollisionSystem collSystem, AFootball parent_, SpriteBatch batch, Controls controls) 
            : base(collSystem, parent_, batch)
        {
            parent = parent_;
            Enabled = false;
            this.controls = controls;
            defaultAICaps = firstAICaps;
        }

        /// <summary>
        /// This should be called after LoadContent
        /// </summary>
        public override void Initialize()
        {
            int preferredWidth = GraphicsComponent.GManager.PreferredBackBufferWidth;
            int preferredHeight = GraphicsComponent.GManager.PreferredBackBufferHeight;
            float aspect;
#if !ANIMATION_TESTING
            camera = new Camera(new Vector3(0, 15, 0), Vector3.UnitY,
                new Vector3(0, 15, cameraHeight), CreateProjectionMatrix(preferredWidth, preferredHeight, out aspect),
                Node);
            camera.UpdateOrder = 1;
            parent.Components.Add(camera);
#else
            camera = new Camera(new Vector3(0, 5, 0), Vector3.UnitY,
                    new Vector3(0, 5, 10), CreateProjectionMatrix(preferredWidth, preferredHeight, out aspect),
                    Node);
#endif
            Node ballNode = new Node("ball", Node, 
                Matrix.CreateWorld(new Vector3(0, 50, 3), new Vector3(0, -1, 0), new Vector3(1, 0, 0)));
            ball = new Ball(ballNode, camera);
            AddObject(ball);
            FootballerPhysics.Ball = ball;

            InitPlayers();

            AddObject(new Plane(camera, Node));

#if (BALL_PHYSICS_TESTING)
            ((UserController)player.Controller).ball = (DynamicPhysics)ball.Physics;

            var camController = new CameraController(camera.World, ball.Physics,
                new Vector3(10, 5, float.NaN), 22);

            Matrix t = world.Matrix;
            t.Translation += new Vector3(0, 3, 0);
            TMatrix world1 = new TMatrix(t);
            AddObject(new Ball(world1, camera));
#else
            var camController = new CameraController(camera.Node, player.Physics, 23);
            camera.Phys = camController;

            FixCameraLimits(aspect);
#endif
            camera.Node.ComputeWorldTransform();

            Enabled = true;

            solver = new CollisionSolver(CollSystem);

            ThrowBall();
            // Has to be after Enabled
            //((FootballerPhysics)player.Physics).AttachBall(ball);

            base.Initialize();


        }
        private void FixCameraLimits(float aspectRatio)
        {
            var controler = ((CameraController)camera.Phys);
            //Matrix view = Matrix.CreateLookAt(new Vector3(0, 0, cameraHeight), Vector3.Zero, Vector3.UnitY);
            //Matrix proj = Matrix.CreatePerspectiveFieldOfView((float)Math.PI / 2, aspectRatio, 1, cameraHeight);
            //BoundingFrustum frustum = new BoundingFrustum(view * proj);
            //Microsoft.Xna.Framework.Plane plane = frustum.Far;
            //plane.Contains()
            float diff = maxAspectRatio - aspectRatio;
            float maxAspectDiff = maxAspectRatio - minAspectRatio;
            float maxLimitDiff = 40 - 23;
            float ratio = diff / maxAspectDiff;
            controler.LimitsX = 23 + (maxLimitDiff * ratio);

            controler.PositiveBoundaries = new Vector2(10 - (5 * ratio), -14);
            controler.NegativeBoundaries = new Vector2(-10 + (5 * ratio), -18);
            //if (aspectRatio >= 1.0f)
            //{
            //    controler.LimitsX = 23;
            //    controler.PositiveBoundaries = new Vector2(10, 0);
            //    controler.NegativeBoundaries = new Vector2(-10, -18);
            //}
            //else
            //{
            //    controler.LimitsX = 40;
            //    controler.PositiveBoundaries = new Vector2(5, 0);
            //    controler.NegativeBoundaries = new Vector2(-5, -18);
            //}
            controler.FixPosition();
        }
        private void InitPlayers()
        {
            FootballerPhysics.MinHeight = (FootballerType.FootballerHeight/2); //not forgeting plane height

			FootballerType.MainCamera = camera;
            FootballerType.Initialize();
            FootballerPhysCaps caps = new FootballerPhysCaps(10, 20, 0.95f);
#if (WINDOWS_PHONE_APP || ANDROID)
            playerType = new FootballerType(FootballerType.ControllerType.USER_TOUCH, caps, controls);
#else
            playerType = new FootballerType(FootballerType.ControllerType.USER_PC, caps, controls);
#endif
            // Create initial transform
            Vector3 pos = new Vector3(0, 0, (FootballerType.FootballerHeight/2)+1f);
            initialTransform = Matrix.CreateWorld(pos, -Vector3.UnitZ, -Vector3.UnitY);
            player = playerType.CreateObject(initialTransform);

            FootballerPhysics playerPhysics = (FootballerPhysics)player.Physics;            
            playerPhysics.Fell += OnPlayerFall;
            playerPhysics.Collided += OnPlayerCollision;
#if (INVINCIBLE_PLAYER)
            playerPhysics.CanFall = false;
#else
            playerPhysics.CanFall = true;
#endif
            AddObject(player);

			var firstAIType = new FootballerType(FootballerType.ControllerType.AI, firstAICaps,
											new AIProperties(10, 600));

            FootballerFactory factory = new FootballerFactory1(AddFootballer, 50, firstAIType);

            //factory.DefaultType = new FootballerType(FootballerType.ControllerType.AI, defaultAICaps);
			// Doesn't matter what we set here for intervals, see AITeam.InitStages()
            DifficultyStatus difficulty = new DifficultyStatus(2500, 600, 3000);
			aiTeam = new AITeam(footballers, RemoveFootballer, factory, difficulty, 100);
            aiTeam.UpdateOrder = 1;
            aiTeam.Enabled = false;
            parent.Components.Add(aiTeam);            

			//var en = aiType.CreateObject(Matrix.CreateWorld(new Vector3(0, 20, 20),
            //    -Vector3.UnitY, Vector3.UnitZ));
            //AddFootballer(en);
            //((CharacterPhysics)player.Physics).PlayClip("walk");
            ((CharacterPhysics)player.Physics).AnimPlayer.Looping = true;
        }
        
        public override void LoadContent()
        {
            Viewport vp = Camera.Viewport;

            // Load the images and sounds if necessary
            //ImageManager.Instance.LoadImages(Content);
            //SoundManager.Instance.LoadSounds(Content);

            FootballerType.LoadContent();
            Plane.LoadContent();
            Ball.LoadContent();

            base.LoadContent();
        }

        private void PositionBall()
        {
            if (ball.IsAttached)
                ball.Dettach();
            ball.SetPosition(new JVector(0, 50, FootballerType.FootballerHeight));
        }

        public void ThrowBall()
        {
			//NOTE: If you change parameters here you have to change time in
			// AFootball.FastForwardToCatch
            PositionBall();
            JVector target = player.Physics.Body.Position;
            target.Z += 1.32f;
            ball.Throw(target, (float)Math.PI / 4);
        }

        public void AddFootballer(GameObject footballer)
		{
			AddObject(footballer);
			footballers.Add(footballer);
		}

        override public void Update(GameTime time)
        {
            camera.Node.ComputeWorldTransform();
            if (Enabled)
            {
                CheckPlayer();
                aiTeam.OponentPos = player.Position;
                base.Update(time);
            }
        }

        protected override void OnEnabledChanged(object sender, EventArgs args)
        {
            base.OnEnabledChanged(sender, args);
            bool val = ((Scene)sender).Enabled;
            if (aiTeam != null)
            {
#if !ANIMATION_TESTING
                aiTeam.Enabled = val;
#else
                aiTeam.Enabled = false;
#endif
            }
            if (camera != null)
                camera.Enabled = val;
        }

        override public void Draw(GameTime time)
        {
            base.Draw(time);
        }

		private void RemoveFootballer(GameObject footballer)
		{
#if (DEBUG)
            Debug.Assert(footballers.Remove(footballer));
#else
            footballers.Remove(footballer);
#endif
            DisableObject(footballer);
		}
       private void CheckPlayer()
       {
            Vector3 position = player.Position;
           //HACK: Adding to halfWidths because of inacuracies related to plane texture
           if (position.X > halfWidth+2f || position.X < -halfWidth-1f ||
                position.Y < negativeBounds)
           {
              TMono.AudioEngine.PlayEffect("whistle");
              Enabled = false;
              GameOverEvent(0);
           }
       }
       //private int gameOverCnt = 0;
       private Timer timer;
       private void GameOverEvent(int delay)
       {
            TimerCallback gameOverCb = new TimerCallback((Object) => { parent.OnGameOver(); });
            timer = new Timer(gameOverCb, null, delay, Timeout.Infinite);
       }
        private void OnPlayerFall(object sender, EventArgs args)
        {
			// Don't play sound effect if it is already playing
			if (fallEffect == null || fallEffect.State != SoundState.Playing)
			{
				fallEffect = TMono.AudioEngine.CreateEffectInstace("hard_tackle");
                // Might be null if sound effect are disabled
                if (fallEffect != null)
    				fallEffect.Play();
				GameOverEvent(2000);
			}
        }
        private void OnPlayerCollision(object sender, EventArgs args)
        {
			// Don't play sound effect if it is already playing
			if (collisionEffect == null || collisionEffect.State != SoundState.Playing)
			{
				collisionEffect = TMono.AudioEngine.CreateEffectInstace("tackle");
                if (collisionEffect != null)
    				collisionEffect.Play();
			}
        }
        internal void Reset()
        {
#if (BALL_PHYSICS_TESTING)
            ball.SetPosition(new JVector(3, 10, 30));
#endif
            //player.Physics.Body.Position = new JVector(0, 0, footballerHeight);
            ((FootballerPhysics)player.Physics).Reset();

            player.SetTransform(initialTransform);
            //player.SetPosition(new JVector(0, 0, (FootballerType.FootballerHeight/2)+1f));
            foreach (var footballer in footballers)
                DisableObject(footballer);
            footballers.Clear();
            ((CameraController)camera.Phys).Reset(new JVector(0, 15, cameraHeight));

            aiTeam.Reset();
            aiTeam.OponentPos = Converter.ToXNAVector(player.Physics.Body.Position);

            PositionBall();
        }
        internal void Start()
        {
            Enabled = true;
            ThrowBall();
        }
        internal void Resize(int width, int height)
        {
            float newAspectRatio;
            camera.Projection = CreateProjectionMatrix(width, height, out newAspectRatio);
            FixCameraLimits(newAspectRatio);
            var viewport = Camera.Viewport;
            viewport.Width = width;
            viewport.Height = height;
            Camera.Viewport = viewport;
            ((UserController)player.Controller).OnClientSizeChanged(this, EventArgs.Empty);
        }
        private Matrix CreateProjectionMatrix(int width, int height, out float newAspectRatio)
        {
            // Have two types of projection matrix: one for normal pc's (widescreen),
            // and another for vertical devices like phones. We don't want widescreen
            // to have too much of an advantage of seeing more of the field, so we limit
            // that aspect ratio to 9/8. 
            // Drawback of this kind of aproach is looks in wide fullscreen mode.
            // Which is very likely on windows 10 tablets, especialy if lock rotation is enabled.
            newAspectRatio = (float)width / (float)height;
            newAspectRatio = Math.Max(newAspectRatio, minAspectRatio);
            newAspectRatio = Math.Min(newAspectRatio, maxAspectRatio);
            //if (newAspectRatio >= 1.0f)
            //    aspectRatio = (float)900 / (float)800;
            //else
            //    aspectRatio = (float)9 / (float)16;
            return Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(90),
                newAspectRatio, 1f, 100f);
        }

    }

    class Plane : GameObject
    {
        private static Model model;
        // Actual (half) length of model little more than 20 000
        // If cordinates per point is 6, then you would have
        // to score arount 3 303 points to reach the end (from the middle)
        private const float initialPosY = 19680;
        public Plane(Camera camera, Node parent) : base(new Node("plane", parent, Matrix.CreateTranslation(
            new Vector3(0, initialPosY, 0))), "plane")
        {
            Graphics = new GraphicsComponent3D(node,
                camera, model);
            Physics = new StaticPhysics(node);
            Controller = new ControllerComponent(Physics);
        }
        static public void LoadContent()
        {
            model = GraphicsComponent.Content.Load<Model>(
                "plane");
         }
        public override void Initialize()
        {
            ((GraphicsComponent3D)Graphics).Model = model;
            float radius = model.Meshes[0].BoundingSphere.Radius;
            BoxShape shape = new BoxShape(new JVector(
                150, radius*4.0f, 1.0f));
            Physics.Body = new RigidBody(shape);
            Physics.Body.Position = new JVector(0, initialPosY, 0);
            Physics.Body.IsStatic = true;
            base.Initialize();
        }
    }
}
