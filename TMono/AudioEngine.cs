﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;
using System.Diagnostics;

namespace TMono
{
    static public class AudioEngine
    {
        public class Album
        {
            private Random rnd = new Random();
            private List<Song> songs;
            private int currentSongIndex;

            public string Name
            { private set; get; }
            public int CurrentSongIndex
            {
                get { return currentSongIndex; }
                set
                {
                    if (value >= songs.Count)
                        throw new IndexOutOfRangeException("No song with this index in the album");
                    currentSongIndex = value;
                }
            }
            public bool IsFinished
            { get { return currentSongIndex >= songs.Count; } }

            public Album(string name)
            {
                Name = name;
                songs = new List<Song>();
            }
            public Album(string name, List<Song> songs)
            {
                Name = name;
                this.songs = songs;
            }

            public void AddSong(Song song)
            {
                songs.Add(song);
            }

            public void AddSong(Song song, int pos)
            {
                songs.Insert(pos, song);
            }

            public void SwitchSongs(int song1Pos, int song2Pos)
            {
                Song temp = songs[song1Pos];
                songs[song1Pos] = songs[song2Pos];
                songs[song2Pos] = temp;
            }

            public void MoveSong(string songName, int pos)
            {
                Song s;
                SwitchSongs(FindSong(songName, out s), pos);
            }

            public Song GetCurrent()
            {
                return songs[currentSongIndex];
            }

            public Song NextSong()
            {
                currentSongIndex++;
                if (currentSongIndex >= songs.Count)
                    currentSongIndex = 0;
                return songs[currentSongIndex];
            }

            public void Shuffle()
            {
                for (int i = 0; i < songs.Count - 1; i++)
                {
                    int song = rnd.Next(i + 1, songs.Count);
                    SwitchSongs(i, song);
                }
            }
            public int FindSong(string name, out Song song)
            {
                int c = 0;
                foreach (Song s in songs)
                {
                    if (s.Name == name)
                    {
                        song = s;
                        return c;
                    }
                    c++;
                }
                song = null;
                return -1;
            }
        }

        private static Dictionary<string, SoundEffect> sounds = new Dictionary<string, SoundEffect>();
        private static Dictionary<string, Song> songs = new Dictionary<string, Song>();
        private static Dictionary<string, Album> albums = new Dictionary<string, Album>();
        private static Album currentAlbum;
        private static string audioDir;
        private static bool musicEnabled = true;

        public static bool MusicEnabled
        {
            get { return musicEnabled; }
            set
            {
                musicEnabled = value;
                if (musicEnabled)
                    MediaPlayer.Resume();
                else
                    MediaPlayer.Pause();
            }
        }
        public static bool SoundEffectsEnabled
        { set; get; } = true;
        public static ContentManager Content
        { set; get; }
        public static string AudioDir
        {
            get { return audioDir; }
            set
            {
                audioDir = value + "/";
            }
        }
        //TODO: Not tested:
        public static bool LoopAlbum
        { set; get; } = true;
        public static bool PlayingAlbum
        {
            get { return currentAlbum != null; }            
            set
            {
                Debug.Assert(currentAlbum != null, "Set to play and album through PlayAlbum()");
                if (!value)
                    currentAlbum = null;
            }
        }

        public static event EventHandler AlbumFinished;

        public static void Init(ContentManager contentManager, string audioDir)
        {
            AudioDir = audioDir;
            Content = contentManager;
            MediaPlayer.MediaStateChanged += OnMediaStateChanged;
        }

        /// <summary>
        /// Call for all your sound files in LoadContent of your game
        /// </summary>
        public static SoundEffect LoadEffect(string assetName)
        {
            SoundEffect sound = Content.Load<SoundEffect>(AudioDir + assetName);
            sounds.Add(assetName, sound);
            return sound;
        }

        public static Song LoadSong(string assetName)
        {
			Song song = Content.Load<Song>(AudioDir + assetName);
            songs.Add(assetName, song);
            return song;
        }

        public static Song LoadSong(string assetName, string albumName)
        {
            Song song = LoadSong(assetName);
            if (!albums.ContainsKey(albumName))
                albums.Add(albumName, new Album(albumName));
            albums[albumName].AddSong(song);
            return song;
        }

        public static void AddAlbum(Album album)
        {
            albums.Add(album.Name, album);
        }

        //TODO: Handle playing the same sound at the same sound 
        // (or not if youre fine with how monogame handles it)
        // and allow different settings with SoundEffectInstance
        public static void PlayEffect(string soundName)
        {
			if (!SoundEffectsEnabled)
				return;
            // Throws key not found
            SoundEffect sound = sounds[soundName];
			sound.Play();
        }
		/// <summary>
		/// Creates the sound effect instace and does not play it just returns it
		/// Allows you to set settings for it before playing
		/// </summary>
		/// <returns>The effect instace.</returns>
		/// <param name="soundName">Sound name.</param>
		public static SoundEffectInstance CreateEffectInstace(string soundName)
		{
			if (!SoundEffectsEnabled)
				return null;
		    // Throws key not found
            SoundEffect sound = sounds[soundName];
			return sound.CreateInstance();
		}
		public static TimeSpan GetDuration(string soundName)
		{
			return sounds[soundName].Duration;
		}

        public static void PlaySong(string songName, bool loop = false)
        {
            Song song = songs[songName];
            PlaySong(song, loop);
        }

        public static void PlaySong(Song song, bool loop = false)
        {
            MediaPlayer.Play(song);
            MediaPlayer.IsRepeating = loop;
            if (!MusicEnabled)
                MediaPlayer.Pause();
        }

        public static void PlayAlbum(string albumName)
        {
            MediaPlayer.IsRepeating = false;
            currentAlbum = albums[albumName];
            currentAlbum.CurrentSongIndex = 0;
            PlaySong(currentAlbum.GetCurrent(), false);            
        }

        public static Album GetAlbum(string albumName)
        {
            return albums[albumName];
        }

        public static void PauseSong()
        {
            MediaPlayer.Pause();
        }

        public static void ResumeSong()
        {
            if (MusicEnabled)
                MediaPlayer.Resume();
        }

        private static void OnMediaStateChanged(object sender, EventArgs e)
        {
            if (PlayingAlbum && MediaPlayer.State == MediaState.Stopped)
            {
                if (currentAlbum.IsFinished)
                {
                    AlbumFinished?.Invoke(null, EventArgs.Empty);
                    if (LoopAlbum)
                        MediaPlayer.Play(currentAlbum.NextSong());
                    else
                        MediaPlayer.Stop();
                }
                else
                    MediaPlayer.Play(currentAlbum.NextSong());
            }
        }
    }
}
