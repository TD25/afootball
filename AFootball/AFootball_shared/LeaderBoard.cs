﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using TMono;
using TMono.MGUI;
using System.Diagnostics;

namespace AFootball
{
    class LeaderBoard
    {
        private LabelComponent bestScoreLabel;
        private int bestScore = 0;
        //TODO: This should be changed each time difficulty changes, so that
        // best score is reset. 
        // Just "data" (or bestScore for windows store apps) means 0.9.0 version difficulty, before this system
        // was added.
#if (WINDOWS_DESKTOP || LINUX_DESKTOP || ANDROID)
        private const string saveFileName = "data-0-9-2";
        private string path;
#elif (WINDOWS_UAP || WINDOWS_APP || WINDOWS_PHONE_APP)
        private const string bestScoreName = "best_score-0-9-2";
#endif

        public int BestScore
        {
            get { return bestScore; }
            private set
            {
                Debug.Assert(value > bestScore);
                bestScore = value;
                bestScoreLabel.Text = "Best score: " + bestScore;
                WriteScore(bestScore);
            }
        }

        public LeaderBoard(LabelComponent bestScoreLabel)
        {
#if (WINDOWS_DESKTOP || LINUX_DESKTOP || ANDROID)
            var dataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
#if (WINDOWS_DESKTOP || LINUX_DESKTOP) 
            dataPath = Path.Combine(dataPath, "Run2TouchDown");
#endif
            if (!Directory.Exists(dataPath))
                Directory.CreateDirectory(dataPath);
            path = Path.Combine(dataPath, saveFileName);
#endif
            bestScore = ReadScore();
            this.bestScoreLabel = bestScoreLabel;
            bestScoreLabel.Text = "Best score: " + bestScore;
        }

        public void AddNewScore(int score)
        {
            if (score > bestScore)
                BestScore = score;
        }

        // Expecting this won't get called often
        // else cache file stream
        private int ReadScore()
        {
            int score;
#if (WINDOWS_DESKTOP || LINUX_DESKTOP || ANDROID)
            using (FileStream stream = new FileStream(path, FileMode.OpenOrCreate))
            {
                try
                {
                    using (BinaryReader reader = new BinaryReader(stream))
                    {
                        score = reader.ReadInt32();
                        reader.Close();
                    }
                }
                catch (EndOfStreamException)
                {
                    score = 0;
                }
            }
#elif (WINDOWS_UAP || WINDOWS_APP || WINDOWS_PHONE_APP)
            Windows.Storage.ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            var val = localSettings.Values[bestScoreName];
            if (val != null)
                score = (int)val;
            else
                score = 0;
#else
            throw new NotImplementedException();
#endif
            return score;
        }

        private void WriteScore(int score)
        {
#if (WINDOWS_DESKTOP || LINUX_DESKTOP || ANDROID)
            using (FileStream stream = new FileStream(path, FileMode.OpenOrCreate))
            {
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    writer.Write(score);
                    writer.Close();
                }
            }
#elif (WINDOWS_UAP || WINDOWS_APP || WINDOWS_PHONE_APP)
            Windows.Storage.ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            localSettings.Values[bestScoreName] = score;
#else
            throw new NotImplementedException();
#endif
        }

    }
}
