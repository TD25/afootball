﻿using System;
using TMono;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace AFootball
{
    class FootballerFactory
    {
		protected Vector3 oponentPos = Vector3.Zero;         //player position
		protected List<FootballerType> fTypes = new List<FootballerType>();
		protected int startDistance = 50; //how much in front of oponent
		protected MainScene.AddFootballerDel addFootballer;
        DifficultyStatus diffStatus;

        public DifficultyStatus DifficultyStatus
        {
            set { diffStatus = value; }
            get { return diffStatus; }
        }

		/// <summary>
		/// Gets or sets the oponent position.
		/// Update this before every Update call
		/// </summary>
		/// <value>The oponent position.</value>
		public Vector3 OponentPos
		{
			get { return oponentPos; }
			set { oponentPos = value; }
		}
		public bool Enabled
		{
			set; get;
		}

		public FootballerFactory(MainScene.AddFootballerDel addFootballerDel, int startDistance, 
            FootballerType firstType)
		{
			Enabled = true;
            addFootballer = addFootballerDel;
            this.startDistance = startDistance;
            fTypes.Add(firstType);
		}

        public FootballerFactory(FootballerFactory factory)
        {
            Enabled = true;
            addFootballer = factory.addFootballer;
            startDistance = factory.startDistance;
            fTypes.Add(new FootballerType(factory.fTypes[0]));
        }

        public virtual FootballerFactory Clone()
        {
            return new FootballerFactory(this);
        }

		protected void AddType(FootballerType type)
		{
			fTypes.Add(type);
		}
		protected virtual Vector3 GetSpawnPos()
		{
			return new Vector3(oponentPos.X, oponentPos.Y + startDistance, oponentPos.Z);
		}
        public virtual void Spawn(GameTime time)
        {
            if (Enabled)
                AddFromType(fTypes[0]);
        }
        protected void AddFromType(FootballerType type)
        {
            addFootballer(type.CreateObject(GetSpawnPos()));
        }
		protected GameObject CreateFromType(FootballerType type)
		{
			return type.CreateObject(GetSpawnPos());
		}
        public FootballerType GetType(int ind)
        {
            return fTypes[ind];
        }
    }

    class FootballerFactory1 : FootballerFactory
    {
        private Stage currentStage;

        public Stage CurrentStage
        {
            set { currentStage = value; }
            get { return currentStage; }
        }

        public FootballerFactory1(MainScene.AddFootballerDel addFootballerDel, int startDistance_,
		                            FootballerType firstType) : base(addFootballerDel, startDistance_, 
                                                                       firstType)
        {
        }

        public FootballerFactory1(FootballerFactory1 factory)
            : base(factory)
        {
            CurrentStage = factory.CurrentStage;
        }

        public override FootballerFactory Clone()
        {
            return new FootballerFactory1(this);
        }

        public override void Spawn(GameTime time)
        {
            if (!Enabled)
                return;
			SpawnType spawnType;
            List<FootballerType> types = currentStage.ChooseTypeToSpawn(out spawnType);
            int diffSum = 0;
            int count = 0;
            Vector3 pos = GetSpawnPos();
            foreach (var type in types)
            {
				Vector3 newPos = pos;
				if (spawnType == SpawnType.NUMBERS_VERTICAL)
					newPos.Y += count * 10;
				else if (spawnType == SpawnType.NUMBERS_HORIZONTAL)
				{
					int direction = (count % 2 == 0) ? -1 : 1;
					newPos.X += (float)(direction * Math.Ceiling((double)count / 2) * 5);
				}
                count++;
                GameObject footballer = type.CreateObject(newPos);
                addFootballer(footballer);
                diffSum += DifficultyStatus.CalculateDiff(type);
            }
            diffSum += DifficultyStatus.GetSpawnIntervalWeight(DifficultyStatus.SpawnInterval) +
			                           DifficultyStatus.GetNumbersWeight(count);
            int difference = DifficultyStatus.CurrentDifficulty - diffSum;
            if (difference > 1)
                currentStage.Improve(difference);
        }
    }
}

