﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace TMono
{
    // Should be in TMono library, but you can't put platform specific implementations
    // into portable library
    static class OS 
    {
        public static void LaunchUrl(string url)
        {
#if (WINDOWS_DESKTOP || LINUX_DESKTOP || ANDROID)
            Process process = Process.Start(url);
#elif (WINDOWS_UAP || WINDOWS_PHONE_APP || WINDOWS_APP)
            Uri uri = new Uri(url);
            Windows.System.Launcher.LaunchUriAsync(uri);
#else
            throw new NotImplementedException();
#endif
        }
    }
}
