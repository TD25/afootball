using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Jitter.Collision;
using TMono;
using TMono.MGUI;
using TMono.TControls;
using System.Collections.Generic;
using System;
using System.Diagnostics;
using Microsoft.Xna.Framework.Media;
using System.Reflection;
#if (ANDROID)
using Android.Views;
#endif

namespace AFootball
{
	/// <summary>
	/// This is the main type for your game.
	/// </summary>
	public class AFootball : TGame
	{
		private CollisionSystem colSystem;
		private MGUI guiSystem;
		private GUIContainer inGameGui;
		private GUIContainer mainMenu;
		private GUIContainer inGamePause;
		private GUIContainer gameOverMenu;
		private LabelComponent finalScoreLabel;
		private LabelComponent pausedScoreLabel;
		private ScoreComponent scoreBox;
		private GUIContainer creditsWindow;
		private GUIContainer buttons;
		private GUIContainer settingsWindow;
		private const int virtualWidth = 1080;
		private const int virtualHeight = 1794;
		private State state = State.NotInitialized;
		private State prevState = State.NotInitialized;
		private LeaderBoard leaderBoard;
		private readonly TimeSpan ballFlyTime = TimeSpan.FromSeconds(3);
		private FootballerPhysics plPhys;
		private AITeam aiTeam;

		public enum State
		{
			NotInitialized, MainMenu, Paused, GameOver, Credits, Settings,
			InGame
		}

		public State CurrentState
		{ get { return state; } }

		public AFootball() : base()
		{

#if (!WINDOWS_PHONE_APP && !ANROID)  //TODO: add other mobile platforms where needed
			IsMouseVisible = true;
#endif
			PrepareGraphicsSettings();
		}

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize()
		{
			// Resizing which couldn't be done earlier because, we didn't
			// know screen size
			ResizeWindow();

			Viewport vp = GraphicsDevice.Viewport;
			//vp.Height = graphics.PreferredBackBufferHeight;
			//vp.Width = graphics.PreferredBackBufferWidth;
			Camera.Viewport = vp;

			// set sampler state for models to draw
			objSamplerState = SamplerState.PointWrap;

			colSystem = new CollisionSystemSAP();

			base.Initialize();
		}

		private void ResizeWindow()
		{
#if (WINDOWS_DESKTOP || LINUX_DESKTOP)
			// Make window vertical to be more like on phones
			// Can't do this in constructor because device is not yet created, and 
			// we don't know screen size           
			//FIXME: Expecting widescreen (or at least square)
			// For windows uap we set prefered size in GamePage constructor, 
			// for windows 8.1 there is no way to set preferred window size,
			// if you set buffer dimensions here, everything gets stretched, but
			// window size stays the same on windows 8.1
			var screenWidth = graphics.GraphicsDevice.DisplayMode.Width;
			var screenHeight = graphics.GraphicsDevice.DisplayMode.Height;

			graphics.PreferredBackBufferHeight = screenHeight - 100;
			graphics.PreferredBackBufferWidth = graphics.PreferredBackBufferHeight + 100;
			//graphics.PreferredBackBufferHeight = 800;
			//graphics.PreferredBackBufferWidth = 600;

			// Place window at the top
			Point pos = Window.Position;
			pos.Y = 10;
			Window.Position = pos;

			graphics.ApplyChanges();

#elif (WINDOWS_PHONE_APP)
            // Backbuffer width and height need to be swapped according to window bounds
			graphics.PreferredBackBufferHeight = graphics.GraphicsDevice.DisplayMode.Height;
			graphics.PreferredBackBufferWidth = graphics.GraphicsDevice.DisplayMode.Width;
            graphics.ApplyChanges();
			OnClientSizeChanged(this, EventArgs.Empty);
#endif
		}

		/// <summary>
		/// This should be called in constructor until graphics device is not yet
		/// created
		/// </summary>
		private void PrepareGraphicsSettings()
		{
#if (WINDOWS_DESKTOP || LINUX_DESKTOP)
			Window.AllowUserResizing = true;
#elif (ANDROID || WINDOWS_PHONE_APP)
            graphics.IsFullScreen = true;
            graphics.SupportedOrientations = DisplayOrientation.Portrait;
#endif

			//FIXME: Ball texture blinks while moving
			// AFootball_81U fails with multisampling set, UWP displays blank, works on android
			//graphics.PreferMultiSampling = true;
			graphics.GraphicsProfile = GraphicsProfile.Reach;

			Window.ClientSizeChanged += OnClientSizeChanged;
			// Settings will be applied when creating device
		}

		private void OnClientSizeChanged(object sender, EventArgs e)
		{
			// Expecting window dimensions to have changed, because
			// sender is usually Window except in ResizeWindow in wp8.1
			int width = Window.ClientBounds.Width;
			int height = Window.ClientBounds.Height;
			if (rootScene != null)
				((MainScene)rootScene).Resize(width, height);
			if (guiSystem != null)
			{
				guiSystem.SetSize(new Point(width, height));
				guiSystem.ScaleForResolution(virtualWidth, virtualHeight);
			}
		}

		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent()
		{
			Rectangle displayRect = new Rectangle(0, 0,
				graphics.GraphicsDevice.PresentationParameters.BackBufferWidth,
				graphics.GraphicsDevice.PresentationParameters.BackBufferHeight);

			//Init controls
			Controls controls;
#if (WINDOWS_PHONE_APP || ANDROID)
            controls = new TouchControls(new Dictionary<byte, Keys>());
#elif (WINDOWS_DESKTOP || LINUX_DESKTOP)
			controls = new PCControls(new Dictionary<byte, Keys>());
#elif (WINDOWS_UAP || WINDOWS_APP)
            controls = new UniversalControls(new Dictionary<byte, Keys>());
#endif
			//has to be less than default ControllerComponent.UpdateOrder in GameObject
			//so that the state is set before any polling to it.
			//And before PhysicsCompoenent update
			controls.UpdateOrder = 5;
			Components.Add(controls);

			LoadAudio();

			rootScene = new MainScene(colSystem, this, spriteBatch, controls);

			base.LoadContent(); //Initialize root scene

			// Handle these actions so we can catch when user wants to fast forward
			controls.RegisterPressedHandler((byte)UserController.Actions.GO_AT_POINTED, OnPress);
			controls.RegisterPressedHandler((byte)UserController.Actions.GO_BACKWARD, OnPress);
			controls.RegisterPressedHandler((byte)UserController.Actions.GO_FORWARD, OnPress);
			controls.RegisterPressedHandler((byte)UserController.Actions.GO_LEFT, OnPress);
			controls.RegisterPressedHandler((byte)UserController.Actions.GO_RIGHT, OnPress);

			MainScene scene = (MainScene)rootScene;
			plPhys = (FootballerPhysics)scene.Player.Physics;
			aiTeam = scene.AITeam;

			InitGUI(controls, displayRect);

			// InitState starts playing sound and goes to main menu, we don't want to do that yet
			// if app is prelaunched on windows store versions. So instead we rely on GamePage to call IniState when
			// window becomes visible. However currently we just don't launch monogame at all if app is prelaunched (see App.xaml.cs)
#if (!WINDOWS_UAP)
			InitState();
#else
            OnStateChange(); // State changed to not initialized
#endif
		}

		private void OnPress(object sender, EventArgs args)
		{
			// if ball is not caught yet
			if (state == State.InGame && !plPhys.HasBall && !plPhys.Catching &&
					aiTeam.TotalGameTime < ballFlyTime)
			{
				FastForwardToCatch();
			}
		}

		private void LoadAudio()
		{
			AudioEngine.Init(Content, "audio");
			AudioEngine.LoadEffect("catch");
			AudioEngine.LoadEffect("kick");
			AudioEngine.LoadEffect("tackle");
			AudioEngine.LoadEffect("hard_tackle");
			AudioEngine.LoadEffect("whistle");
			AudioEngine.LoadSong("The-Games", "all");
			AudioEngine.LoadSong("Frantic-Gameplay", "all");
			AudioEngine.LoopAlbum = true;
		}

		/// <summary>
		/// UnloadContent will be called once per game and is the place to unload
		/// game-specific content.
		/// </summary>
		protected override void UnloadContent()
		{
			// TODO: Unload any non ContentManager content here
		}

		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update(GameTime gameTime)
		{
			// Remove this directive if you add gamepad support to desktop
#if (WINDOWS_PHONE_APP || ANDROID || WINDOWS_UAP)
            var buttonState = GamePad.GetState(PlayerIndex.One);
            if (buttonState.Buttons.Back == ButtonState.Pressed)
                OnBack(this, EventArgs.Empty);
#endif
			base.Update(gameTime);
		}

		private void FastForwardToCatch()
		{
			FootballerPhysics.Ball.Physics.Enabled = false;

			TimeSpan time = ballFlyTime - aiTeam.TotalGameTime;
			Debug.Assert(time.TotalMilliseconds > 0, "FastForwardToCatch called, at the bad time");
			FastForward(time);
			plPhys.PlayCatchAnimation();
			plPhys.AttachBall(FootballerPhysics.Ball);
		}

        private void GoToMainMenu()
        {
            state = State.MainMenu;
            OnStateChange();
            ((MainScene)rootScene).Reset();
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }

        //TODO: Make separate class
        private void InitGUI(Controls controls, Rectangle displayRect)
        {
            LinkComponent.LaunchUrl = OS.LaunchUrl;

            //TODO: Use bigger font instead of scaling it up.
            SpriteFont mediumFont = GraphicsComponent.Content.Load<SpriteFont>("Allstar_medium");
            SpriteFont largeFont = GraphicsComponent.Content.Load<SpriteFont>("Allstar_large");

            guiSystem = new MGUI(spriteBatch, Components, displayRect);
            guiSystem.Initialize();
            guiSystem.ScaleForResolution(virtualWidth, virtualHeight);
            //guiSystem.Scale = new Vector2(0.5f);

            Point buttonSize = new Point(536, 339); //original
            //Main menu
            mainMenu = new GUIContainer(displayRect);
            mainMenu.InheritParentSize = true;
            guiSystem.AddComponent(mainMenu);
            //LabelComponent title = new LabelComponent(largeFont, Color.White);
            //title.Text = "Run2Touchdown";
            //mainMenu.AddComponent(title);
            Texture2D titleText = Content.Load<Texture2D>("textures/title");
            TextureComponent title = new TextureComponent(titleText, Color.White, titleText.Bounds.Size);
            title.Scale = new Vector2(1, 2);
            title.SetAlignmet(VerticalPosition.CENTER, HorizontalPosition.CENTER);
            mainMenu.AddComponent(title);
            title.PositionBetween(new Position(HorizontalPosition.CENTER, VerticalPosition.TOP),
                new Position(HorizontalPosition.CENTER, VerticalPosition.CENTER), 0.5f, 0.0f);
            Texture2D startTexture = Content.Load<Texture2D>("textures/football");
            TextureComponent startText = new TextureComponent(startTexture, Color.White, buttonSize);
            LabelComponent startLabel = new LabelComponent(mediumFont, Color.White);
            startLabel.Text = "START";
            Button startButton = new Button(controls, startText);
            startButton.Label = startLabel;
            startButton.VerticalAlignment = VerticalPosition.CENTER;
            startButton.HorizontalAlignment = HorizontalPosition.CENTER;
            mainMenu.AddComponent(startButton, HorizontalPosition.CENTER, VerticalPosition.CENTER);
            startButton.Clicked += OnStart;
            Texture2D closeButtonTex = Content.Load<Texture2D>("textures/close-button");
            // Don't add exit button if platform does not allow programaticaly closing
#if (!WINDOWS_APP)
            Button exitButton = new Button(controls, new TextureComponent(closeButtonTex, Color.White, new Point(100, 100)));
            exitButton.SetAlignmet(VerticalPosition.TOP, HorizontalPosition.RIGHT);
            mainMenu.AddComponent(exitButton);
            exitButton.AlignToParent(VerticalPosition.TOP, HorizontalPosition.RIGHT);
            // Behaviour of this button is the same as after back button pressed while in main menu
            exitButton.Clicked += OnBack;
#endif

            //Buttons which are visible on all menus
            buttons = new GUIContainer(displayRect);
            buttons.InheritParentSize = true;
            guiSystem.AddComponent(buttons);
            Texture2D infoText = Content.Load<Texture2D>("textures/info");
            Button infoButton = new Button(controls, new TextureComponent(infoText, Color.White, new Point(200, 200)));
            infoButton.SetAlignmet(VerticalPosition.CENTER, HorizontalPosition.CENTER);
            buttons.AddComponent(infoButton);
            infoButton.PositionBetween(new Position(HorizontalPosition.CENTER, VerticalPosition.CENTER),
                new Position(HorizontalPosition.RIGHT, VerticalPosition.BOTTOM), 0.75f, 0.5f);
            infoButton.Clicked += OnInfoClicked;

            Texture2D settingsText = Content.Load<Texture2D>("textures/settings");
            Button settingsButton = new Button(controls, new TextureComponent(settingsText, Color.White, new Point(200, 200)));
            settingsButton.SetAlignmet(VerticalPosition.CENTER, HorizontalPosition.CENTER);
            buttons.AddComponent(settingsButton);
            settingsButton.PositionBetween(new Position(HorizontalPosition.LEFT, VerticalPosition.CENTER),
                new Position(HorizontalPosition.CENTER, VerticalPosition.BOTTOM), 0.75f, 0.5f);
            settingsButton.Clicked += OnSettingsClicked;

            LabelComponent bestScoreLabel = new LabelComponent(mediumFont, Color.White);
            bestScoreLabel.SetAlignmet(VerticalPosition.BOTTOM, HorizontalPosition.CENTER);
            buttons.AddComponent(bestScoreLabel);
            bestScoreLabel.AlignToParent(VerticalPosition.BOTTOM, HorizontalPosition.CENTER);
            leaderBoard = new LeaderBoard(bestScoreLabel);

            //credits window
            Rectangle win = new Rectangle(0, 0, (int)(virtualWidth * 0.8f),
                (int)(virtualHeight * 0.6f));
            creditsWindow = new GUIContainer(win);
            creditsWindow.MakeFit = true;
            //Draw window in separate batch to enable cliping of text which does not fit
            //This also makes sure that draw order is big enough so that window overlays the other menus
            creditsWindow.SeparateBatch = true;
            creditsWindow.MinScale = new Vector2(0.3f, 0.3f);
            // We want credits to overlay other menus, so we add enough to draw order
            guiSystem.AddComponent(creditsWindow);
            creditsWindow.SetAlignmet(VerticalPosition.CENTER, HorizontalPosition.CENTER);
            creditsWindow.AlignToParent(VerticalPosition.CENTER, HorizontalPosition.CENTER);
            Color col = new Color(0, 0, 0, 0.8f);
            Texture2D overlayText = TextureComponent.CreateTexture(GraphicsDevice, col);
            creditsWindow.BackgroundTexture = overlayText;
            Button closeButton = new Button(controls, new TextureComponent(closeButtonTex, Color.White, 
                new Point(100, 100)));
            closeButton.SetAlignmet(VerticalPosition.TOP, HorizontalPosition.RIGHT);
            creditsWindow.AddComponent(closeButton);
            closeButton.AlignToParent(VerticalPosition.TOP, HorizontalPosition.RIGHT);
            closeButton.Clicked += OnCloseButton;
            AddCreditLabels(creditsWindow, mediumFont, controls);

            //settings window
            Rectangle settingsRect = new Rectangle(0, 0, (int)(virtualWidth * 0.8f),
                (int)(virtualHeight * 0.3f));
            settingsWindow = new GUIContainer(settingsRect);
            settingsWindow.MakeFit = true;
            //Draw window in separate batch to enable cliping of text which does not fit
            //This also makes sure that draw order is big enough so that window overlays the other menus
            settingsWindow.SeparateBatch = true;
            settingsWindow.MinScale = new Vector2(0.3f, 0.3f);
            // We want credits to overlay other menus, so we add enough to draw order
            guiSystem.AddComponent(settingsWindow);
            settingsWindow.SetAlignmet(VerticalPosition.CENTER, HorizontalPosition.CENTER);
            settingsWindow.AlignToParent(VerticalPosition.CENTER, HorizontalPosition.CENTER);
            settingsWindow.BackgroundTexture = overlayText;
            Button closeButton1 = new Button(controls, new TextureComponent(closeButtonTex, Color.White, 
                new Point(100, 100)));
            closeButton1.SetAlignmet(VerticalPosition.TOP, HorizontalPosition.RIGHT);
            settingsWindow.AddComponent(closeButton1);
            closeButton1.AlignToParent(VerticalPosition.TOP, HorizontalPosition.RIGHT);
            closeButton1.Clicked += OnCloseButton;
            AddSettings(settingsWindow, mediumFont, controls);
            //settingsWindow.Scalable = false;

            //In game GUI
            inGameGui = new GUIContainer(displayRect);
            inGameGui.InheritParentSize = true;
            guiSystem.AddComponent(inGameGui);
            Texture2D texture = GraphicsComponent.Content.Load<Texture2D>("textures/pause17");
            TextureComponent text = new TextureComponent(texture, Color.White, new Point(100, 100));
            Button pauseButton = new Button(controls, text);
            pauseButton.HorizontalAlignment = HorizontalPosition.RIGHT;
            pauseButton.VerticalAlignment = VerticalPosition.TOP;
            pauseButton.Pressed += OnPause; //not using clicked so that it stops immediately (footballers stop)
            pauseButton.UpdateOrder = 5; //before UserController
            inGameGui.AddComponent(pauseButton, HorizontalPosition.RIGHT, VerticalPosition.TOP);
            // Add scorebox
            LabelComponent scoreText = new LabelComponent(largeFont, Color.White, new Vector2(0, 0), string.Empty);
            scoreBox = new ScoreComponent(scoreText, ((MainScene)rootScene).Player);
            scoreBox.UpdateOrder = 1;
            inGameGui.AddComponent(scoreText, HorizontalPosition.LEFT, VerticalPosition.TOP);
            Components.Add(scoreBox);

            //in game menu
            inGamePause = new GUIContainer(displayRect);
            inGamePause.InheritParentSize = true;
            guiSystem.AddComponent(inGamePause);
            texture = Content.Load<Texture2D>("textures/pause17");
            text = new TextureComponent(texture, Color.White, new Point(180, 180));
            text.SetAlignmet(VerticalPosition.CENTER, HorizontalPosition.CENTER);
            inGamePause.AddComponent(text);
            text.PositionBetween(new TMono.MGUI.Position(HorizontalPosition.CENTER, VerticalPosition.TOP),
                new Position(HorizontalPosition.CENTER, VerticalPosition.CENTER), 0.5f, 0.0f);
            startText = new TextureComponent(startTexture, Color.White, buttonSize);
            Button continueButton = new Button(controls, startText);
            //continueButton.LabelPlacement = new Position(HorizontalPosition.RIGHT, VerticalPosition.TOP);
            continueButton.SetAlignmet(VerticalPosition.CENTER, HorizontalPosition.CENTER);
            startLabel = new LabelComponent(mediumFont, Color.White);
            startLabel.Text = "CONTINUE";
            startLabel.Scale = new Vector2(0.75f, 0.75f);
            continueButton.Label = startLabel;
            inGamePause.AddComponent(continueButton);
            continueButton.PositionBetween(new Position(HorizontalPosition.CENTER, VerticalPosition.BOTTOM),
                new Position(HorizontalPosition.CENTER, VerticalPosition.CENTER), 0.5f, 0.0f);
            pausedScoreLabel = new LabelComponent(largeFont, Color.White);
            inGamePause.AddComponent(pausedScoreLabel, HorizontalPosition.LEFT, VerticalPosition.TOP);
            continueButton.Clicked += OnContinue; //has to be clicked so that footballer does not react to this press
            Texture2D backTexture = Content.Load<Texture2D>("textures/back_button");
            Button backButton = new Button(controls, new TextureComponent(backTexture, Color.White, new Point(120, 120)));
            backButton.SetAlignmet(VerticalPosition.TOP, HorizontalPosition.RIGHT);
            inGamePause.AddComponent(backButton);
            backButton.AlignToParent(VerticalPosition.TOP, HorizontalPosition.RIGHT);
            backButton.Clicked += OnBack;

            //game over menu
            gameOverMenu = new GUIContainer(displayRect);
            guiSystem.AddComponent(gameOverMenu);
            gameOverMenu.InheritParentSize = true;
            LabelComponent label = new LabelComponent(largeFont, Color.White);
            label.Text = "GAME OVER";
            label.SetAlignmet(VerticalPosition.CENTER, HorizontalPosition.CENTER);
            //label.Scale = new Vector2(2, 2);
            gameOverMenu.AddComponent(label);
            label.PositionBetween(new Position(HorizontalPosition.CENTER, VerticalPosition.TOP),
                new Position(HorizontalPosition.CENTER, VerticalPosition.CENTER), 0.5f, 0.0f);
            //Add final score 
            finalScoreLabel = new LabelComponent(largeFont, Color.White);  
            finalScoreLabel.SetAlignmet(VerticalPosition.CENTER, HorizontalPosition.CENTER);
            gameOverMenu.AddComponent(finalScoreLabel, HorizontalPosition.CENTER, VerticalPosition.CENTER);
            //finalScoreLabel.PositionBetween(new Position(HorizontalPosition.CENTER, VerticalPosition.CENTER),
            //    new Position(HorizontalPosition.CENTER, VerticalPosition.TOP), 0.2f, 0.0f);

            startText = new TextureComponent(startTexture, Color.White, buttonSize);
            Button restartButton = new Button(controls, startText);
            restartButton.SetAlignmet(VerticalPosition.CENTER, HorizontalPosition.CENTER);
            label = new LabelComponent(mediumFont, Color.White);
            label.Text = "RESTART";
            label.Scale = new Vector2(0.85f, 0.85f);
            restartButton.Label = label;
            gameOverMenu.AddComponent(restartButton);
            restartButton.PositionBetween(new Position(HorizontalPosition.CENTER, VerticalPosition.BOTTOM),
                new Position(HorizontalPosition.CENTER, VerticalPosition.CENTER), 0.5f, 0.0f);
            restartButton.Clicked += OnRestart;
            Button backButton2 = new Button(controls, new TextureComponent(backTexture, Color.White, new Point(120, 120)));
            backButton2.SetAlignmet(VerticalPosition.TOP, HorizontalPosition.RIGHT);
            gameOverMenu.AddComponent(backButton2);
            backButton2.AlignToParent(VerticalPosition.TOP, HorizontalPosition.RIGHT);
            backButton2.Clicked += OnBack;


            // Register hotkeys for buttons
#if (WINDOWS_DESKTOP || LINUX_DESKTOP || WINDOWS_UAP || WINDOWS_APP)
            pauseButton.AddHotkey(Keys.Escape);
            continueButton.AddHotkey(Keys.Escape);
            continueButton.AddHotkey(Keys.Space);
            startButton.AddHotkey(Keys.Space);
            restartButton.AddHotkey(Keys.Space);
            backButton.AddHotkey(Keys.Back);
            backButton2.AddHotkey(Keys.Back);
            closeButton.AddHotkey(Keys.Escape);
            closeButton1.AddHotkey(Keys.Escape);
#if (!WINDOWS_APP)
            exitButton.AddHotkey(Keys.Escape);
#endif
#endif
        }

        private void OnStateChange()
        {
            switch (state)
            {
                case State.NotInitialized:
                    gameOverMenu.SetEnabled(false);
                    inGamePause.SetEnabled(false);
                    inGameGui.SetEnabled(false);
                    creditsWindow.SetEnabled(false);
                    settingsWindow.SetEnabled(false);
                    mainMenu.SetEnabled(false);
                    buttons.SetEnabled(false);
                    rootScene.Enabled = false;
                    break;
                case State.MainMenu:
                    gameOverMenu.SetEnabled(false);
                    inGamePause.SetEnabled(false);
                    inGameGui.SetEnabled(false);
                    creditsWindow.SetEnabled(false);
                    settingsWindow.SetEnabled(false);
                    mainMenu.SetEnabled(true);
                    buttons.SetEnabled(true);
                    rootScene.Enabled = false;
                    break;
                case State.Paused:
                    pausedScoreLabel.Text = scoreBox.Score.ToString();
                    gameOverMenu.SetEnabled(false);
                    mainMenu.SetEnabled(false);
                    inGamePause.SetEnabled(true);
                    inGameGui.SetEnabled(false);
                    buttons.SetEnabled(true);
                    creditsWindow.SetEnabled(false);
                    settingsWindow.SetEnabled(false);
                    rootScene.Enabled = false;
                    //AudioEngine.PauseSong();
                    break;
                case State.GameOver:
                    finalScoreLabel.Text = scoreBox.Score.ToString();
                    settingsWindow.SetEnabled(false);
                    creditsWindow.SetEnabled(false);
                    gameOverMenu.SetEnabled(true);
                    inGamePause.SetEnabled(false);
                    mainMenu.SetEnabled(false);
                    buttons.SetEnabled(true);
                    inGameGui.SetEnabled(false);
                    rootScene.Enabled = false;
                    AudioEngine.PauseSong();
                    break;
                case State.Credits:
                    creditsWindow.SetEnabled(true);
                    settingsWindow.SetEnabled(false);
                    gameOverMenu.Enabled = false;
                    inGamePause.Enabled = false;
                    mainMenu.Enabled = false;
                    buttons.Enabled = false;
                    break;
                case State.Settings:
                    creditsWindow.SetEnabled(false);
                    settingsWindow.SetEnabled(true);
                    gameOverMenu.Enabled = false;
                    inGamePause.Enabled = false;
                    mainMenu.Enabled = false;
                    buttons.Enabled = false;
                    break;
                case State.InGame:
                    settingsWindow.SetEnabled(false);
                    creditsWindow.SetEnabled(false);
                    gameOverMenu.SetEnabled(false);
                    inGamePause.SetEnabled(false);
                    mainMenu.SetEnabled(false);
                    buttons.SetEnabled(false);
                    inGameGui.SetEnabled(true);
                    rootScene.Enabled = true;
                    // Nothing wrong if was playing
                    AudioEngine.ResumeSong();
                    break;
            }
        }
        private void OnCloseButton(object sender, EventArgs e)
        {
            AudioEngine.PlayEffect("kick");
            State temp = state;
            state = prevState;
            prevState = temp;
            OnStateChange();
        }

        private void OnSettingsClicked(object sender, EventArgs e)
        {
            AudioEngine.PlayEffect("kick");
            prevState = state;
            state = State.Settings;
            OnStateChange();
        }

        private void OnInfoClicked(object sender, EventArgs e)
        {
            AudioEngine.PlayEffect("kick");
            prevState = state;
            state = State.Credits;
            OnStateChange();
        }

        internal void InitState()
        {
            AudioEngine.PlayAlbum("all");
            state = State.MainMenu;
            OnStateChange();
        }

        private void OnPause(object sender, EventArgs args)
        {
            if (state != State.Paused)
            {
                AudioEngine.PlayEffect("kick");
                Pause();
            }
        }

        internal void Pause()
        {
            prevState = state;
            state = State.Paused;
            OnStateChange();
        }

        private void OnContinue(object sender, EventArgs args)
        {
            AudioEngine.PlayEffect("kick");
            prevState = state;
            state = State.InGame;
            OnStateChange();
        }

        private void OnStart(object sender, EventArgs args)
        {
            AudioEngine.PlayEffect("kick");
            prevState = state;
            state = State.InGame;
            OnStateChange();
            ((MainScene)rootScene).Start();
        }

        private void OnBack(object sender, EventArgs args)
        {
			AudioEngine.PlayEffect("kick");
            if (state == State.InGame)
                Pause();
            else if (state == State.Paused || state == State.GameOver)
                GoToMainMenu();
#if (!WINDOWS_APP)
            else
                Exit();
#endif
        }

        private void OnRestart(object sender, EventArgs args)
        {
            ((MainScene)rootScene).Reset();
            OnStart(sender, args);
            AudioEngine.ResumeSong();
            //AudioEngine.PlayAlbum("all");
        }

        /// <summary>
        /// Gets called by rootScene
        /// </summary>
        internal void OnGameOver()
        {
            prevState = state;
            state = State.GameOver;
            leaderBoard.AddNewScore(scoreBox.Score);
            OnStateChange();
        }

        private const string myCredit = "Game by\n   Run2Games";
        private const string myLink = "   gamejolt.com/profile/run2games/\n";
        private const string myUrl = "http://gamejolt.com/profile/run2games/859599";
        private const string songs = "Music by\n   Eric Matyas";
        private const string songsLink = "   www.soundimage.org\n";
        private const string songsUrl = "http://soundimage.org"; //Edge complains about security certificate when https
        private const string pauseIcon = "Pause icon by\n   Yannick from ";
        private const string pauseLink = "   www.flaticon.com\n";
        private const string pauseUrl = "https://flaticon.com";
        private const string infoIcon = "Info icon by\n   Ezekiel63745 at en.wikipedia";
        private const int creditCount = 7;

        private PositionedGUIComponent[] CreateCredits(Controls controls, SpriteFont regularFont)
        {
            LabelComponent[] labels = new LabelComponent[creditCount];
            PositionedGUIComponent[] cr = new PositionedGUIComponent[creditCount];

            for (int i = 0; i < creditCount; i++)
                labels[i] = new LabelComponent(regularFont, Color.White);

            labels[0].Text = myCredit;
            labels[1].Text = myLink;
            labels[2].Text = songs;
            labels[3].Text = songsLink;
            labels[4].Text = pauseIcon;
            labels[5].Text = pauseLink;
            labels[6].Text = infoIcon;

            Color linkColor = Color.Blue;
            cr[0] = labels[0];
            cr[1] = new LinkComponent(controls, labels[1], myUrl);
            labels[1].color = linkColor;
            cr[2] = labels[2];
            cr[3] = new LinkComponent(controls, labels[3], songsUrl);
            labels[3].color = linkColor;
            cr[4] = labels[4];
            cr[5] = new LinkComponent(controls, labels[5], pauseUrl);
            labels[5].color = linkColor;
            cr[6] = labels[6];

            return cr;
        }

        private void AddCreditLabels(GUIContainer creditsWindow, SpriteFont titleFont, Controls controls)
        {
            BoxContainer list = new BoxContainer(Orientation.VERTICAL);
            list.InheritParentSize = true;
            list.DefaultHorizontalAlignment = HorizontalPosition.LEFT;
            list.HParentAlignment = HorizontalPosition.LEFT;
            //list.Spaces = 5;
            creditsWindow.AddComponent(list);

            //TODO: Make credits scrolable
            LabelComponent title = new LabelComponent(titleFont, Color.White, "credits");
            BoxContainer.Element element = new BoxContainer.Element(title, 0, VerticalPosition.TOP,
                HorizontalPosition.CENTER, VerticalPosition.TOP, HorizontalPosition.CENTER);
            list.AddComponent(element);

            SpriteFont regularFont = Content.Load<SpriteFont>("regular");
            PositionedGUIComponent[] labels = CreateCredits(controls, regularFont);
            foreach (var label in labels)
            {
                list.AddComponent(label);
                // lock font size so that it is not scaled on resize events
                // We are trusting initial window size to scale according to it properly
                //label.MinScale = label.MaxScale = label.RealScale;
            }

            // Add version label
            Assembly asm = typeof(AFootball).GetTypeInfo().Assembly;
            string version = GetAssemblyVersion(asm);
            LabelComponent versionLabel = new LabelComponent(regularFont, Color.White, 'v'+version);
            versionLabel.SetAlignmet(VerticalPosition.BOTTOM, HorizontalPosition.RIGHT);
            creditsWindow.AddComponent(versionLabel);
            versionLabel.AlignToParent(VerticalPosition.BOTTOM, HorizontalPosition.RIGHT);
        }

        public string GetAssemblyVersion(Assembly asm)
        {
            var attr = CustomAttributeExtensions.GetCustomAttribute<AssemblyFileVersionAttribute>(asm);
            if (attr != null)
                return attr.Version;
            else
                return "";
        }

        private void AddSettings(GUIContainer settingsWindow, SpriteFont titleFont, Controls controls)
        {
            //BoxContainer list = new BoxContainer(Orientation.VERTICAL, new Rectangle(new Point(0, 0),
            //    settingsWindow.LocalArea.Size));
            BoxContainer list = new BoxContainer(Orientation.VERTICAL);
            list.InheritParentSize = true;
            list.DefaultHorizontalAlignment = HorizontalPosition.LEFT;
            list.HParentAlignment = HorizontalPosition.LEFT;
            list.Spaces = 20;
            settingsWindow.AddComponent(list);

            LabelComponent title = new LabelComponent(titleFont, Color.White, "settings");
            BoxContainer.Element element = new BoxContainer.Element(title, 0, VerticalPosition.TOP, HorizontalPosition.CENTER,
                VerticalPosition.TOP, HorizontalPosition.CENTER);
            list.AddComponent(element);

            Texture2D chText = Content.Load<Texture2D>("textures/checkbox-checked");
            Texture2D unchText = Content.Load<Texture2D>("textures/checkbox-unchecked");
            SpriteFont font = Content.Load<SpriteFont>("regular");
            LabelComponent sfx = new LabelComponent(font, Color.White, "  Sound Effects Enabled");
            LabelComponent music = new LabelComponent(font, Color.White, "  Music Enabled");
            LabelComponent uSfx = new LabelComponent(font, Color.White, "  Sound Effects Disabled");
            LabelComponent uMusic = new LabelComponent(font, Color.White, "  Music Disabled");
            Point boxSize = new Point(80, 80);
            CheckBox sfxBox = new CheckBox(controls, new TextureComponent(unchText, Color.White, boxSize),
                new TextureComponent(chText, Color.White, boxSize), uSfx, sfx);
            CheckBox musicBox = new CheckBox(controls, new TextureComponent(unchText, Color.White, boxSize),
                new TextureComponent(chText, Color.White, boxSize), uMusic, music);
            sfxBox.Checked = musicBox.Checked = true;
            sfxBox.CheckedChanged += OnSfxChecked;
            musicBox.CheckedChanged += OnMusicChecked;

            list.AddComponent(sfxBox);
            list.AddComponent(musicBox);

            ////lock in to initial size again
            //sfxBox.MinScale = sfxBox.MaxScale = musicBox.MinScale = musicBox.MaxScale = musicBox.RealScale;
            //music.MinScale = music.MaxScale = sfx.MinScale = sfx.MaxScale = uMusic.MinScale = uMusic.MaxScale =
            //    uSfx.MinScale = sfx.MaxScale = sfx.RealScale;
        }

        private void OnMusicChecked(object sender, EventArgs e)
        {
            AudioEngine.MusicEnabled = ((CheckBox)sender).Checked;
        }

        private void OnSfxChecked(object sender, EventArgs e)
        {
            AudioEngine.SoundEffectsEnabled = ((CheckBox)sender).Checked;
        }
    }
}
