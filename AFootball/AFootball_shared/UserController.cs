using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TMono;
using Jitter.Collision;
using Jitter.LinearMath;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using TMono.TControls;
using TMono.Physics;

namespace AFootball
{
    class UserController : ControllerComponent
    {
        public enum Actions
        {
            GO_AT_POINTED,
            GO_FORWARD,
            GO_BACKWARD,
            GO_LEFT,
            GO_RIGHT,
            GO_JUMP
        }
#if BALL_PHYSICS_TESTING
        public DynamicPhysics ball;
#endif
        private Controls controls;
        private Point pointedPos = Point.Zero;
        private FootballerPhysics fPhys;
        private Camera camera;   //needed for getting object pos to screen coordinates
        private Matrix invClient;
        private Vector3 direction; //direction this frame (should be reset every frame)
        // Used for additional animations when evading collisions
        private float time2Collision = -1;
        private FootballerPhysics nearestFootballer;
        private Vector3 dir2Collide;
        
        public UserController(PhysicsComponent phys, Controls controls_, Camera camera_)
            : base(phys)
        {
            OnClientSizeChanged(this, EventArgs.Empty);
            controls = controls_;
            fPhys = (FootballerPhysics)phys;
            camera = camera_;
            RegisterForEvents();
        }

        internal void OnClientSizeChanged(object sender, EventArgs args)
        {
            Viewport vp = Camera.Viewport;
            invClient = Matrix.Invert(Matrix.CreateOrthographicOffCenter(0, vp.Width, vp.Height, 0, -1, 1));
            //invClient = Matrix.Invert(Matrix.CreatePerspectiveOffCenter(0, vp.Width, vp.Height, 0, 1, 100));
        }

        private void RegisterForEvents()
        {
#if (!ANIMATION_TESTING && !BALL_PHYSICS_TESTING)
            controls.RegisterDownHandler((byte)Actions.GO_AT_POINTED, OnGoAtPointed);
            if (controls is PCControls)
            {
                controls.RegisterDownHandler((byte)Actions.GO_BACKWARD, OnGoBackward);
                controls.RegisterDownHandler((byte)Actions.GO_FORWARD, OnGoForward);
                controls.RegisterDownHandler((byte)Actions.GO_LEFT, OnGoLeft);
                controls.RegisterDownHandler((byte)Actions.GO_RIGHT, OnGoRight);
            }
#endif
        }
        private void OnGoAtPointed(object sender, ControlsEventArgs args)
        {
            var arg = (PosControlsEventArgs)args;
            direction = GetDirection(arg.Position);

        }
        private void OnGoBackward(object sender, ControlsEventArgs args)
        {
            direction += Vector3.Down;
        }
        private void OnGoForward(object sender, ControlsEventArgs args)
        {
            direction += Vector3.Up;
        }
        private void OnGoLeft(object sender, ControlsEventArgs args)
        {
            direction += Vector3.Left;
        }
        private void OnGoRight(object sender, ControlsEventArgs args)
        {
            direction += Vector3.Right;
        }

        public void RegisterImpendingCollsion(FootballerPhysics footballer, float timeLeft)
        {
            if (time2Collision < 0 || time2Collision > timeLeft)
            {
                time2Collision = timeLeft;
                nearestFootballer = footballer;
                dir2Collide = Converter.ToXNAVector(fPhys.Body.LinearVelocity);
            }
        }

        private void TryToEvade()
        {
            float angle = TMath.CalculateAngleAbs(direction, dir2Collide);
            //FIXME:
            if (angle > Math.PI / 7)
            {
                bool left = (direction.X > dir2Collide.X) ? false : true;
                fPhys.Spin(left);
                ResetEvading();
            }
        }

        private void ResetEvading()
        {
            time2Collision = -1;
            nearestFootballer = null;
            dir2Collide = Vector3.Zero;
        }

        /// <summary>
        /// Must be called after all events have fired
        /// Meaning update order of this component has to be higher than that of controls
        /// </summary>
        public override void Update(GameTime time)
        {
            if (time2Collision >= 0)
                time2Collision -= (float)time.ElapsedGameTime.TotalSeconds;
            //need to check if enabled because often update gets called event though it was disbaled in the same frame
            if (direction.Length() > 0 && Enabled && fPhys.HasBall)
            {
                if (time2Collision > 0)
                    TryToEvade();
                fPhys.Accelerate(direction); //this method normalizes dir vector           
            }
            direction = Vector3.Zero;

            //TODO: remove unneeded clips
#if ANIMATION_TESTING
            if (Keyboard.GetState().IsKeyDown(Keys.Q))
                fPhys.AnimPlayer.PlayClip("walk");
            else if (Keyboard.GetState().IsKeyDown(Keys.W))
                fPhys.AnimPlayer.PlayClip("run");
            else if (Keyboard.GetState().IsKeyDown(Keys.E))
            {
                fPhys.AnimPlayer.PlayClip("run_with_ball");
                fPhys.AnimPlayer.Looping = true;
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.R))
                fPhys.AnimPlayer.PlayClip("squat");
            else if (Keyboard.GetState().IsKeyDown(Keys.T))
                fPhys.AnimPlayer.PlayClip("tackle");
            else if (Keyboard.GetState().IsKeyDown(Keys.Y))
            {
                //fPhys.AnimPlayer.PlayClip("fall");
                fPhys.Fall();
                fPhys.AnimPlayer.Looping = true;
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.U))
                fPhys.AnimPlayer.PlayClip("catch");
            else if (Keyboard.GetState().IsKeyDown(Keys.I))
                fPhys.AnimPlayer.PlayClip("wait");
            else if (Keyboard.GetState().IsKeyDown(Keys.D))
                fPhys.AnimPlayer.PlayClip("spin_right");
            else if (Keyboard.GetState().IsKeyDown(Keys.A))
                fPhys.AnimPlayer.PlayClip("spin_right", true);
            else if (Keyboard.GetState().IsKeyDown(Keys.P))
                fPhys.AnimPlayer.Pause();
            else if (Keyboard.GetState().IsKeyDown(Keys.S))
                fPhys.AnimPlayer.Stop(true);
            else if (Keyboard.GetState().IsKeyDown(Keys.M))
                fPhys.AnimPlayer.SpeedFactor += 0.01;
            else if (Keyboard.GetState().IsKeyDown(Keys.N))
                fPhys.AnimPlayer.SpeedFactor -= 0.01;
#endif
#if BALL_PHYSICS_TESTING
            if (Keyboard.GetState().IsKeyDown(Keys.Up))
                ball.Impulse(JVector.Up);
            else if (Keyboard.GetState().IsKeyDown(Keys.Down))
                ball.Impulse(JVector.Down);
            else if (Keyboard.GetState().IsKeyDown(Keys.Left))
                ball.Impulse(JVector.Right);
            else if (Keyboard.GetState().IsKeyDown(Keys.Right))
                ball.Impulse(JVector.Left);
            else if (Keyboard.GetState().IsKeyDown(Keys.Space))
                ball.Impulse(JVector.Backward);
#endif
        }
        private Vector3 GetDirection()  //gets direction from new pointer position
        {
            Point pointer = controls.GetPosition();
            return GetDirection(pointer);
        }
        private Vector3 GetDirection(Point pointer)
        {
            Vector2 pointerPos = new Vector2(pointer.X, pointer.Y);
            Vector4 objPos = new Vector4(0, 0, 0, 1);
            Matrix worldViewProj = phys.Node.WorldTransform * camera.View * camera.Projection;
            objPos = Vector4.Transform(objPos, worldViewProj);
            objPos /= objPos.W;
            Vector2 clientResult = Vector2.Transform(new Vector2(objPos.X, objPos.Y), invClient);
            Vector2 res = pointerPos - clientResult;
            return new Vector3(res.X, -res.Y, 0);
        }
        protected override void OnEnabledChanged(object sender, EventArgs args)
        {
            base.OnEnabledChanged(sender, args);
            ResetEvading();
            direction = Vector3.Zero;
        }
    }
}
