using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TMono;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Jitter.Dynamics;
using Jitter.Collision.Shapes;
using Jitter.LinearMath;
using System.Diagnostics;
using TMono.TControls;
using TMono.Physics;

namespace AFootball
{
    class FootballerType : IGameObjectType
    {
        public enum ControllerType
        {
            USER_PC,
            USER_TOUCH,
            AI
        }
        static private Camera camera;
        private ControllerType contrlType;
        private Dictionary<byte, Keys> actionDict;
        private FootballerPhysCaps caps;
        private Texture2D[] textures;
        private Controls controls = null;
        //static private Shape shape = new BoxShape(1.3f, 2.0f, 0.5f);//new CapsuleShape(2.0f, 0.3f);
        static private Shape shape; //= new BoxShape(1.5f, 0.8f, 2.0f);//new CapsuleShape(2.0f, 0.3f);
        static private Texture2D[] team1Textures = new Texture2D[4];
        static private Texture2D[] team2Textures = new Texture2D[4];
        static private AnimatedModel model;
        static private FootballerPhysics playerPhys;   //assumes there is only one in a game
        static private ObjectPool<AnimatedGameObject> pool = new ObjectPool<AnimatedGameObject>(30, 20, CreateDefault);
        static private AI.ImpendingCollisionDel RegisterImpendingColl;

        static public float FootballerHeight
        { private set; get; }
        public AIProperties PropertiesAI
        {
            set; get;
        }
        public FootballerPhysCaps PhysicsCaps
        {
            set { caps = value; }
            get { return caps; }
        }
		static public Camera MainCamera
		{
			set { camera = value; }
		}

        public FootballerType(ControllerType controllerType_, 
            FootballerPhysCaps capabilities, AIProperties aiProperties, Controls controls = null)
        {
			if (camera == null)
				throw new InvalidOperationException("MainCamera property of FootballerType " +
												"not initialized");
            contrlType = controllerType_;
            if (contrlType == ControllerType.USER_PC)
            {
                Debug.Assert(controls != null && (controls is PCControls || controls is UniversalControls));
                actionDict = new Dictionary<byte, Keys>
                { {(byte)UserController.Actions.GO_FORWARD, Keys.Up },
                  {(byte)UserController.Actions.GO_BACKWARD, Keys.Down },
                  {(byte)UserController.Actions.GO_LEFT, Keys.Left },
                  {(byte)UserController.Actions.GO_RIGHT, Keys.Right },
                  {(byte)UserController.Actions.GO_AT_POINTED, (Keys)PCControls.leftMouseButton }
                };
                textures = team1Textures;
            }
            else if (contrlType == ControllerType.USER_TOUCH)
            {
                Debug.Assert(controls != null && controls is TouchControls);
                actionDict = new Dictionary<byte, Keys> {
                  {(byte)UserController.Actions.GO_AT_POINTED, (Keys)TouchControls.touched }
                };
                textures = team1Textures;
            }
            else if (contrlType == ControllerType.AI)
                textures = team2Textures;

            caps = capabilities;
			PropertiesAI = aiProperties;
            this.controls = controls;
        }
		public FootballerType(ControllerType contrlType_, FootballerPhysCaps physCaps, Controls controls) 
			: this(contrlType_, physCaps, new AIProperties(10, 100, 1000, 0.6f), controls)
		{
		}
        public FootballerType(FootballerType type) : this(type.contrlType, 
            new FootballerPhysCaps(type.PhysicsCaps), new AIProperties(type.PropertiesAI))
        {
        }
        public GameObject CreateObject(Vector3 pos)
        {
			Matrix world = Matrix.CreateWorld(pos, -Vector3.UnitZ, -Vector3.UnitY);
            return CreateObject(world);
        }

        public GameObject CreateObject(Matrix world_)
        {
            // Assuming physics and graphics components are created (gone through CreateDefault)
            AnimatedGameObject footballer = pool.Create();
            //Initialize world transform
            footballer.Node.LocalTransform = world_;

            //Initialize physics
            FootballerPhysics phys = (FootballerPhysics)footballer.Physics;
			phys.Init(footballer.Node, new FootballerPhysCaps(caps));     //trusting that other parameters are properly initialized
            phys.Body.Position = Converter.ToJVector(footballer.Node.LocalTransform.Translation);

            //Graphics
            footballer.Graphics.Node = footballer.Node; //Everything else can be reused
            ((GraphicsComponentSkinned)footballer.Graphics).SetEffectsDel = SetEffect;

            //Controller
            if (contrlType == ControllerType.USER_PC || contrlType == ControllerType.USER_TOUCH)
            {
                Debug.Assert(controls != null);
                controls.AddActions(actionDict);
                //User controller shouldn't be initialize too often so we just create new instead of initializing existing
                UserController controller = new UserController(phys, controls, camera);
                footballer.Controller = controller;
                //assuming there is only one user in a game
                // User footballer should be created first because of the following 2 calls
                playerPhys = phys;  
                RegisterImpendingColl = controller.RegisterImpendingCollsion; 

                playerPhys.ReadyToCatchBall = true;
            }
            else if (contrlType == ControllerType.AI)
            {
                if (footballer.Controller == null)
					footballer.Controller = new AI(phys, playerPhys, new AIProperties(PropertiesAI), 
					                               RegisterImpendingColl);
                else
                {
                    AI ai = footballer.Controller as AI;
                    //TODO: Change controller if needed
                    Debug.Assert(ai != null);
					ai.Init(phys, playerPhys, new AIProperties(PropertiesAI));
                }
            }
            return footballer;
        }

        static public void LoadContent()
        {
            model = new AnimatedModel("football_player");
            model.LoadContent(GraphicsComponent.Content);

            BoundingSphere sphere = model.Model.Meshes[0].BoundingSphere;
            FootballerHeight = sphere.Radius * 2;
            shape = new BoxShape(1.5f, 0.75f, FootballerHeight);

            model.Model.Meshes[4].Effects = new ModelEffectCollection(new List<Effect>());

            // Load textures
            //TODO: Build and load diffuse_helmet_... separately for android (unless it starts to work)
            team1Textures[0] = GraphicsComponent.Content.Load<Texture2D>("flattened/diffuse_footballer_2_0");
            team1Textures[1] = GraphicsComponent.Content.Load<Texture2D>("flattened/diffuse_helmet_2_0");
            team1Textures[3] = GraphicsComponent.Content.Load<Texture2D>("flattened/patern_helmet_2_0");
            team1Textures[2] = GraphicsComponent.Content.Load<Texture2D>("flattened/number_2_0");

            team2Textures[0] = GraphicsComponent.Content.Load<Texture2D>("flattened/diffuse_footballer_1");
            team2Textures[1] = GraphicsComponent.Content.Load<Texture2D>("flattened/diffuse_helmet_1");
            team2Textures[3] = GraphicsComponent.Content.Load<Texture2D>("flattened/patern_helmet_1");
            team2Textures[2] = GraphicsComponent.Content.Load<Texture2D>("flattened/number_1");
        }
        static public void Initialize()
        {
            pool.InitObjects(20);
        }
        private void SetEffect(GraphicsComponent3D graphics)
        {
            Model model = ((GraphicsComponentSkinned)graphics).Model.Model;
            for (int i = 0; i < textures.Count(); i++)
                ((SkinnedEffect)model.Meshes[i].Effects[0]).Texture = textures[i];
            
        }
        private static AnimatedGameObject CreateDefault()
        {
            Node world = new Node(Matrix.CreateWorld(Vector3.Zero, -Vector3.UnitZ, -Vector3.UnitY));
            var graphics = new GraphicsComponentSkinned(world, camera, model);
            FootballerPhysics phys = new FootballerPhysics(new RigidBody(shape), 
                world, new Vector3(0, 0, -9.8f), null, true, true);
            //setting controller to null because we don't know what kind of controller we will need
            AnimatedGameObject footballer = new AnimatedGameObject(phys, graphics, null, world, "footballer");
            phys.ParentObject = footballer;
            return footballer;
        }

    }
}
