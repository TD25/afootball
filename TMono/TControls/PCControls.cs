﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace TMono.TControls
{
    public class PCControls : Controls
    {
        //if mouse or keyboard not available, is set to default in Update
        private MouseState mouseState;
        private KeyboardState keyState;
        protected Point lastMousePos;     //pressed or checked with GetMousePos
        public const int leftMouseButton = 255;
        public const int rightMouseButton = 256;

        public PCControls(Dictionary<byte, Keys> actions)
            : base(actions)
        {
        }

        /// <summary>
        /// Update should get called before using polling if keys are pressed (released)
        /// </summary>
        public override void Update(GameTime gameTime)
        {
            //set to default if not available
            mouseState = Mouse.GetState(); 
            keyState = Keyboard.GetState();
            base.Update(gameTime);
        }
        public override bool IsKeyPressed(byte action)
        {
            if (!Actions.ContainsKey(action))
                return false;

            if (Actions[action] == (Keys)leftMouseButton)
            {
                if (mouseState.LeftButton == ButtonState.Pressed)
                {
                    lastMousePos = mouseState.Position;
                    return true;
                }
                else return false;
            }
            if (Actions[action] == (Keys)rightMouseButton)
            {
                if (mouseState.RightButton == ButtonState.Pressed)
                {
                    lastMousePos = mouseState.Position;
                    return true;
                }
                else
                    return false;
            }
            else
                return keyState.IsKeyDown(Actions[action]);
        }
        public override bool IsKeyUp(byte action)
        {
            if (!Actions.ContainsKey(action))
                return false;

            if (Actions[action] == (Keys)leftMouseButton)
                return mouseState.LeftButton == ButtonState.Released;
            if (Actions[action] == (Keys)rightMouseButton)
                return mouseState.RightButton == ButtonState.Released;
            else
                return keyState.IsKeyUp(Actions[action]);
        }
        /// <summary>
		/// Gets relative mouse position (in client window)
        /// </summary>
        public override Point GetPosition()
        {
            return mouseState.Position;
        }

        protected override void CallHandler(byte action, EventHandler<ControlsEventArgs> handler)
        {
            ControlsEventArgs args;
            if (Actions[action] == (Keys)rightMouseButton || Actions[action] == (Keys)leftMouseButton)
                args = new PosControlsEventArgs(action, lastMousePos);
            else
                args = new ControlsEventArgs(action);
            handler(this, args);
        }
    }
}
