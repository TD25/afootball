﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace TMono
{
    public class GraphicsComponent3D : GraphicsComponent
    {
        protected Node node; //reference to node in GameObject which is updated by PhysicsComponent
        protected Model model;
        protected Camera camera;
        public delegate void SetEffects(GraphicsComponent3D thisComponent);
        protected SetEffects setEffects = delegate (GraphicsComponent3D t) { };

        public Model Model
        {
            get { return model; }
            set { model = value; }
        }
        public Node Node
        {
            get { return node; }
            set { node = value; }
        }

        /// <summary>
        /// Use this to prepare object to be drawn
        ///     by setting effects (textures, lighting etc.)
        /// It is called automaticaly before drawing
        /// </summary>
        public SetEffects SetEffectsDel
        {
            get { return setEffects; }
            set { setEffects = value; }
        }

        public GraphicsComponent3D(Node node, Camera camera_, Model model)
        {
            camera = camera_;
            this.model = model;
            this.node = node;
        }

        protected GraphicsComponent3D(Node node, Camera camera_)
        {
            camera = camera_;
            this.node = node;
        }

        public override void Draw(GameTime time)
        {
            setEffects(this);
            DrawModel();
        }

        virtual protected void DrawModel()
        {
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect eff in mesh.Effects)
                {
                    eff.World = node.WorldTransform;
                    eff.View = camera.View;
                    eff.Projection = camera.Projection;
                }
                mesh.Draw();
            }
        }
    }

    public class GraphicsComponentSkinned : GraphicsComponent3D
    {
        new protected AnimatedModel model;
        private List<Bone> bones;

        public List<Bone> Bones
        {
            set { bones = value; }
            get { return bones; }
        }
        new public AnimatedModel Model
        {
            get { return model; }
            set { model = value; }
        }

        public GraphicsComponentSkinned(Node node, Camera camera_, AnimatedModel model_)
            : base(node, camera_)
        {
            model = model_;
        }

        //TODO: Try to optimize
        protected override void DrawModel()
        {
            if (bones == null)
                return;

            //
            // Determine the skin transforms from the skeleton
            //
            Matrix[] skeleton = new Matrix[model.ModelExtra.Skeleton.Count];
            for (int s = 0; s < model.ModelExtra.Skeleton.Count; s++)
            {
                Bone bone = bones[model.ModelExtra.Skeleton[s]];
                skeleton[s] = bone.SkinTransform * bone.WorldTransform;
            }

            // Draw the model.
            foreach (ModelMesh modelMesh in model.Model.Meshes)
            {
                // If you need to remove mesh from model remove its effect
                if (modelMesh.Effects.Count == 0)
                    continue;
                foreach (Effect effect in modelMesh.Effects)
                {
                    if (effect is BasicEffect)
                    {
                        BasicEffect beffect = effect as BasicEffect;
                        //beffect.World = boneTransforms[modelMesh.ParentBone.Index];
                        beffect.View = camera.View;
                        beffect.Projection = camera.Projection;
                        beffect.EnableDefaultLighting();
                        beffect.PreferPerPixelLighting = true;
                    }

                    if (effect is SkinnedEffect)
                    {
                        SkinnedEffect seffect = effect as SkinnedEffect;
                        //seffect.World = boneTransforms[modelMesh.ParentBone.Index];
                        seffect.View = camera.View;
                        seffect.Projection = camera.Projection;
                        seffect.EnableDefaultLighting();
                        seffect.PreferPerPixelLighting = true;
                        seffect.SetBoneTransforms(skeleton);
                    }
                }
                modelMesh.Draw();
            }
        }
    }
}
