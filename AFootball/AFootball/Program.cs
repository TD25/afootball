﻿using System;

namespace AFootball
{
#if WINDOWS_DESKTOP || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            using (var game = new AFootball())
                game.Run();
        }
    }
#endif
}
