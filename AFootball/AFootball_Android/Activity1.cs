using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;

namespace AFootball
{
    [Activity(Label = "Run2TouchDown"
        , MainLauncher = true
        , Icon = "@drawable/icon"
        , Theme = "@style/Theme.Splash"
        , AlwaysRetainTaskState = true
        , LaunchMode = Android.Content.PM.LaunchMode.SingleInstance
        , ScreenOrientation = ScreenOrientation.Portrait
        , ConfigurationChanges = ConfigChanges.Orientation | ConfigChanges.Keyboard | ConfigChanges.KeyboardHidden | ConfigChanges.ScreenSize)]
    public class Activity1 : Microsoft.Xna.Framework.AndroidGameActivity
    {
        private AFootball game;

        protected override void OnCreate(Bundle bundle)
        {
            TMono.OS.Activity = this;
            base.OnCreate(bundle);
            game = new AFootball();
            var view = (View)game.Services.GetService(typeof(View));
            SetContentView(view);
            game.Run();
        }

        protected override void OnPause()
        {
            base.OnPause();
            if (game.CurrentState == AFootball.State.InGame)
                game.Pause();
        }
    }
}

